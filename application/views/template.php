<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CRM-TV</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>-->
        <script src="<?php echo base_url(); ?>assets/editablegrid/js/jquery-1.12.4.min.js" ></script>
        <script src="<?php echo base_url(); ?>assets/editablegrid/js/jquery.richtext.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/editablegrid/js/select2.min.js"></script>
        <!-- editgrid js -->
        <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid.js"></script>
        <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid_charts.js"></script>
        <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid_charts_ofc.js"></script>
        <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid_editors.js"></script>
        <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid_renderers.js"></script>
        <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid_utils.js"></script>
        <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid_validators.js"></script>
        <!-- Sweet Alert -->
        <script src="<?php echo base_url(); ?>assets/dist/js/sweetalert.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sweetalert/sweetalert.css">

        <meta charset="utf-8">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/jvectormap/jquery-jvectormap.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.print.min.css" media="print">
        <!-- Bootstrap 3.3.6 -->
        <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/css/bootstrap.min.css">-->
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Morris charts -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/morris.js/morris.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
        <!-- Font Awesome Icon Library -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <!-- JavaScript -->
        <script src="<?= base_url() ?>assets/plugins/alertifyjs/alertify.min.js"></script>
        <!-- CSS -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/alertifyjs/css/alertify.min.css"/>
        <!-- Default theme -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/alertifyjs/css/themes/default.min.css"/>
        <!-- Semantic UI theme -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/alertifyjs/css/themes/semantic.min.css"/>
        <!-- Bootstrap theme -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/alertifyjs/css/themes/bootstrap.min.css"/>
        <!-- editgrid -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/editablegrid/css/style.css" type="text/css" media="screen">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/editablegrid/css/responsive.css" type="text/css" media="screen">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/editablegrid/css/font-awesome-4.7.0/css/font-awesome.min.css" type="text/css" media="screen">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/editablegrid/css/richtext.min.css" type="text/css" media="screen">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/editablegrid/css/select2.min.css" type="text/css" media="screen">
        <!-- Bootstrap Rating -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap-star/css/star-rating.min.css" />
        <script src="<?php echo base_url(); ?>assets/bootstrap-star/js/star-rating.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/justgage.js/justgage.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/justgage.js/raphael-2.1.4.min.js"></script>
        <script src="<?= base_url()?>assets/notify.min.js"></script>
        <script src="<?= base_url()?>assets/notify.js"></script>
        <style>
            @font-face {
                font-family: segoeui;
                src: url(<?php echo base_url(); ?>assets/dist/css/segoeui.ttf);
            }
            body {
                font-size: 12px !important;
                font-family: segoeui !important;
            }
            .example-modal .modal {
                position: relative;
                top: auto;
                bottom: auto;
                right: auto;
                left: auto;
                display: block;
                z-index: 1;
            }
            .example-modal .modal {
                background: transparent !important;
            }
            .table {
                /* width: 100%;
                 max-width: 100%;
                 margin-bottom: 20px;*/
                font-size: 12px;
            }
            .btn {
                display: inline-block;
                padding: 6px 12px;
                margin-bottom: 0;
                font-size: 12px;
                font-weight: 400;
                line-height: 1.42857143;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                -ms-touch-action: manipulation;
                touch-action: manipulation;
                cursor: pointer;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                background-image: none;
                border: 1px solid transparent;
                border-radius: 4px;
            }
            h1 {
                font-size: 22px !important;
            }
            .sidebar-menu>li>a {
                font-size: 13px !important;
                font-weight: 500 !important;
            }
            .treeview-menu>li>a {
                font-size: 13px !important;
                font-weight: 500 !important;
            }
        </style>
    </head>
    <body class="hold-transition skin-black sidebar-mini sidebar-collapse fixed">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="<?= base_url('index.php'); ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><img src="<?php echo base_url(); ?>assets/dist/img/logokecil.png" width="50"/></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b><img src="<?php echo base_url(); ?>assets/dist/img/logokecil.png" width="50"/></b> <b>CRM</b> TV</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo base_url(); ?>assets/dist/img/user_icon.png" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?php echo $this->data["session"]["name"]; ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo base_url(); ?>assets/dist/img/user_icon.png" class="img-circle" alt="User Image">
                                        <p><?php echo $this->data["session"]["name"]; ?></p>
                                        <p>
                                            <i class="fa fa-user"></i>
                                            <?= $this->data['session']['user_role']?>
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer" >
                                        <!-- <div class="pull-left">
                                            <a href="#" class="btn bg-navy btn-flat margin">Profile</a>
                                        </div> -->
                                        <div class="pull-right">
                                            <a href="<?php echo base_url(); ?>index.php/logout" class="btn bg-navy btn-flat margin">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url(); ?>assets/dist/img/user_icon.png" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo $this->data["session"]["name"]; ?></p>
                            <a href="javascript:;"><i class="fa fa-user"></i>
                                <?php echo $this->data["session"]["user_role"]; ?>
                            </a>
                        </div>
                    </div>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <?php
                        $master_uri = current_url();
                        $data_menu = json_decode($_COOKIE['all_menu'],true);
                        $data_menu = $data_menu['menu'];
                        
                        $menu = array_keys($data_menu);
                        foreach ($menu as $key){
                            $menu_master = explode("=",$key);
                            $menu_name = $menu_master[0];
                            $menu_logo = $menu_master[1];
                        ?>
                            <?php if(count($data_menu[$key]) == 1){?>
                                <li <?= ($master_uri == $data_menu[$key][0]['url'] ? "class='active'" : "") ?>>
                                    <a href="<?= $data_menu[$key][0]['url']?>">
                                        <i class="<?= $menu_logo?>"></i> <span><?= $menu_name?></span>
                                    </a>
                                </li>
                            <?php }else{?>
                                <li <?= ($this->uri->segment(1) == 'Management' || $this->uri->segment(1) == 'management' ? 'class="treeview active"' : 'class="treeview"')?>>
                                    <a href="javascript:;"><i class="<?= $menu_logo?>"></i><span><?= $menu_name?></span>
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <?php
                                        $data_sub_menu = $data_menu[$key];
                                        foreach ($data_sub_menu as $key_sub) {
                                        ?>
                                        <li <?= ($master_uri == $key_sub['url'] ? "class='active'" : "") ?>>
                                            <a href="<?= $key_sub['url']?>">
                                                <i class="<?= $key_sub['icon']?>"></i> <span><?= $key_sub['name']?></span>
                                            </a>
                                        </li>
                                        <?php }?>       
                                    </ul>
                                </li>
                            <?php }?>
                        <?php
                        }
                        ?>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <div id="loading" style="display: flex;justify-content: center;align-items: center;width: 100%;position: absolute;height: 100vh;z-index: 1000;left: 0;">
                    <img src="<?php echo base_url();?>assets/loading.gif">
                </div>
                <!-- Content Header (Page header) -->
                <?php $this->load->view($content); ?>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.1.0
                </div>
                <strong>Developed By </strong>PT Sinergi Informatika Semen Indonesia
            </footer>
            <!-- Control Sidebar -->
            
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
        <!-- jQuery 3 -->
<!--        <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>-->
        <!-- Bootstrap 3.3.7 -->
        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url(); ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
        <!-- Sparkline -->
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
        <!-- jvectormap  -->
        <!-- SlimScroll -->
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- ChartJS -->
        <!-- FastClick -->
        <script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/moment/min/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <!-- AdminLTE for demo purposes -->
        <!-- DataTables -->
        <!-- DataTables -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
        <script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        
        

        <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/pace/pace.min.css">
        <script src="<?php echo base_url(); ?>assets/plugins/pace/pace.min.js"></script> -->
        <script type="text/javascript">
            var $loading = $('#loading').hide();
           //Attach the event handler to any element
           $(document)
             .ajaxStart(function () {
                //ajax request went so show the loading image
                 $loading.show();
             })
           .ajaxStop(function () {
               //got response so hide the loading image
                $loading.hide();
            });
        </script>

    </body>
</html>