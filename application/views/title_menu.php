<!-- <?php //list($REVNR,$ESN,$EO,$WORKSCOPE,$CONTRACTUAL_TAT,$INDUCTION_DATE,$CURRENT_TAT,$COMPANY_NAME,$REVBD,$REVED,$TYPE) = $listProject; ?> -->
<div class="container-fluid">
<br>
<h4 align="center"><u><?php echo $title; ?></u></br>Project ESN : <?php echo $SERNR; ?></h4>

<div class="row">
	<div class="col-lg-12 col-xs-12" style="padding:0">
	  <div class="box box-solid">
		  <div class="box">
			  <div class="container-fluid" style="padding: 10px 15px;">
			  <div class="col-lg-12 col-xs-2 " style="padding:0">
				  <table  class="stripe row-border" width="100%">
				  	<col width="10%">
  					<col width="2%">
  					<col width="48%">
  					<col width="10%">
  					<col width="2%">
  					<col width="28%">
					<tbody>
						<tr style="line-height:24px">
							<td><b>Engine Owner</b></td>
							<td>:</td>
							<td><?= $TEAM_EO; ?></td>
							<td><b>Customer</b></td>
							<td>:</td>
							<td><?= $COMPANY_NAME ?></td>
						</tr>
						<tr style="line-height:24px">
							<td><b>Workscope</b></td>
							<td>:</td>
							<td style="text-align: top center;"><?= $REVTY; ?></td>
							<td><b>Type</b></td>
							<td>:</td>
							<td><?= $ATWRT ?> ( <?= $ENGINE_APU; ?> )</td>
						</tr>
						<tr  style="line-height:24px">
							<td><b><?= ($type == "open" ? "Induction Date" : "Serviceable Date")?></b></td>
							<td>:</td>
							<?php if($type == "open"){?>
							<td><?php if($INDUCT_DATE != null && $INDUCT_DATE != "" && $INDUCT_DATE != "00000000"){ $date = new DateTime($INDUCT_DATE); echo $date->format('d F Y');} ?></td>
							<?php }else{?>
							<td><?php if($ATSDATE_SMR != null && $ATSDATE_SMR != "" && $ATSDATE_SMR != "00000000"){ $date = new DateTime($ATSDATE_SMR); echo $date->format('d F Y');} ?></td>
							<?php }?>
							<td><b>Contractual TAT</b></td>
							<td>:</td>
							<td><?= $CONTRACTUAL_TAT; ?></td>
						</tr>
					</tbody>
				</table>
				</div>
				 </div>
				</div>
		  </div>
	  </div>
	</div>
</div>
