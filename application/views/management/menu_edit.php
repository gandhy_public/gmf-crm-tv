<style >
table {
  /* display: block; */
  width: 100%;
}
</style>

<section class="content-header">
    <h4>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h4>
</section>

<!-- Main content -->
<section class="content">
  
  
  <div class="row">
    <div class="col-md-12">
      <div class="nav-tabs-custom">
        <!-- <?php //$this->load->view($nav_tabs); ?> -->
        <!-- /.Grocery CRUD -->
        <div class="tab-content">
          <section class="content">
            <!-- <div class="box-header">
              
            </div> -->
            <div class="box-body">
              
              <div class="section">
                <a href="<?= base_url()?>index.php/Management/Menu"><button type="button" class="btn btn-primary pull-right">Back To List Group</button></a>
                <div class="col-lg-12 col-md-12 col-xs-12">
                  <div>

                   <!-- Nav tabs -->
                   <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#content_group" aria-controls="profile" role="tab" data-toggle="tab"><?= $title_tab?></a></li>
                     <!-- <li role="presentation" ><a href="#list_menu" aria-controls="home" role="tab" data-toggle="tab">List Menu</a></li> -->  
                   </ul>

                   <!-- Tab panes -->
                   <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade in active" id="content_group">
                        <div class="box">
                          <div class="box-body">
                            <form action="<?= base_url()."index.php/api/management/UpdateGroup"?>" method="post" id="form-edit">
                              <div class="col-lg-12 col-xs-12">
                                <div class="form-horizontal">
                                  <input type="hidden" class="form-control input-sm" id="id_group" name="id_group" required>
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Status Group</label>
                                    <div class="col-sm-6">
                                      <select class="form-control input-sm" id="status" name="status" required="">
                                        <option value="1">Active</option>
                                        <option value="0">Non-Active</option>
                                      </select>
                                    </div>
                                  </div>
                                  <hr>
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Name Group</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control input-sm" id="group" name="group" required> 
                                    </div>
                                  </div>
                                  <?php foreach($modul as $modul){?>

                                    <div class="form-group">
                                      <label class="col-sm-3 control-label">Select Menu in Module <?= $modul?></label>
                                      <div class="col-sm-6">   
                                        <select class="js-example-placeholder-multiple js-states form-control modul_menu" name="menu[]" id="<?= $modul?>" multiple="multiple">
                                          <?php foreach($menu[$modul] as $key){?>
                                          <option value="<?= $key['MODUL_ID']."_".$key['MENU_ID']?>" title="<?= $key['MENU_TOP']?>"><?= $key['MENU_NAME']?></option>
                                          <?php }?>
                                        </select>
                                      </div>
                                    </div>


                                    <?php foreach($menu[$modul] as $top){ if($top['MENU_TOP']){?>
                                      <div class="form-group tab-menu <?= str_replace(' ', '-',  $top['MENU_NAME'])?>">
                                        <label class="col-sm-3 control-label">Top Menu in <?= $top['MENU_NAME']?></label>
                                        <div class="col-sm-6">
                                          <?php foreach($menu_top[$top['MENU_NAME']] as $key){?>
                                          <input type="checkbox" name="menu_top[]" value="<?= $key['MODUL_ID']."_".$key['MENU_ID']."_".$key['MENU_TOP_ID']?>"> <?= $key['MENU_TOP_NAME']?>
                                          <?php }?>
                                        </div>
                                      </div>
                                    <?php }}?>


                                  <?php }?>
                                  
                                  <div class="form-group">
                                    <div class="col-sm-4">
                                      <button type="submit" class="btn btn-success pull-right">Update Group</button>      
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                   </div>
                 </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    <!-- /.col -->
  </div>

</section>
<?php $this->load->view($script)?>
