<style >
table {
  /* display: block; */
  width: 100%;
}
</style>

<section class="content-header">
    <h4>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h4>
</section>

<!-- Main content -->
<section class="content">
  
  
  <div class="row">
    <div class="col-md-12">
      <div class="nav-tabs-custom">
        <!-- <?php //$this->load->view($nav_tabs); ?> -->
        <!-- /.Grocery CRUD -->
        <div class="tab-content">
          <section class="content">
            <!-- <div class="box-header">
              
            </div> -->
            <div class="box-body">
              
              <div class="section">
                <div class="col-lg-12 col-md-12 col-xs-12">
                  <div>

                   <!-- Nav tabs -->
                   <ul class="nav nav-tabs" role="tablist">
                      <li role="presentation" class="active"><a href="#list_user" aria-controls="profile" role="tab" data-toggle="tab">List User</a></li>
                      <?php if($editable){?>
                      <li role="presentation" ><a href="#create_group" aria-controls="home" role="tab" data-toggle="tab">Create User</a></li>  
                      <?php }?>
                     <!-- <li role="presentation" ><a href="#list_menu" aria-controls="home" role="tab" data-toggle="tab">List Menu</a></li> -->  
                   </ul>

                   <!-- Tab panes -->
                   <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade in active" id="list_user">
                       <div class="box">
                         <div class="box-body">
                           <table id="tbUser" class="table table-bordered table-hover table-striped" width="100%">
                             <thead style="background-color: #3c8dbc; color:#ffffff;">
                               <tr>
                                 <th><center> Name </center></th>
                                 <th><center> Username </center></th>
                                 <th><center> User Role </center></th>
                                 <th><center> User Group </center></th>
                                 <th><center> Status </center></th>
                                 <th><center> Act </center></th>
                               </tr>
                             </thead>
                             <tbody>
                                
                             </tbody>
                           </table>
                         </div>
                       </div>
                      </div>

                      <div role="tabpanel" class="tab-pane fade" id="create_group">
                        <div class="box">
                          <div class="box-body">
                            <form action="<?= base_url()."index.php/api/management/CreateUser"?>" method="post">
                              <div class="col-lg-12 col-xs-12">
                                <div class="form-horizontal">
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-4">
                                      <input type="text" class="form-control input-sm" id="nama" name="nama" required> 
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">Username</label>
                                    <div class="col-sm-4">
                                      <input type="text" class="form-control input-sm" id="username" name="username" required> 
                                      <input type="checkbox" id="ldap" name="ldap"> Is LDAP ? 
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">Password</label>
                                    <div class="col-sm-4">
                                      <input type="password" class="form-control input-sm" id="password" name="password"> 
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">User Role</label>
                                    <div class="col-sm-4">
                                      <select class="form-control input-sm" id="role" name="role" style="width: 100%;" required>
                                        <?php 
                                          foreach ($role as $data) { 
                                        ?>
                                        <option value="<?= $data->ID ?>" data-x="<?= $data->LEVEL?>"><?= $data->USER_ROLE; ?></option>
                                        <?php } ?>
                                      </select> 
                                    </div>
                                  </div>
                                  <div class="form-group select-customer">
                                    <label class="col-sm-2 control-label">Customer</label>
                                    <div class="col-sm-4">
                                      <select class="form-control input-sm select2" id="customer" name="customer" style="width: 100%;">
                                        <option value="" disabled selected>Select your customer</option>
                                        <?php 
                                          foreach ($customer as $data) { 
                                        ?>
                                        <option value="<?= $data->ID_CUSTOMER ?>"><?= $data->COMPANY_NAME; ?></option>
                                        <?php } ?>
                                      </select>         
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">User Group</label>
                                    <div class="col-sm-4">
                                      <select class="form-control input-sm select2" id="group" name="group" style="width: 100%;" required>
                                        <option value="" disabled selected>Select your user group</option>
                                        <?php 
                                          foreach ($group as $data) { 
                                        ?>
                                        <option value="<?= $data->id ?>"><?= $data->name; ?></option>
                                        <?php } ?>
                                      </select>    
                                    </div>
                                  </div>
                                  <div id="setting_menu">
                                  <hr>
                                    Setting Editable in Sidebar Menu. Default setting just VIEW
                                    <br><br>
                                    <div id="isi_setting_menu">
                                      
                                    </div>
                                  </div>
                                  <div id="setting_menu_top">
                                  <hr>
                                    Setting Editable in Tab Menu. Default setting just VIEW
                                    <br><br>
                                    <div id="isi_setting_menu_top">
                                      
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="col-sm-4">
                                      <button type="submit" class="btn btn-success pull-right">Create User</button>      
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                   </div>
                 </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    <!-- /.col -->
  </div>

</section>

<?php $this->load->view($modal)?>
<?php $this->load->view($script)?>
