<script>
  $(document).ready(function(){
    var table = $('#tbGroup').DataTable({
        serverSide: true,
        processing: true,
        ajax: {
            url: "<?=base_url('index.php/api/Management/ListGroup')?>",
            type: "POST",
            data: {
              editable:"<?=$editable?>"
            }
        },
        columns: [            
            {data: 'no', name: 'no', orderable: false},            
            {data: 'group', name: 'group', orderable: false},
            {data: 'status', name: 'status', orderable: true},
            {data: 'act', name: 'act',orderable: false}
        ],
    });

    <?php if($this->session->flashdata('success')){?>
      $.notify("<?= $this->session->flashdata('success')?>", "success");
    <?php }elseif($this->session->flashdata('fail')){?>
      $.notify("<?= $this->session->flashdata('fail')?>", "error");
    <?php }?>
    
    $('.tab-menu').hide();
    $('.modul_menu, .modul_menu_edit').select2({
      placeholder: "Select a menu",
      multiple: true,
      width: '100%',
    });

    $('.modul_menu').on('select2:selecting', function(e) {
      if(e.params.args.data.title){
        var nama_class = e.params.args.data.text;
        nama_class = nama_class.replace(" ","-"); 
        $('.'+nama_class).show();
      }
    });
    $('.modul_menu').on('select2:unselecting', function (e) {
      if(e.params.args.data.title){
        var nama_class = e.params.args.data.text;
        nama_class = nama_class.replace(" ","-"); 
        $('.'+nama_class).hide();
      }
    });




    
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>