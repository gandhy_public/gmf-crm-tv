<script>
  $(document).ready(function(){
    var editable = "<?= $editable?>";
    if(!editable){
      $("#form-edit :input").prop("disabled", true);
      $(".btn-success").hide();
    }
    
    <?php if($this->session->flashdata('success')){?>
      $.notify("<?= $this->session->flashdata('success')?>", "success");
    <?php }elseif($this->session->flashdata('fail')){?>
      $.notify("<?= $this->session->flashdata('fail')?>", "error");
    <?php }?>
    
    $('.tab-menu').hide();
    $('.modul_menu, .modul_menu_edit').select2({
      placeholder: "Select a menu",
      multiple: true,
      width: '100%',
    });

    $('.modul_menu').on('select2:selecting', function(e) {
      if(e.params.args.data.title){
        var nama_class = e.params.args.data.text;
        nama_class = nama_class.replace(" ","-"); 
        $('.'+nama_class).show();
        $('.'+nama_class).find("input").prop('checked', false);
      }
    });
    $('.modul_menu').on('select2:unselecting', function (e) {
      if(e.params.args.data.title){
        var nama_class = e.params.args.data.text;
        nama_class = nama_class.replace(" ","-"); 
        $('.'+nama_class).hide();
        $('.'+nama_class).find("input").prop('checked', false);
      }
    });

    $.ajax({
      url: '<?= base_url('index.php/api/Management/GetGroup'); ?>',
      type: 'POST',
      data: {
        id_group: <?= $id_group?>
      },
      success: function(resp) {
        resp = JSON.parse(resp);

        if(resp.status){
          //Set Select Menu
          $('select[name="menu[]"]').select2('val', [resp.menu]);

          //Set Select Menu Top
          if(resp.menu_top){
            $.each(resp.menu_top, function(key, item){
              nama_class = key.replace(" ","-"); 
              $('.'+nama_class).show();
            });
            $('input[name="menu_top[]"]').val(resp.menu_top.value);
          }

          //Set Detail Group
          $('#group').val(resp.group[0].name);
          $('#status').val(resp.group[0].is_active);
          $('#id_group').val(resp.group[0].id);
        }else{
          $.notify(resp.message, "error");
          setTimeout(function(){ window.location = "<?= base_url('index.php')?>"; }, 2000);
        }
      }
    });

  });
</script>