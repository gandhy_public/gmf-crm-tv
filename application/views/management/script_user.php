<script>
  $(function () {
    <?php if($this->session->flashdata('success')){?>
      $.notify("<?= $this->session->flashdata('success')?>", "success");
    <?php }elseif($this->session->flashdata('fail')){?>
      $.notify("<?= $this->session->flashdata('fail')?>", "error");
    <?php }?>

    $('#customer').select2();
    $('#customer_edit').select2();
    $('#group').select2();
    $('#group_edit').select2();
    $('.select-customer').hide();
    $('.select-customer-edit').hide();
    $('#setting_menu').hide();
    $('#setting_menu_edit').hide();
    $('#setting_menu_top').hide();
    $('#setting_menu_top_edit').hide();

    var table_user = $('#tbUser').DataTable({
        serverSide: true,
        processing: true,
        ajax: {
            url: "<?=base_url('index.php/api/Management/ListUser')?>",
            type: "POST",
            data: {
              editable:"<?=$editable?>"
            }
        },
        columns: [                       
            {data: 'name', name: 'name'},
            {data: 'username', name: 'username'},
            {data: 'role', name: 'role'},
            {data: 'group', name: 'group'},
            {data: 'status', name: 'status'},
            {data: 'act', name: 'act',orderable: false}
        ],
    });

    table_user.on("click",".btn_edit",function(){
      var id_user = $(this).data('x');
      $('#modalEditUser').modal('show');
      $('.loading').text("Loading...");
      $('.loading').show();
      $('.modal-body').hide();
      $.ajax({
        url : '<?= base_url()?>index.php/api/Management/GetUser',
        type : 'POST',
        data : {
          id_user : id_user
        },
        success : function(resp){
          resp = JSON.parse(resp);
          if(resp.status){
            $('.loading').hide();
            $('.modal-body').show();

            $('#id_edit').val(resp.data[0].ID_USER);
            $('#status_edit').val(resp.data[0].STATUS);
            $('#nama_edit').val(resp.data[0].NAME);
            $('#username_edit').val(resp.data[0].USERNAME);
            if(resp.data[0].PASSWORD == "using ldap"){
              $("#ldap_edit").text("User using LDAP");
            }else{
              $("#ldap_edit").text("");
            }
            $("#role_edit").val(resp.data[0].ROLE_ID).change();
            if(resp.data[0].LEVEL == 3){
              $('.select-customer-edit').show();
              $("#customer_edit").select2().val(resp.data[0].CUSTOMER_ID).trigger('change.select2');
            }else{
              $('.select-customer-edit').hide();
              $("#customer_edit").val("").trigger('change');
            }
            $("#group_edit").select2().val(resp.data[0].GROUP_ID).trigger('change.select2');
            get_editable(resp.data[0].GROUP_ID,"_edit",resp.menu,resp.menu_top);
            $('.btn-submit').prop("disabled",false);
          }else{
            $('.loading').text('Fail get data user!');
            $('.loading').show();
            $('.modal-body').hide();
          }
        }
      });
    });

    table_user.on("click",".btn_password",function(){
      var id_user = $(this).data('x');
      $('#modalEditPassword').modal("show");
      $('#password_edit').val("");
      $('#repassword_edit').val("");
      $('#id_password').val(id_user);
    });

    $("#formEditPassword button[type='submit']").on("click", function(e){
       e.preventDefault();
       var pass = $('#password_edit').val();
       var repass = $('#repassword_edit').val();
       if(pass == "" && repass == ""){
        $('#alert_password').show().text("Password and Repassword is required");
        return
       }
       if(pass != repass){
        $('#alert_password').show().text("Password and Repassword must be same");
       }else{
        $(this).parents('#formEditPassword').submit();
       }
    })
  });

  $("#ldap").change(function() {
    if(this.checked) {
      $('#password').prop('disabled', true);
    }else{
      $('#password').prop('disabled', false);
    }
  });

  $('#role').on('change', function() {
    var role_id = $('#role option:selected').attr('data-x');
    if(role_id == 3){
      $('.select-customer').show();
      $("#customer").select2().val("").trigger('change.select2').prop("required",true);
    }else if(role_id != 3){
      $('.select-customer').hide();
      $("#customer").select2().val("").trigger('change.select2').prop("required",false);
    }else{
      $('.select-customer').hide();
      $("#customer").select2().val("").trigger('change.select2').prop("required",false);
    }
  });
  $('#role_edit').on('change', function() {
    var role_id = $('#role_edit option:selected').attr('data-x');
    if(role_id == 3){
      $('.select-customer-edit').show();
      $("#customer_edit").select2().val("").trigger('change.select2').prop("required",true);
    }else if(role_id != 3){
      $('.select-customer-edit').hide();
      $("#customer_edit").select2().val("").trigger('change.select2').prop("required",false);
    }else{
      $('.select-customer-edit').hide();
      $("#customer_edit").select2().val("").trigger('change.select2').prop("required",false);
    }
  });

  $('#group').on('change', function() {
    var group = $('#group').val();
    get_editable(group);
  });
  $('#group_edit').change(function() {
    var group = $('#group_edit').val();
    get_editable(group,"_edit");
  });

  /*=================================================================================================================*/

  function get_editable(group,type = "",menu = "",menu_top = ""){
    $.ajax({
      url: '<?= base_url('index.php/api/Management/GetEditable'); ?>',
      type: 'POST',
      data: {
        id_group: group
      },
      success: function(resp) {
        if(resp){
          resp = JSON.parse(resp);
          var data_menu = resp.menu;
          var data_menu_top = resp.menu_top;
          var html = "";
          
          if(data_menu.length > 0){
            $.each(data_menu, function(key,val) { 
              html += '<div class="form-group">'+
                '<label class="col-sm-2 control-label">'+ val.menu_name +'</label>'+
                '<div class="col-sm-4">'+
                  '<input type="checkbox" id="config_menu_'+val.menu_id+'" name="config_menu[]" value="'+ val.menu_id +'"> is EDIT/WRITE'+
                '</div>'+
              '</div>';                 
            });
            $('#isi_setting_menu'+type+'').html(html);
            $('#setting_menu'+type+'').show();
          }else{
            $('#isi_setting_menu'+type+'').html("");
            $('#setting_menu'+type+'').hide();
          }

          if(data_menu_top.length > 0){
            html = "";
            $.each(data_menu_top, function(key,val) { 
              html += '<div class="form-group">'+
                '<label class="col-sm-2 control-label">'+ val.menu_top_name +' ('+ val.menu_name +')</label>'+
                '<div class="col-sm-4">'+
                  '<input type="checkbox" id="config_menu_top_'+val.menu_top_id+'" name="config_menu_top[]" value="'+ val.menu_top_id +'"> is EDIT/WRITE'+
                '</div>'+
              '</div>';                 
            });
            $('#isi_setting_menu_top'+type+'').html(html);
            $('#setting_menu_top'+type+'').show();
          }else{
            $('#isi_setting_menu_top'+type+'').html("");
            $('#setting_menu_top'+type+'').hide();
          }

          if(menu != ""){
            $.each(menu, function(i, item) {
              $("#config_menu_"+item.menu_id+"").attr("checked",true);
            });
          }

          if(menu_top != ""){
            $.each(menu_top, function(i, item) {
              $("#config_menu_top_"+item.menu_top_id+"").attr("checked",true);
            });
          }
        }else{
          $.notify("Failed get setting editabel", "error");
        }
      }
    });
  }
</script>