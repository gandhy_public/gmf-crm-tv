<!-- Form Edit Group Modal -->
<div class="modal fade" id="modalEditUser" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle">Form Edit User</h3>
      </div>
      <form id="formEditUser" method="post" action="<?= base_url('index.php/api/Management/UpdateUser')?>">
        <p class="loading">Loading...</p>
        <div class="modal-body col-sm-12" style="display: none;">
          <input type="hidden" id="id_edit" name="id">
          <div class="col-lg-12 col-xs-12">
            <div class="form-horizontal">
              <div class="form-group">
                <label class="col-sm-2 control-label">Status User</label>
                <div class="col-sm-10">
                  <select class="form-control input-sm" id="status_edit" name="status" style="width: 100%;" required>
                    <option value="1">Active</option>
                    <option value="0">Non Active</option>
                  </select> 
                </div>
              </div>
              <hr>
              <div class="form-group">
                <label class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control input-sm" id="nama_edit" name="nama" required> 
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Username</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control input-sm" id="username_edit" name="username" required> 
                  <p id="ldap_edit"></p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">User Role</label>
                <div class="col-sm-10">
                  <select class="form-control input-sm" id="role_edit" name="role" style="width: 100%;" required>
                    <option value="" disabled selected>Choose User Role</option>
                    <?php 
                      foreach ($role as $data) { 
                    ?>
                    <option value="<?= $data->ID ?>" data-x="<?= $data->LEVEL?>"><?= $data->USER_ROLE; ?></option>
                    <?php } ?>
                  </select> 
                </div>
              </div>
              <div class="form-group select-customer-edit">
                <label class="col-sm-2 control-label">Customer</label>
                <div class="col-sm-10">
                  <select class="form-control input-sm select2" id="customer_edit" name="customer" style="width: 100%;" required>
                    <?php 
                      foreach ($customer as $data) { 
                    ?>
                    <option value="<?= $data->ID_CUSTOMER ?>"><?= $data->COMPANY_NAME; ?></option>
                    <?php } ?>
                  </select>         
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">User Group</label>
                <div class="col-sm-10">
                  <select class="form-control input-sm select2" id="group_edit" name="group" style="width: 100%;" required>
                    <?php 
                      foreach ($group as $data) { 
                    ?>
                    <option value="<?= $data->id ?>"><?= $data->name; ?></option>
                    <?php } ?>
                  </select>    
                </div>
              </div>
              <div id="setting_menu_edit">
              <hr>
                Setting Editable in Sidebar Menu. Default setting just VIEW
                <br><br>
                <div id="isi_setting_menu_edit">
                  
                </div>
              </div>
              <div id="setting_menu_top_edit">
              <hr>
                Setting Editable in Tab Menu. Default setting just VIEW
                <br><br>
                <div id="isi_setting_menu_top_edit">
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary btn-submit" disabled>Update User</button>
        </div>
      </form>
    </div>
  </div>
</div>





<div class="modal fade" id="modalEditPassword" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle">Form Edit Password User</h3>
      </div>
      <form id="formEditPassword" method="post" action="<?= base_url('index.php/api/Management/UpdatePassword')?>">
        <div class="modal-body col-sm-12">
          <input type="hidden" id="id_password" name="id">
          <div class="col-lg-12 col-xs-12">
            <div class="form-horizontal">
              <p style="color: red;display: none" id="alert_password">Hello</p>
              <div class="form-group">
                <label class="col-sm-2 control-label">New Password</label>
                <div class="col-sm-10">
                  <input type="password" class="form-control input-sm" id="password_edit" name="password" required> 
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Retype New Password</label>
                <div class="col-sm-10">
                  <input type="password" class="form-control input-sm" id="repassword_edit" name="repassword" required> 
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary btn-submit">Update Password User</button>
        </div>
      </form>
    </div>
  </div>
</div>