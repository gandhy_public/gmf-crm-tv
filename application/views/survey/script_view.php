<script>
$(document).on('ready', function(){
	<?php if($this->session->flashdata('success')){?>
      //$.notify("<?= $this->session->flashdata('success')?>", "success");
      swal("Success", "Thank You for Submitting Post Project Feedback", "success");
    <?php }elseif($this->session->flashdata('fail')){?>
      //$.notify("<?= $this->session->flashdata('fail')?>", "error");
      swal("Success", "Fail Submitting Post Project Feedback", "error");
    <?php }?>

    $('.rating').rating();

    function getSum(elmt){
    	var sum = 0;
		for( var i = 0; i < elmt.length; i++ ){
		    sum += elmt[i]; //don't forget to add the base
		}
		return sum;
	}

    var val_array = [0, 0, 0, 0, 0];
    var avg = 0;
    $('.rating').on("change",function(){
    	var value = parseFloat($(this).val());
    	var key = $(this).data('x');

    	val_array[key] = value;
    	console.log(val_array);
    	var sum = getSum(val_array)
    	avg = sum/val_array.length;
    	$('#average').text(avg);
    	$('#input_avg').val(avg);
    });

    $.ajax({
	    url: "<?= base_url('index.php/api/survey/GetListSurvey'); ?>",
	    type: "POST",
	    data: {
	        sernr: <?= $SERNR?>
	    },
	    success: function(resp){
	    	if(resp){
	    		$("#form-survey :input").prop("disabled", true);
	    	}else{
	    		$("#form-survey :input").prop("disabled", false);
	    	}
	    }
	});

	$('form button[type="submit"]').on('click', function (e) {
		e.preventDefault();
		swal({
			title: "Are you sure want to Submit?",
			text: "Project : <?= $SERNR?>\nScore : "+avg,
			icon: "warning",
			buttons: true,
			dangerMode: true,
		})
		.then((willDelete) => {
		  if (willDelete) {
		    $(this).parents('form').submit();
		  }
		});
	});
});
</script>