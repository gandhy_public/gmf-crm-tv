<style type="text/css">
    .content-header {
        font-family: "Helvetica";
        src: url("assets/bower_components/font-awesome/fonts/HelveticaLTStd-Roman_0.otf");
    }

    .content {
        font-family: "Helvetica";
        src: url("assets/bower_components/font-awesome/fonts/HelveticaLTStd-Roman_0.otf");
    }
</style>

<section class="content-header">
    <div class="panel panel-default">
        <h1 align="center">
            <u><?= $title?></u>
        </h1>
    </div>
</section>

<section class="content">

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <a href="<?= base_url()?>index.php/export/survey/all/excel" target="_blank"><button class="btn btn-success pull-right" type="button"> Export To EXCELL </button></a>
                </div>
                <div class="box-body">
                    <div class=" table-responsive ">
                        <table id="tbSurvey" class="table table-responsive table-bordered table-striped" style="width: 100%">
                            <thead>
                                <th><center>ESN</center></th>
                                <th><center>EO</center></th>
                                <th><center>WORKSCOPE</center></th>
                                <th><center>CUSTOMER</center></th>
                                <th><center>RESPONDER</center></th>
                                <th><center>SERVICEABLE DATE</center></th>
                                <th><center>FEEDBACK SCORE</center></th>
                                <th><center>ACTION</center></th>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
        <!-- /.col -->
    </div>

</section>

<?php $this->load->view($script)?>
