<script type="text/javascript">
    $(document).ready(function () {

        var table = $('#tbSurvey').DataTable({
            processing: true,
            serverSide: true,
            paging: true,
            sScrollX: true,
            ajax: {
                url: "<?= base_url('index.php/api/Survey/GetListSurvey')?>",
                type: "POST",
            },

            columns: [   
                {data: 'esn', name: 'esn', orderable: false},
                {data: 'eo', name: 'eo', orderable: false},            
                {data: 'workscope', name: 'workscope', orderable: false},
                {data: 'customer', name: 'customer', orderable: false},
                {data: 'responder', name: 'responder', orderable: false},
                {data: 'serviceable', name: 'serviceable', orderable: false},
                {data: 'score', name: 'score', orderable: false},
                {data: 'action', name: 'action', orderable: false}
            ]
        });
        
    });
</script>