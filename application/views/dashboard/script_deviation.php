<script type="text/javascript">
    $(document).ready(function () {

        var table = $('#tbdevpoint').DataTable({
            processing: true,
            serverSide: true,
            paging: true,
            ajax: {
                url: "<?= base_url('index.php/api/dashboard/GetListDeviation'); ?>",
                type: "POST",
                data: {
                    docno:<?= $docno?>,
                    revnr:<?= $revnr?>
                }
            },
            columns: [              
                {data: 'no', name: 'no', orderable: false},
                {data: 'dev_no', name: 'dev_no', orderable: false},
                {data: 'eng_tat', name: 'eng_tat', orderable: false},
                {data: 'cus_tat', name: 'cus_tat', orderable: false},
                {data: 'prc_tat_increase', name: 'prc_tat_increase', orderable: false},
                {data: 'current_gate', name: 'current_gate', orderable: false},
                {data: 'next_gate_eff', name: 'next_gate_eff', orderable: false},
                {data: 'dev_reason', name: 'dev_reason', orderable: false},
                {data: 'prc_delayed', name: 'prc_delayed', orderable: false},
                {data: 'department', name: 'department', orderable: false},
                {data: 'root_cause', name: 'root_cause', orderable: false},
                {data: 'corrective_action', name: 'corrective_action', orderable: false},
                {data: 'created_by', name: 'created_by', orderable: false},
                {data: 'created_date', name: 'created_date', orderable: false}
            ]
        });

        $.ajax({
            url: "<?= base_url('index.php/api/dashboard/GetChartDeviation'); ?>",
            type: "POST",
            data: {
                docno: <?= $docno?>
            },
            success: function(resp){
                resp = JSON.parse(resp);
                renderChart(resp.count_gate);
            }
        });

        function renderChart(data){
          var color = Chart.helpers.color;
          new Chart(document.getElementById("bar-chart-horizontal"), {
              type: 'horizontalBar',
              data: {
                labels: ["GATE 1", "GATE 2", "GATE 3", "GATE 4", "GATE 5", "GATE 6", "GATE 7", "GATE 8", "GATE 9"],
                datasets: [
                  {
                    label: "Population (millions)",
                    backgroundColor:  ['#ccebff','#ffcccc','#ffd1b3','#ffeb99','#e6ccff','#e6fffa','#e0e0d1','#b3ffb3','##c2d6d6'],
                    borderColor:['#66c2ff','#ff6666','#ff751a','#e6b800','#a64dff','#adad85','#1affd1','#4dff4d','#527a7a'],
                    borderWidth : 1,
                    hoverBackgroundColor:['#66c2ff','#ff6666','#ff751a','#e6b800','#a64dff','#adad85','#1affd1','#4dff4d','#527a7a'],
                    hoverBorderColor :['#ccebff','#ffcccc','#ffd1b3','#ffeb99','#e6ccff','#e6fffa','#e0e0d1','#b3ffb3','##c2d6d6'],
                    data: data
                  }
                ]
              },
              options: {
                legend: { display: false },
                title: {
                  display: true,
                  text: 'Deviation Total'
                }
              }
          });
        }

    });
</script>
