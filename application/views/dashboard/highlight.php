<section class="content">
<?php
$this->load->view($header_menu);
$this->load->view($title_menu);
?>
  <div class="row">
    <div class="col-md-12">
          <!-- Line chart -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-bar-chart-o"></i>
          <div style="width:50%; margin:0 auto;">
            <canvas id="bar-chart-horizontal" width="800" height="450"></canvas>
          </div>

            <div class="col-sm-12">
                   <br>
                    <table id="tbhightlight" class="table table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <tr>
                           <th>NO</th>
                           <th>HL NO</th>
                           <th>Applicable Module</th>
                           <th>Highlight</th>
                           <th>Status</th>
                           <th>Created By</th>
                           <th>Created Date</th>
                          </tr>
                      </thead>
                    </table>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
<?php $this->load->view($script)?>