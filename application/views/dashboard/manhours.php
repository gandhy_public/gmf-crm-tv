<section class="content">
    <?php
    $this->load->view($header_menu);
    $this->load->view($title_menu);
    ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">

                        <div class="col-md-4">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Total</h3>
                                </div>
                                <div class="box-body chart-responsive">
                                    <div class="chart" id="bar-chart" style="height: 300px;">
                                        <canvas id="bar-chart-grouped" width="800" height="500"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Job Card</h3>
                                </div>
                                <div class="box-body chart-responsive">
                                    <div class="chart" id="bar-chart" style="height: 300px;">
                                        <canvas id="bar-chart-grouped1" width="800" height="500"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title">MDR</h3>
                                </div>
                                <div class="box-body chart-responsive">
                                    <div class="chart" id="bar-chart" style="height: 300px;">
                                        <canvas id="bar-chart-grouped2" width="800" height="500"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12" style="background-color:white;">
                            <div class="col-sm-12 table-responsive">
                                <div>
                                <a href="<?= base_url()?>index.php/export/dashboard/manhours/pdf/<?= $this->uri->segment(4)?>" target="_blank"><button class="btn btn-success" type="button"> Export To PDF </button></a>
                                <a href="<?= base_url()?>index.php/export/dashboard/manhours/excel/<?= $this->uri->segment(4)?>" target="_blank"><button class="btn btn-success" type="button"> Export To EXCEL </button></a>
                                </div>
                                <br>
                                <table id="tv" class="table table-bordered table-hover" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Order Number</th>
                                            <th>Order Type</th>
                                            <th>MAT</th>
                                            <th>Description</th>
                                            <th>Status</th>
                                            <th>Mhrs Plan (Hrs)</th>
                                            <th>Mhrs Actual (Hrs)</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

<?php $this->load->view($script)?>