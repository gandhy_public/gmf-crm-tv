<script>
  $(document).ready(function() {
    <?php if($type == "close" || $editable == FALSE){?>
      $("#form-report :input").attr("disabled", true);
    <?php }?>

    <?php if($this->session->flashdata('success')){?>
      $.notify("<?= $this->session->flashdata('success')?>", "success");
    <?php }elseif($this->session->flashdata('fail')){?>
      $.notify("<?= $this->session->flashdata('fail')?>", "error");
    <?php }?>

    var max = parseInt($('#DETAIL_PROJECT').attr('maxlength'));
    $('.detail_project_input #DETAIL_PROJECT').on('input', function() {
      var total = max - $(this).val().length
      $('.detail_project_input .char-left span').text(total);
      if(total <= 10) {
        $('.detail_project_input .char-left').css('color', 'red');
      } else {
        $('.detail_project_input .char-left').css('color', 'inherit');
      }
    });

    var max_edit = parseInt($('#DETAIL_PROJECT_EDIT').attr('maxlength'));
    $('.detail_project_input_edit #DETAIL_PROJECT_EDIT').on('input', function() {
      var total_edit = max_edit - $(this).val().length
      $('.detail_project_input_edit .char-left span').text(total_edit);
      if(total_edit <= 10) {
        $('.detail_project_input_edit .char-left').css('color', 'red');
      } else {
        $('.detail_project_input_edit .char-left').css('color', 'inherit');
      }
    });

    var table = $('#tbReport').DataTable({
        processing: true,
        serverSide: true,
        paging: true,
        ajax: {
            url: "<?= base_url('index.php/api/dashboard/GetListReport'); ?>",
            type: "POST",
            data: {
              docno: "<?= $docno?>",
              editable:"<?= $editable?>",
              type: "<?= $type?>"
            }
        },
        columns: [              
            {data: 'week', name: 'no', orderable: false},
            {data: 'detail', name: 'dev_no', orderable: false},
            {data: 'date', name: 'eng_tat', orderable: false},
            {data: 'action', name: 'cus_tat', orderable: false}
        ]
    });

    table.on('click', '.btn-delete', function () {
        var id = $(this).data("x");

        swal({
          title: "WARNING !",
          text: "Are you sure to delete this data ?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              type: 'POST',
              url : "<?=  base_url('index.php/api/dashboard/DeleteReport') ?>",
              data: {id : id},
              success: function(res){
                if(res){
                  swal("Data has been deleted!", {
                    icon: "success",
                  });
                }else{
                  swal("Data failed deleted!", {
                    icon: "error",
                  });
                }
                table.ajax.reload();
              }
            });
          }
        });
    });

    table.on('click', '.btn-edit', function () {
        var id = $(this).data("x");

        $.ajax({
          type: 'POST',
          url : "<?=  base_url('index.php/api/dashboard/DetailReport') ?>",
          data: {id : id},
          success: function(resp){
            resp = JSON.parse(resp);
            resp = resp[0];

            $('#modal_edit').modal("show");
            $('#WEEK_EDIT').val(resp.WEEK);
            $('#DETAIL_PROJECT_EDIT').val(resp.DETAIL_PROJECT);
            $('#TOTAL').text(256-resp.DETAIL_PROJECT.length);
            $('#DATE_EDIT').val(resp.DATE_INPUT);
            $('#id').val(id);
          }
        });
    });

  });
</script>