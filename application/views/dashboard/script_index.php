<script type="text/javascript">
    $(document).ready(function () {

        var table = $('#tbProject').DataTable({
            processing: true,
            serverSide: true,
            paging: true,
            sScrollX: true,
            ajax: {
                url: "<?= base_url('index.php/api/dashboard/GetListProject/').$type; ?>",
                type: "POST",
            },
            columns: [   
                {data: 'esn', name: 'esn', orderable: false},
                {data: 'project_no', name: 'project_no', orderable: false},
                {data: 'eo', name: 'eo', orderable: false},
                {data: 'customer', name: 'customer', orderable: false},            
                {data: 'workscope', name: 'workscope', orderable: false},
                {data: 'induction_date', name: 'induction_date', orderable: true},
                {data: 'contractual_tat', name: 'contractual_tat', orderable: true},
                {data: 'current_tat', name: 'current_tat', orderable: true},
                {data: 'action', name: 'action', orderable: true}
            ],
            <?php if($type == "close"){?>
            columnDefs: [
                {
                    "targets": [ 8 ],
                    "visible": false,
                    "searchable": false
                }
            ],
            <?php }?>
            fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                if ( aData['cek'] == "1" ){
                    $('td', nRow).eq(7).css('background-color', '#f1c1c0');
                }
                else{
                    $('td', nRow).eq(7).css('background-color', '#dbecc6');
                }
            }
        });

        table.on('click', '.btn_sync', function () {
            var revnr = $(this).data('x');
            swal({
                title: "Are you sure want to sync with Revision Number "+revnr+"?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                swal({
                  title: 'WARNING!',
                  text: "Don't refresh or reload this page, system is checking process!",
                  icon: 'warning',
                  buttons: false
                });

                sync_revnr(revnr);
              }
            });
        });

        function sync_revnr(revnr){
            $.ajax({
                url: "<?= base_url('index.php/Sapi/create_job_by_revision'); ?>",
                type: "POST",
                data: {
                    revnr: revnr
                },
                success: function(resp){
                    resp = JSON.parse(resp);
                    if(resp.status == "success"){
                        swal({
                            title: 'Sync Succesfully',
                            text: 'Wait for a few minutes, system is processing sync with SAP',
                            icon: 'success',
                        }).then((value) => {
                            table.ajax.reload();
                        });
                        table.ajax.reload();
                    }else{
                        swal({
                            title: 'WARNING !',
                            text: 'System is already sync with other Revision Number, wait for a few minutes',
                            icon: 'warning',
                        }).then((value) => {
                            table.ajax.reload();
                        });
                        table.ajax.reload();
                    } 
                }
            });
        }
    });
</script>