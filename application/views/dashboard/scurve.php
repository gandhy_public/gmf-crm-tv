<section class="content">
    <?php 
    $this->load->view($header_menu); 
    $this->load->view($title_menu); 
    ?>

    	<div class="row">
      		<div class="col-md-12">
      			<div class="box box-primary">
          			<div class="box-header with-border">
            			<i class="fa fa-bar-chart-o"></i>
            			<div style="width:100%; margin:0 auto;">
              			     <canvas id="myChart" width="1366" height="600"></canvas><br>
            			</div>
          			</div>
        		</div>
      		</div>
      	</div>

</section>
<?php $this->load->view($script)?>