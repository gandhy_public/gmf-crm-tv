<section class="content">
    <?php
    $this->load->view($header_menu);
    $this->load->view($title_menu);
    ?>

        <div class="row">
            <div class="col-sm-4">
                <div class="box">
                    <div class="box-body">
                        <div class="info-box">
                            <span class="info-box-icon" style="background-color: #65b32b;"><i class="fa fa-file-text-o"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Actual TAT</span>
                                <span id="actual_tat" class="info-box-number" style="font-size: 20px">0 Days</span>
                            </div>
                        </div>
                        <div class="info-box">
                            <span class="info-box-icon" style="background-color: #f3e812;"><i class="fa fa-file-text"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Foreseen TAT</span>
                                <span id="plan_sv_date" class="info-box-number" style="font-size: 20px">0 Days</span>
                                <span class="info-box-text">Plan Serviceable Date<br></span>
                            </div>
                        </div>
                        <div class="info-box">
                            <span class="info-box-icon" style="background-color: #0dacc1;"><i class="fa fa-list-alt"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Job Card Completion </span>
                                <span id="percentage_jc_closed" class="info-box-number" style="font-size: 20px">0%</span>
                                <span id="total_jc_closed" class="info-box-text">0 of 0</span>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <p>Dissasembly:
                                <div class="progress">
                                    <div id="percentage_dissasembly" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:0%; background-color:#08aebe;">
                                        0%
                                    </div>
                                </div>
                            </p>
                            <p>Inspection:
                                <div class="progress">
                                    <div id="percentage_inspection" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:0%; background-color:#08aebe;">
                                        0%
                                    </div>
                                </div>
                            </p>
                            <p>Repair:
                                <div class="progress">
                                    <div id="percentage_repair" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:0%; background-color:#08aebe;">
                                        0%
                                    </div>
                                </div>
                            </p>
                            <p>Assembly:
                                <div class="progress">
                                    <div id="percentage_assembly" class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:0%; background-color:#08aebe;">
                                        0%
                                    </div>
                                </div>
                            </p>
                            <p>Test:
                                <div class="progress">
                                    <div id="percentage_test" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:0%; background-color:#08aebe;">
                                        0%
                                    </div>
                                </div>
                            </p>
                            <p>QEC:
                                <div class="progress">
                                    <div id="percentage_qec" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:0%; background-color:#08aebe;">
                                        0%
                                    </div>
                                </div>
                            </p>
                            <p>AD/SB:
                                <div class="progress">
                                    <div id="percentage_adsb" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:0%; background-color:#08aebe;">
                                        0%
                                    </div>
                                </div>
                            </p>
                            <p>SPEC PRO:
                                <div class="progress">
                                    <div id="percentage_spec" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:0%; background-color:#08aebe;">
                                        0%
                                    </div>
                                </div>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="col-sm-8">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><b style="font-size: 20px; font-family:Helvetica;">Overall Summary</b></h3><br>
                    </div>
                    <div class="box-body" style="margin-top: -30px;">
                        <div class="row">
                            <div class="col-lg-4">
                                <div id="smallbuddy" style="width:230px; height:130px">
                                    <div id="gauge" class="200x160px"></div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div id="smallbuddy" style="width:230px; height:130px">
                                    <div id="gauge1" class="200x160px"></div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div id="smallbuddy" style="width:230px; height:130px">
                                    <div id="gauge2" class="200x160px"></div>
                                </div><br>
                            </div>
                            <div class="col-sm-12">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><b style="font-size: 20px; font-family:Helvetica;">&nbsp;Job Card List</b></h3>
                                </div>
                                <table id="tbJobcard" class="table table-bordered table-striped" style="width: 100%;"><br>
                                    <thead>
                                        <tr>
                                            <th>MAT</th>
                                            <th>DESCRIPTION</th>
                                            <th>STATUS</th>
                                        </tr>
                                    </thead>
                                </table>   
                            </div>    
                        </div>
                    </div> 
                </div>
            </div>
        </div>

</section>

<?php $this->load->view($script)?>

