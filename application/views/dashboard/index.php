<style type="text/css">
    .content-header {
        font-family: "Helvetica";
        src: url("assets/bower_components/font-awesome/fonts/HelveticaLTStd-Roman_0.otf");
    }

    .content {
        font-family: "Helvetica";
        src: url("assets/bower_components/font-awesome/fonts/HelveticaLTStd-Roman_0.otf");
    }
</style>

<section class="content-header">
    <div class="panel panel-default">
        <h1 align="center">
            <u><?= $title?></u><br>
            Engine Maintenance
        </h1>
    </div>
</section>

<section class="content">

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <a href="<?= base_url()?>index.php/export/dashboard/project/pdf/<?= $type?>" target="_blank"><button class="btn btn-success pull-right" type="button" style="margin-left: 2px"> Export To PDF </button></a>
                    <a href="<?= base_url()?>index.php/export/dashboard/project/excel/<?= $type?>" target="_blank"><button class="btn btn-success pull-right" type="button"> Export To EXCELL </button></a>
                </div>
                <div class="box-body">
                    <div class=" table-responsive ">
                        <table id="tbProject" class="table table-responsive table-bordered table-striped" style="width: 100%">
                            <thead>
                                <th>ESN</th>
                                <th>Project No</th>
                                <th>EO</th>
                                <th>CUSTOMER</th>
                                <th>WORKSCOPE</th>
                                <th><?= ($type == "open" ? "INDUCTION DATE" : "SERVICEABLE DATE")?></th>
                                <th>CONTRACTUAL TAT (Days)</th>
                                <th><?= ($type == "open" ? "CURRENT TAT (Days)" : "ACTUAL TAT (Days)")?></th>
                                <th>ACTION</th>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
        <!-- /.col -->
    </div>

</section>

<?php $this->load->view($script)?>
