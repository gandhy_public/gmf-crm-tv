<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#tbJobcard').DataTable({
            processing: true,
            serverSide: true,
            paging: true,
            sScrollX: true,
            ajax: {
                url: "<?= base_url('index.php/api/dashboard/GetListJobcard'); ?>",
                type: "POST",
                data: {
                    docno:<?= $docno?>,
                    revnr:<?= $revnr?>
                }
            },
            columns: [              
                {data: 'mat', name: 'mat', orderable: false},
                {data: 'description', name: 'description', orderable: false},
                {data: 'status', name: 'status', orderable: false}
            ],
            fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                if ( aData['status'] == "open" ){
                    $('td', nRow).eq(2).css('background-color', '#ffaa80');
                }else if( aData['status'] == "progress" ){
                    $('td', nRow).eq(2).css('background-color', '#33bbff');
                }else if( aData['status'] == "close" ){
                    $('td', nRow).eq(2).css('background-color', '#38e144');
                }
            }
        });
    });
</script>

<script type="text/javascript">
    var g1 = new JustGage({
        id: "gauge",
        value: 0,
        min: 0,
        max: 100,
        symbol: '%',
        levelColors: ["#ffaa80"],
        label: "Open Job Cards",
        levelColorsGradient: false,
    });

    var g2 = new JustGage({
        id: "gauge1",
        value: 0,
        min: 0,
        max: 100,
        symbol: '%',
        levelColors: ["#33bbff"],
        label: "In Progress Job Cards"
    });

    var g3 = new JustGage({
        id: "gauge2",
        value: 0,
        min: 0,
        max: 100,
        symbol: '%',
        levelColors: ["#38e144"],
        label: "Closed Job Cards"
    });
</script>

<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        $.ajax({
            url: '<?= base_url('index.php/api/dashboard/GetDetailOverview'); ?>',
            type: 'POST',
            data: { 
              docno: <?= $docno; ?>,
              revnr: <?= $revnr; ?>
            },
            success: function(response) {
                var response = $.parseJSON(response);
                var data = response.data;
                $('#actual_tat').text(data.ACT_TAT + ' Days');
                $('#plan_sv_date').text(data.PlanSVDate + ' Days');
                $('#percentage_jc_closed').text(data.percentage_jc_closed + '%');
                $('#total_jc_closed').text(data.totalJcClosed + ' of ' + data.totalJobCard);

                $('#percentage_disassembly').text(data.percentage_disassembly + '%');
                $('#percentage_disassembly').css("width", data.percentage_disassembly + '%');
                $('#percentage_inspection').text(data.percentage_inspection + '%');
                $('#percentage_inspection').css("width", data.percentage_inspection + '%');
                $('#percentage_repair').text(data.percentage_repair + '%');
                $('#percentage_repair').css("width", data.percentage_repair + '%');
                $('#percentage_assembly').text(data.percentage_assembly + '%');
                $('#percentage_assembly').css("width", data.percentage_assembly + '%');
                $('#percentage_test').text(data.percentage_test + '%');
                $('#percentage_test').css("width", data.percentage_test + '%');
                $('#percentage_qec').text(data.percentage_qec + '%');
                $('#percentage_qec').css("width", data.percentage_qec + '%');
                $('#percentage_adsb').text(data.percentage_adsb + '%');
                $('#percentage_adsb').css("width", data.percentage_adsb + '%');
                $('#percentage_spec').text(data.percentage_spec + '%');
                $('#percentage_spec').css("width", data.percentage_spec + '%');

                g1.refresh(data.percentage_jc_open);
                g2.refresh(data.percentage_jc_progress);
                g3.refresh(data.percentage_jc_closed);  
            }
        });
    });
</script>