<script>
  $(document).ready(function() {
    $.ajax({
      type:'ajax',
      url: '<?= base_url('index.php/api/dashboard/GetScurve/').$docno.'/'.$revnr; ?>',
      ascync :'false',   
      dataType : 'json',
      type : 'GET',     
      success: function(data){
        var datagstrp = [];
        var dataPlan = [];
        var dataActual = [];
        var lineIndex =0;
        for(var a in data['gstrp']) {
          datagstrp.push(data['gstrp'][a]);
        }
        for(var b in data['plan']) {
          dataPlan.push(data['plan'][b]);
        }
        for(var c in data['actual']) {
          dataActual.push(data['actual'][c]);
        }
        lineIndex= data['lineIndex'];
        var originalLineDraw = Chart.controllers.line.prototype.draw;
        Chart.helpers.extend(Chart.controllers.line.prototype, {
          draw: function() {
            originalLineDraw.apply(this, arguments);
            var chart = this.chart;
            var ctx = chart.chart.ctx;
            var index = chart.config.data.lineAtIndex;
            if (index) {
              var xaxis = chart.scales['x-axis-0'];
              var yaxis = chart.scales['y-axis-0'];
              ctx.save();
              ctx.beginPath();
              ctx.moveTo(xaxis.getPixelForValue(undefined, index), yaxis.top);
              ctx.strokeStyle = '#38e144';
              ctx.lineTo(xaxis.getPixelForValue(undefined, index), yaxis.bottom);
              ctx.stroke();
              ctx.restore();
            }
          }
        });
        var config = {
          type: 'line',
          data: {
            labels: datagstrp,
            datasets: [{
              label: "PLAN",
              data: dataPlan,
              borderColor: "#42a5f6",
              backgroundColor: "#42a5f6",
              fill: false
            }, 
            {
              label: "ACTUAL", 
              data: dataActual,
              borderColor: "#ff3385",
              backgroundColor: "#ff3385",
              fill: false
            },
            {
              label: "TODAY", 
              data: dataActual,
              borderColor: "#38e144",
              backgroundColor: "#38e144",
              fill: false
            }],
            lineAtIndex: lineIndex
          }
        };  
        var ctx = document.getElementById("myChart").getContext("2d");
        new Chart(ctx, config);
      },
      error : function(data) {
      }
    });
  });
</script>