<style type="text/css">
  @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);
  fieldset, label { margin: 0; padding: 0; }
  .rating { 
      border: none;
      float: left;
  }
  .rating > input { display: none; } 
  .rating > label:before { 
      margin: 10px;
      font-size: 1.25em;
      font-family: FontAwesome;
      display: inline-block;
      content: "\f005";
  }
  .rating > .half:before { 
      content: "\f089";
      position: absolute;
  }
  .rating > label { 
      color: #ddd; 
      float: right; 
  }
  /***** CSS Magic to Highlight Stars on Hover *****/

  .rating > input:checked ~ label, /* show gold star when clicked */
  .rating:not(:checked) > label:hover, /* hover current star */
  .rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

  .rating > input:checked + label:hover, /* hover current star when changing rating */
  .rating > input:checked ~ label:hover,
  .rating > label:hover ~ input:checked ~ label, /* lighten current selection */
  .rating > input:checked ~ label:hover ~ label { color: #FFED85;  } 
  .n-padding {
    padding: 0px;
    padding-bottom: 5px;
  }
</style>
<section class="content">
    <?php
    $this->load->view($header_menu);
    $this->load->view($title_menu);
    ?>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-body">
            <div style="width:80%; margin:0 auto;">
              <form action="<?php echo base_url(); ?>index.php/api/Dashboard/CreateSurvey" method="POST" id="form-survey">

                  <input type="hidden" name="SERNR" value="<?= $SERNR?>">
                  <input type="hidden" name="REVNR" value="<?= $revnr?>">
                  <input type="hidden" name="EQUNR" value="<?= $docno?>">
                  <input type="hidden" name="COMPANY_NAME" value="<?= $COMPANY_NAME?>">
                  <input type="hidden" name="EO" value="<?= $TEAM_EO?>">
                  <input type="hidden" name="WORKSCOPE" value="<?= $REVTY?>">
                  <input type="hidden" name="AEDAT" value="<?= $AEDAT?>">

                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-5">
                        <label for="rateServices" style="font-size: 20px;">Communication</label><br>
                        <small class="form-text text-muted" style="font-size: 12px;">Description : Clear communication by the personnel to customer; personnel can be easily contacted</small>
                      </div>

                      <div class="col-sm-7">
                          <div class="row">
                            <div class="col-sm-2" style="padding: 10px; margin: auto">
                              <label style="font-size: 13px;">Score : </label>
                            </div>
                            <div class="col-sm-10">
                              <input name="rating[]" id="rating" data-x='0' class="rating rating-loading" data-size="sm" data-min="0" data-max="5" data-step="0.5" <?= (isset($survey[0]['RATE_KATEGORI_1']) ? "value='".$survey[0]['RATE_KATEGORI_1']."' readonly='readonly'" : "")?> required>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-2" style="padding: 10px; margin: auto">
                              <label style="font-size: 13px;">Comments : </label>
                            </div>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name="comment[]" placeholder="Your Answer" value="<?= (isset($survey[0]['COMMENT_KATEGORI_1']) ? $survey[0]['COMMENT_KATEGORI_1'] : "")?>" required>
                            </div>
                          </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <label style="font-size: 13px;">Guidance : </label>
                        <table border="1" width="100%">
                          <thead>
                            <th width="10%"><center>Score</center></th>
                            <th width="90%"><center>Guidance</center></th>
                          </thead>
                          <tbody>
                            <tr>
                              <td><center>1</center></td>
                              <td><center>EO rarely gives project report</center></td>
                            </tr>
                            <tr>
                              <td><center>2</center></td>
                              <td><center>EO gives project report inconsistenly, EO rarely communicate available issues regarding the project</center></td>
                            </tr>
                            <tr>
                              <td><center>3</center></td>
                              <td><center>EO gives project report inconsistenly, EO sometimes communicate available issues regarding the project</center></td>
                            </tr>
                            <tr>
                              <td><center>4</center></td>
                              <td><center>EO gives project report periodically, EO communicate available issues regarding the project</center></td>
                            </tr>
                            <tr>
                              <td><center>5</center></td>
                              <td><center>EO gives project report periodically, EO actively communicate available issues regarding the project</center></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-5">
                        <label for="rateServices" style="font-size: 20px;">Product Quality</label><br>
                        <small class="form-text text-muted" style="font-size: 12px;">Description : Satisfaction with the end quality of work; Work documentation is detail & complete</small>
                      </div>

                      <div class="col-sm-7">
                          <div class="row">
                            <div class="col-sm-2" style="padding: 10px; margin: auto">
                              <label style="font-size: 13px;">Score : </label>
                            </div>
                            <div class="col-sm-10">
                              <input name="rating[]" id="rating" data-x='1' class="rating rating-loading" data-size="sm" data-min="0" data-max="5" data-step="0.5" <?= (isset($survey[0]['RATE_KATEGORI_2']) ? "value='".$survey[0]['RATE_KATEGORI_2']."' readonly='readonly'" : "")?> required>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-2" style="padding: 10px; margin: auto">
                              <label style="font-size: 13px;">Comments : </label>
                            </div>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name="comment[]" placeholder="Your Answer" value="<?= (isset($survey[0]['COMMENT_KATEGORI_2']) ? $survey[0]['COMMENT_KATEGORI_2'] : "")?>" required>
                            </div>
                          </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <label style="font-size: 13px;">Guidance : </label>
                        <table border="1" width="100%">
                          <thead>
                            <th width="10%"><center>Score</center></th>
                            <th width="90%"><center>Guidance</center></th>
                          </thead>
                          <tbody>
                            <tr>
                              <td><center>1</center></td>
                              <td><center>EGT Margin Engine does not fulfill the contract / APU not passed test</center></td>
                            </tr>
                            <tr>
                              <td><center>2</center></td>
                              <td><center>EGT Margin Engine does not fulfill the contract / APU not passed test</center></td>
                            </tr>
                            <tr>
                              <td><center>3</center></td>
                              <td><center>EGT Margin Engine is tight / APU passsed test</center></td>
                            </tr>
                            <tr>
                              <td><center>4</center></td>
                              <td><center>EGT Margin Engine is good / APU passsed test</center></td>
                            </tr>
                            <tr>
                              <td><center>5</center></td>
                              <td><center>EGT Margin Engine is satisfying / APU passsed test</center></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-5">
                        <label for="rateServices" style="font-size: 20px;">Quality Assurance & Safety</label><br>
                        <small class="form-text text-muted" style="font-size: 12px;">Description : Accuracy level and strict adherence to operations manual and process standards; Safety & security standards are practiced throughout the work process and supervised (limited to Quality Control / Inspector job function)</small>
                      </div>

                      <div class="col-sm-7">
                          <div class="row">
                            <div class="col-sm-2" style="padding: 10px; margin: auto">
                              <label style="font-size: 13px;">Score : </label>
                            </div>
                            <div class="col-sm-10">
                              <input name="rating[]" id="rating" data-x='2' class="rating rating-loading" data-size="sm" data-min="0" data-max="5" data-step="0.5" <?= (isset($survey[0]['RATE_KATEGORI_3']) ? "value='".$survey[0]['RATE_KATEGORI_3']."' readonly='readonly'" : "")?> required>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-2" style="padding: 10px; margin: auto">
                              <label style="font-size: 13px;">Comments : </label>
                            </div>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name="comment[]" placeholder="Your Answer" value="<?= (isset($survey[0]['COMMENT_KATEGORI_3']) ? $survey[0]['COMMENT_KATEGORI_3'] : "")?>" required>
                            </div>
                          </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <label style="font-size: 13px;">Guidance : </label>
                        <table border="1" width="100%">
                          <thead>
                            <th width="10%"><center>Score</center></th>
                            <th width="90%"><center>Guidance</center></th>
                          </thead>
                          <tbody>
                            <tr>
                              <td><center>1</center></td>
                              <td><center>SVR received without correction. SVR received &lt;15 day after Engine/APU Serviceable</center></td>
                            </tr>
                            <tr>
                              <td><center>2</center></td>
                              <td><center>SVR received without correction. SVR received 15 day after Engine/APU Serviceable</center></td>
                            </tr>
                            <tr>
                              <td><center>3</center></td>
                              <td><center>SVR received after 1 time correction. SVR received 15 day after Engine/APU Serviceable</center></td>
                            </tr>
                            <tr>
                              <td><center>4</center></td>
                              <td><center>SVR received after 2 times correction. SVR received 16-30 day after Engine/APU Serviceable</center></td>
                            </tr>
                            <tr>
                              <td><center>5</center></td>
                              <td><center>SVR received after 3 times correction. SVR received more than 30 day after Engine/APU Serviceable</center></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-5">
                        <label for="rateServices" style="font-size: 20px;">On Time Delivery</label><br>
                        <small class="form-text text-muted" style="font-size: 12px;">Description : Turnaround time fulfillment (Commitment to the original turnaround time)</small>
                      </div>

                      <div class="col-sm-7">
                          <div class="row">
                            <div class="col-sm-2" style="padding: 10px; margin: auto">
                              <label style="font-size: 13px;">Score : </label>
                            </div>
                            <div class="col-sm-10">
                              <input name="rating[]" id="rating" data-x='3' class="rating rating-loading" data-size="sm" data-min="0" data-max="5" data-step="0.5" <?= (isset($survey[0]['RATE_KATEGORI_4']) ? "value='".$survey[0]['RATE_KATEGORI_4']."' readonly='readonly'" : "")?> required>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-2" style="padding: 10px; margin: auto">
                              <label style="font-size: 13px;">Comments : </label>
                            </div>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name="comment[]" placeholder="Your Answer" value="<?= (isset($survey[0]['COMMENT_KATEGORI_4']) ? $survey[0]['COMMENT_KATEGORI_4'] : "")?>" required>
                            </div>
                          </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <label style="font-size: 13px;">Guidance : </label>
                        <table border="1" width="100%">
                          <thead>
                            <th width="10%"><center>Score</center></th>
                            <th width="90%"><center>Guidance</center></th>
                          </thead>
                          <tbody>
                            <tr>
                              <td><center>1</center></td>
                              <td><center>TAT over 30 days</center></td>
                            </tr>
                            <tr>
                              <td><center>2</center></td>
                              <td><center>TAT over 7 days</center></td>
                            </tr>
                            <tr>
                              <td><center>3</center></td>
                              <td><center>TAT over 1-3 days</center></td>
                            </tr>
                            <tr>
                              <td><center>4</center></td>
                              <td><center>TAT achieved same as Contractual</center></td>
                            </tr>
                            <tr>
                              <td><center>5</center></td>
                              <td><center>TAT achieved as Contractual/below</center></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-5">
                        <label for="rateServices" style="font-size: 20px;">Personnel Competency & Availability</label><br>
                        <small class="form-text text-muted" style="font-size: 12px;">Description : Highly competent personnel on technical ability (manpower quality); Personel good attitude helpfulness; Number of certified and skilled personnel is sufficient</small>
                      </div>

                      <div class="col-sm-7">
                          <div class="row">
                            <div class="col-sm-2" style="padding: 10px; margin: auto">
                              <label style="font-size: 13px;">Score : </label>
                            </div>
                            <div class="col-sm-10">
                              <input name="rating[]" id="rating" data-x='4' class="rating rating-loading" data-size="sm" data-min="0" data-max="5" data-step="0.5" <?= (isset($survey[0]['RATE_KATEGORI_5']) ? "value='".$survey[0]['RATE_KATEGORI_5']."' readonly='readonly'" : "")?> required>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-2" style="padding: 10px; margin: auto">
                              <label style="font-size: 13px;">Comments : </label>
                            </div>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name="comment[]" placeholder="Your Answer" value="<?= (isset($survey[0]['COMMENT_KATEGORI_5']) ? $survey[0]['COMMENT_KATEGORI_5'] : "")?>" required>
                            </div>
                          </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <label style="font-size: 13px;">Guidance : </label>
                        <table border="1" width="100%">
                          <thead>
                            <th width="10%"><center>Score</center></th>
                            <th width="90%"><center>Guidance</center></th>
                          </thead>
                          <tbody>
                            <tr>
                              <td><center>1</center></td>
                              <td><center>Lack of Skill; Lack of Experience (&lt;2th); Lack of Manpower</center></td>
                            </tr>
                            <tr>
                              <td><center>2</center></td>
                              <td><center>Skill are enough; Lack of Experience (&lt;2th); Lack of Manpower</center></td>
                            </tr>
                            <tr>
                              <td><center>3</center></td>
                              <td><center>Skill are enough; Experience are enough (2-5th); Lack of Manpower</center></td>
                            </tr>
                            <tr>
                              <td><center>4</center></td>
                              <td><center>Skill good enough; Experience are enough (2-5th); Manpower are enough</center></td>
                            </tr>
                            <tr>
                              <td><center>5</center></td>
                              <td><center>Skill are good; Experience are good (&gt;5th); Manpower are enough</center></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="rateServices" style="font-size: 20px;">Other Comments</label>
                          <textarea class="form-control" rows="5" name="other_comment" required><?= (isset($survey[0]['OTHER_COMMENT']) ? $survey[0]['OTHER_COMMENT'] : "")?></textarea>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-4"></div>
                      <div class="col-sm-4" style="text-align:center;">
                        <label style="font-size: 20px;">Average Score</label><br>
                        <label style="margin: auto;background: #ddd;padding: 20px;text-align: center;font-size: 20px;" id="average"><?= (isset($survey[0]['AVERAGE_RATE']) ? $survey[0]['AVERAGE_RATE'] : "0.0")?></label>
                        <input type="hidden" name="average" id="input_avg">
                      </div>
                      <div class="col-sm-4"></div>
                    </div>
                  </div>
                  <div style="text-align:center;">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

</section>
<?php $this->load->view($script)?>