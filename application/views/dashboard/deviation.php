<section class="content">
<?php
$this->load->view($header_menu);
$this->load->view($title_menu);
?>
<div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>
            <div style="width:45%; margin:0 auto;">
              <canvas id="bar-chart-horizontal" width="800" height="450"></canvas>
            </div>
            <br>
            <div class="col-sm-12">
             <table id="tbdevpoint" class="table table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th rowspan="2" scope="col">No.</th>
                          <th rowspan="2" scope="col">Dev No</th>
                          <th colspan="3" scope="col">TAT</th>
                          <th colspan="2" scope="col">Gate</th>
                          <th colspan="3" scope="col">Deviation Details</th>
                          <th rowspan="2" scope="col">Root Cause</th>
                          <th rowspan="2" scope="col">Corrective Action</th>
                          <th rowspan="2" scope="col">Created By</th>
                          <th rowspan="2" scope="col">Created Date</th>
                        </tr>
                        <tr>
                          <th scope="col">Eng TAT</th>
                          <th scope="col">Cus TAT</th>
                          <th scope="col">Prc TAT Increase</th>
                          <th scope="col">Current Gate</th>
                          <th scope="col">Next Gate Eff</th>
                          <th scope="col">Dev Reason</th>
                          <th scope="col">Prc Delayed</th>
                          <th scope="col">Department</th>
                        </tr>
                      </thead>
                    </table>
                 </div>
            <!-- /.box-body-->

          </div>
          <!-- /.box -->
        </div>

    </div>
</div>
</section>
<?php $this->load->view($script)?>

