<style>
.n-padding {
  padding: 0px;
  padding-bottom: 5px;
}
</style>
<section class="content">
    <?php
    $this->load->view($header_menu);
    $this->load->view($title_menu);
    ?>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-body">
            <form id="form-report" action="<?= base_url('index.php/api/dashboard/CreateReport/'.$docno.'/'.$revnr) ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
              <div class="form-group">
                <label for="week" class="col-sm-3 control-label">Week</label>
                <div class="col-md-6">
                  <input type="number" class="form-control" id="WEEK" name="WEEK" min="1" required>
                </div>
              </div>
              <div class="form-group">
                <label for="detail_report" class="col-sm-3 control-label">Detail Report</label>
                <div class="col-md-9 no-padding">
                  <div class="col-md-12" id="group-detail">
                  </div>
                  <div class="col-md-8 detail_project_input">
                    <textarea class="form-control" name="DETAIL_PROJECT" id="DETAIL_PROJECT" cols="10" rows="4" maxlength="256" required></textarea>
                    <div class="char-left" style="margin-top:5px"><span>256</span> characters left</div>
                  </div>
                  <div class="col-md-4">
                    <!-- <button class="btn btn-primary btn-sm" id="add-new-detail" type="button"><i class="fa fa-plus"></i></button> -->
                    <!-- <button class="btn btn-danger btn-sm" id="remove-new-detail" type="button"><i class="fa fa-times"></i></button> -->
                  </div>
                  <!-- <textarea class="form-control" id="DETAIL_PROJECT" name="DETAIL_PROJECT" style="min-height: 130px;"></textarea> -->
                </div>
              </div>
              <div class="form-group">
                <label for="date" class="col-sm-3 control-label">Date</label>
                <div class="col-md-6">
                  <input class="form-control" type="date" id="DATE" name="DATE" required>
                </div>
              </div>
              <input name="EQUNR" hidden="true" value="<?= $docno ?>">
              <div class="col-md-12">
                <div id="button-submit" class="pull-right">
                  <button class="btn btn-success pull=right" id="btnSubmit" type="submit" ><i class="fa fa-save"> </i> Save </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-body">
            <div class=" table-responsive ">
              <table id="tbReport" class="table table-striped table-bordered table-hovered " >
                <thead>
                  <tr>
                    <th style="width: 5%">Week</th>
                    <th style="width: 70%">Detail Report</th>
                    <th style="width:10%;">Date</th>
                    <th style="white-space:nowrap;width:15%">Action</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

</section>



<div class="modal loan_exchange fade" id="modal_edit" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form method="post" action="<?= base_url('index.php/api/dashboard/UpdateReport')?>" id="form_edit">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Form Edit EO Report</h3>
        </div>
        <div class="modal-body">
          <input type="hidden" name="id" id="id">
          <input type="hidden" name="docno" value="<?= $this->uri->segment(3)?>">
          <input type="hidden" name="revnr" value="<?= $this->uri->segment(4)?>">
          <div class="form-group row">
            <label class="col-sm-4 control-label">Week</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" id="WEEK_EDIT" name="WEEK" min="1" required>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-4 control-label">Detail Report</label>
            <div class="col-sm-8 detail_project_input_edit">
              <textarea class="form-control" name="DETAIL_PROJECT" id="DETAIL_PROJECT_EDIT" cols="10" rows="4" maxlength="256" required></textarea>
              <div class="char-left" style="margin-top:5px"><span id="TOTAL">256</span> characters left</div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-4 control-label">Date</label>
            <div class="col-sm-8">
              <input class="form-control" type="date" id="DATE_EDIT" name="DATE" required>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" id="btn_submit">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php $this->load->view($script)?>