<script type="text/javascript">
    $(document).ready(function () {

        var table = $('#tv').DataTable({
            processing: true,
            serverSide: true,
            paging: true,
            sScrollX: true,
            ajax: {
                url: "<?= base_url('index.php/api/dashboard/GetListManhours'); ?>",
                type: "POST",
                data: {
                    revnr: <?= $revnr?>
                }
            },
            columns: [      
                {data: 'order_number', name: 'order_number', orderable: false},
                {data: 'order_type', name: 'order_type', orderable: false},
                {data: 'mat', name: 'mat', orderable: false},
                {data: 'description', name: 'description', orderable: false},            
                {data: 'status', name: 'status', orderable: true},
                {data: 'plan', name: 'plan', orderable: true},
                {data: 'actual', name: 'actual', orderable: true}
            ]
        });

        $.ajax({
            url: "<?= base_url('index.php/api/dashboard/GetChartManhours'); ?>",
            type: "POST",
            data: {
                revnr: <?= $revnr?>
            },
            success: function(resp){
                resp = JSON.parse(resp);
                renderChart(resp.chart);
            }
        });

    });

    function renderChart(data) {
        new Chart(document.getElementById("bar-chart-grouped"), {
            type: 'bar',
            data: {
                labels: ["PLAN", "ACTUAL"],
                datasets: [
                    {
                    // label: 'Status',
                    data: data.total,
                    backgroundColor:  ['#ccebff','#ffcccc'],
                    borderColor:['#66c2ff','#ff6666'],
                    borderWidth : 1,
                    hoverBackgroundColor:['#66c2ff','#ff6666'],
                    hoverBorderColor :['#ccebff','#ffcccc']
                  }
                ]
            },
            options: {
                title: {
                    display: true,
                },
                legend: {
                    display:false
                }
            }
        }); 

        new Chart(document.getElementById("bar-chart-grouped1"), {
            type: 'bar',
            data: {
                labels: ["PLAN", "ACTUAL"],
                datasets: [
                    {
                    data: data.jobcard,
                    backgroundColor:  ['#ccebff','#ffcccc'],
                    borderColor:['#66c2ff','#ff6666'],
                    borderWidth : 1,
                    hoverBackgroundColor:['#66c2ff','#ff6666'],
                    hoverBorderColor :['#ccebff','#ffcccc']
                  },
                ]
            },
            options: {
                title: {
                    display: true,
                },
                legend: {
                    display:false
                }
            }
        });

        new Chart(document.getElementById("bar-chart-grouped2"), {
            type: 'bar',
            data: {
                labels: ["PLAN", "ACTUAL"],
                datasets: [
                    {
                    // label: 'Status',
                    data: data.mdr,
                    backgroundColor:  ['#ccebff','#ffcccc'],
                    borderColor:['#66c2ff','#ff6666'],
                    borderWidth : 1,
                    hoverBackgroundColor:['#66c2ff','#ff6666'],
                    hoverBorderColor :['#ccebff','#ffcccc']
                  }
                ]
            },
            options: {
                title: {
                    display: true,
                },
                legend: {
                    display:false
                }
            }
        });
    }
</script>