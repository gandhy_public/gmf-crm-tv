<script type="text/javascript">
    $(document).ready(function () {

        var table = $('#tbhightlight').DataTable({
            processing: true,
            serverSide: true,
            paging: true,
            ajax: {
                url: "<?= base_url('index.php/api/dashboard/GetListHighlight'); ?>",
                type: "POST",
                data: {
                    docno:<?= $docno?>,
                    revnr:<?= $revnr?>
                }
            },
            columns: [              
                {data: 'no', name: 'no', orderable: false},
                {data: 'hl_no', name: 'hl_no', orderable: false},
                {data: 'applicable_module', name: 'applicable_module', orderable: false},
                {data: 'highlight', name: 'highlight', orderable: false},
                {data: 'status', name: 'status', orderable: false},
                {data: 'created_by', name: 'created_by', orderable: false},
                {data: 'created_date', name: 'created_date', orderable: false}
            ]
        });

        $.ajax({
            url: "<?= base_url('index.php/api/dashboard/GetChartHighlight'); ?>",
            type: "POST",
            data: {
                docno: <?= $docno?>
            },
            success: function(resp){
                resp = JSON.parse(resp);
                renderChart(resp.status);
            }
        });

        function renderChart(data){
          new Chart(document.getElementById("bar-chart-horizontal"), {
              type: 'horizontalBar',
              data: {
                labels: ["Open", "Close"],
                datasets: [
                  {
                    label: "Status",
                    backgroundColor: ["#b8525d","#0da785"],
                    data: data
                  }
                ]
              },
              options: {
                legend: { display: false },
                title: {
                  display: true,
                  text: 'Status'
                },
                scales: {
                     xAxes: [{
                         ticks: {
                             beginAtZero: true
                         }
                     }]
                 }
              }
          });
        }

    });
</script>
