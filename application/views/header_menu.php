<div class="row">
    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12" style="margin-bottom: 10px;">
        <a style="padding: 8px 40px;background: #fff;border: 1px solid #eaeaea;" class="btn btn-rounded waves-effect btn-social btn-block btn-default" href= "<?php echo base_url(); ?>index.php/dashboard/" href='javascript:void(0);'>
            <i class="fa fa-home"></i> Dashboard
        </a>
    </div>
    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12" style="width: 100%;">
        <div>
            <?php
            $top_menu = json_decode($_COOKIE['all_menu_top'],true);
            $top_menu = $top_menu['menu_top'];
            foreach($top_menu['dashboard/'.$type] as $key){
            ?>
            <a style="padding: 8px 40px; width: 14%;" class="btn btn-outline-primary btn-rounded waves-effect btn-social <?=$key['style']?>" href= "<?= $key['url']?>/<?= $docno?>/<?= $revnr?>/<?= $type?>" href='javascript:void(0);'>
                <i class="<?= $key['icon']?>"></i> <?= $key['name']?>
            </a>
            <?php }?>
        </div>
    </div>
</div>