<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class M_rfc extends CI_Model {

  private $rfclog = "TB_RFC_LOG";

  function check_already_created($n, $v) {
    $sql = "SELECT COUNT(ID) AS COUNT FROM $this->rfclog WHERE JOBNAME = '$n' AND VARIANT = '$v' AND RFCSTATUS = 'CREATED'";
    $data = $this->db->query($sql)->row();
    return $data->COUNT;
  }

  function check_already_running() {
    return $this->db->query("SELECT * FROM $this->rfclog WHERE RFCSTATUS = 'CREATED'")->result();
  }

  function create_log($param) {
    return $this->db->insert($this->rfclog, $param);
  }

  function update_log($id, $param) {
    return $this->db->set($param)->where("ID", $id)->update($this->rfclog);
  }

  function get_log_by_ids($ids) {
    $array = implode("','", $ids);
    return $this->db->query("SELECT * FROM $this->rfclog WHERE ID IN ('".$array."')")->result();
  }

}