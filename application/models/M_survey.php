<?php
date_default_timezone_set("Asia/Jakarta");
class M_survey extends CI_Model {

	private $column_order = [
        'survey' => ["SERNR","EO","WORKSCOPE","COMPANY_NAME","NAME","SERVICEABLE_DATE","AVERAGE_RATE"],
    ];
    private $column_search = [
        'survey' => ["SERNR","EO","WORKSCOPE","COMPANY_NAME","NAME","SERVICEABLE_DATE","AVERAGE_RATE"],
    ];
    private $order = [
        'survey' => ['ID' => 'DESC'],
    ];

    function get_list_survey($total = FALSE, $filter = FALSE){
        $param = $this->_get_datatables('survey');

        $sql = "
        WITH TEMP AS (
            SELECT
                *
            FROM
                TV_SURVEY
        ),
        X AS ( 
          SELECT 
          ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order']." ) AS ROWID,
          * 
          FROM TEMP ".$param['where']."
        ) 
        SELECT * FROM X 
        WHERE ROWID > ".$param['start']." AND ROWID <= ".$param['end']."
        ";

        if($total){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return $this->db->query($sql)->num_rows();
        }elseif($filter){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return $this->db->query($sql)->num_rows();
        }else{
            return $this->db->query($sql)->result();
        }
    }

    function get_all_survey(){
        $sql = "
        SELECT * FROM TV_SURVEY ORDER BY ID DESC
        ";
        return $this->db->query($sql)->result();
    }

	function create_survey($SERNR,$REVNR,$EQUNR,$COMPANY_NAME,$EO,$WORKSCOPE,$AEDAT,$rating,$comment,$other_comment,$average){
		$data = [
			'SERNR' => $SERNR,
			'REVNR' => $REVNR,
			'EQUNR' => $EQUNR,
			'USER_ID' => get_session('id_user'),
			'NAME' => get_session('name'),
			'CUSTOMER_ID' => get_session('customer_id'),
			'COMPANY_NAME' => $COMPANY_NAME,
			'EO' => $EO,
			'WORKSCOPE' => $WORKSCOPE,
			'OTHER_COMMENT' => $other_comment,
			'AVERAGE_RATE' => $average,
			'CREATE_AT' => date('Y-m-d H:i:s'),
			'SERVICEABLE_DATE' => $AEDAT
		];

		$a = 0;
		foreach ($rating as $key) {
			$a++;
			$data['RATE_KATEGORI_'.$a] = $key;
		}
		$a = 0;
		foreach ($comment as $key) {
			$a++;
			$data['COMMENT_KATEGORI_'.$a] = $key;
		}
		
		return $this->db->insert('TV_SURVEY', $data);
	}

	function get_survey($revnr){
		$query = $this->db->select('*')
						->from('TV_SURVEY')
						->where('REVNR',$revnr)
						->get();
		return $query->result_array();
	}

	/*=====================================================================================================================*/

    private function _get_datatables($type){
        $column_order = $this->column_order[$type];
        $column_search = $this->column_search[$type];
        $order = $this->order[$type];
    
        $i = 0;$a = 0;
        if($_REQUEST['search']['value']){
            foreach ($column_search as $item){
                if($_REQUEST['search']['value']) {  
                    if($i===0) {
                        //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->like($item, $_REQUEST['search']['value']);
                    } else {
                        $this->db->or_like($item, $_REQUEST['search']['value']);
                    }
                    //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
                }
                $i++;
            }
        }else{
            $like_arr = [];
            foreach ($_REQUEST['columns'] as $key) {
                if($key['search']['value']){
                    $index = $column_search[$a-2];
                    $like_arr[$index] = $key['search']['value'];
                    /*if($a===0) {
                        $this->db->like($column_search[$a-2], $key['search']['value']);
                    } else {
                        $this->db->or_like($column_search[$a-2], $key['search']['value']);
                    }*/
                }
                $a++;
            }
            $this->db->like($like_arr);
        }
      
        if(isset($_REQUEST['order'])) {
            if($_REQUEST['order']['0']['column'] == 0) {
                $this->db->order_by(key($order), $order[key($order)]);
            }else{
                $this->db->order_by($column_order[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
            }
        } else if(isset($order)) {
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $result =  $this->db->get_compiled_select();

        $query = explode("WHERE", $result);
        if(count($query) == 1){
            $pecah = explode("ORDER BY", $result);
            $where = "";
        }else{
            $pecah = explode("ORDER BY", $query[1]);
            $where = "WHERE ".$pecah[0];
        }
        
        $order = $pecah[1];
        $start = $_REQUEST['start'];
        $end   = $_REQUEST['length'] + $_REQUEST['start'];

        return array(
            'start'       => $start,
            'end'         => $end,
            'order'       => $order,
            'where'       => $where
        );
    }

}