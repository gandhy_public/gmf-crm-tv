<?php
date_default_timezone_set("Asia/Jakarta");
class M_management extends CI_Model {

	private $column_order = [
        'group' => [null,"name","is_active"],
        'user' => ["NAME","USERNAME","ROLE_ID","is_active"],
    ];
    private $column_search = [
        'group' => ["name"],
        'user' => ["NAME","USERNAME","USER_ROLE","USER_GROUP"],
    ];
    private $order = [
        'group' => ['create_at' => 'DESC'],
        'user' => ['ID_USER' => 'DESC'],
    ];

    function get_editable($id_group){
        $select = [
            'TV_MN_MENU.id AS menu_id',
            'TV_MN_MENU.name AS menu_name'
        ];
        $query = $this->db->select($select)
                        ->from('TV_MN_MANAGE_MENU')
                        ->join('TV_MN_MENU','TV_MN_MANAGE_MENU.menu_id = TV_MN_MENU.id')
                        ->where('TV_MN_MANAGE_MENU.group_id',$id_group)
                        ->where('TV_MN_MENU.editable',1)
                        ->get()->result_array();
        $hasil['menu'] = $query;

        $select = [
            'TV_MN_MENU_TOP.id AS menu_top_id',
            'TV_MN_MENU_TOP.name AS menu_top_name',
            'TV_MN_MENU.name AS menu_name'
        ];
        $query = $this->db->select($select)
                        ->from('TV_MN_MANAGE_MENU_TOP')
                        ->join('TV_MN_MENU_TOP','TV_MN_MANAGE_MENU_TOP.menu_top_id = TV_MN_MENU_TOP.id')
                        ->join('TV_MN_MENU','TV_MN_MANAGE_MENU_TOP.menu_id = TV_MN_MENU.id')
                        ->where('TV_MN_MANAGE_MENU_TOP.group_id',$id_group)
                        ->where('TV_MN_MENU_TOP.editable',1)
                        ->get()->result_array();
        $hasil['menu_top'] = $query;
        return $hasil;
    }

    function get_all_role(){
        return $this->db->get('TV_MN_ROLE')->result();
    }

    function get_all_customer(){
        return $this->db->get('CUSTOMER')->result();
    }

    function get_all_group(){
        return $this->db->get_where('TV_MN_GROUP', array('is_active' => 1))->result();
    }

    function create_user($param){
        $this->db->insert('TV_MN_USER', $param);
        return $this->db->insert_id();
    }

    function cek_user($username,$password){
        return $this->db->get_where('TV_MN_USER', array('USERNAME' => $username,'PASSWORD' => $password))->result_array();
    }

    function get_list_user($total = FALSE, $filter = FALSE){
        $param = $this->_get_datatables('user');
        $sql = "
        WITH TEMP AS (
            SELECT 
                TV_MN_USER.*,
                TV_MN_ROLE.*,
                TV_MN_GROUP.name AS USER_GROUP
            FROM
                TV_MN_USER
            JOIN TV_MN_ROLE ON TV_MN_USER.ROLE_ID = TV_MN_ROLE.ID
            JOIN TV_MN_GROUP ON TV_MN_USER.GROUP_ID = TV_MN_GROUP.ID
        ),
        X AS (
            SELECT 
                ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order']." ) AS ROWID,
                * 
            FROM TEMP ".$param['where']."
        )
        SELECT * FROM X 
        WHERE ROWID > ".$param['start']." AND ROWID <= ".$param['end']."
        ";
        if($total){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return $this->db->query($sql)->num_rows();
        }elseif($filter){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return $this->db->query($sql)->num_rows();
        }else{
            return $this->db->query($sql)->result();
        }
    }

    function create_mn_editable_menu($param){
        return $this->db->insert('TV_MN_EDITABLE_MENU',$param);
    }

    function delete_mn_editable_menu($user_id){
        return $this->db->delete('TV_MN_EDITABLE_MENU', array('user_id' => $user_id));
    }

    function create_mn_editable_menu_top($param){
        return $this->db->insert('TV_MN_EDITABLE_MENU_TOP',$param);
    }

    function delete_mn_editable_menu_top($user_id){
        return $this->db->delete('TV_MN_EDITABLE_MENU_TOP', array('user_id' => $user_id));
    }

    function get_user($id_user){
        $select = [
            'TV_MN_USER.*',
            'TV_MN_ROLE.LEVEL'
        ];
        $query = $this->db->select($select)
                        ->from('TV_MN_USER')
                        ->join('TV_MN_ROLE','TV_MN_USER.ROLE_ID=TV_MN_ROLE.ID')
                        ->where('ID_USER',$id_user)
                        ->get();
        return $query->result_array();
    }

    function get_editable_menu($id_user){
        $query = $this->db->select('menu_id')
                        ->from('TV_MN_EDITABLE_MENU')
                        ->where('user_id',$id_user)
                        ->get();
        return $query->result_array();
    }

    function get_editable_menu_top($id_user){
        $query = $this->db->select('menu_top_id')
                        ->from('TV_MN_EDITABLE_MENU_TOP')
                        ->where('user_id',$id_user)
                        ->get();
        return $query->result_array();
    }

    function update_user($param,$id){
        $this->db->where('ID_USER', $id);
        return $this->db->update('TV_MN_USER', $param);
    }










	function get_list_group($total = FALSE, $filter = FALSE){
        $param = $this->_get_datatables('group');

        $sql = "
        WITH TEMP AS (
            SELECT 
            	*
            FROM
                TV_MN_GROUP
        ),
        X AS (
            SELECT 
                ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order']." ) AS ROWID,
                * 
            FROM TEMP ".$param['where']."
        )
        SELECT * FROM X 
        WHERE ROWID > ".$param['start']." AND ROWID <= ".$param['end']."
        ";
        if($total){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return $this->db->query($sql)->num_rows();
        }elseif($filter){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return $this->db->query($sql)->num_rows();
        }else{
            return $this->db->query($sql)->result();
        }
    }

	function get_modul_menu(){
		$select = [
			'TV_MN_MODUL.id AS MODUL_ID',
			'TV_MN_MODUL.name AS MODUL_NAME',
			'TV_MN_MENU.id AS MENU_ID',
			'TV_MN_MENU.name AS MENU_NAME',
			'TV_MN_MENU.menu_top AS MENU_TOP',
		];
		$query = $this->db->select($select)
				->from('TV_MN_MENU')
				->join('TV_MN_MODUL','TV_MN_MENU.modul_id = TV_MN_MODUL.id')
				->get()->result();

		foreach ($query as $key) {
			$data[$key->MODUL_NAME][] = [
				'MODUL_ID' => $key->MODUL_ID,
				'MENU_ID' => $key->MENU_ID,
				'MENU_NAME' => $key->MENU_NAME,
				'MENU_TOP' => $key->MENU_TOP
			];
		}
		return $data;
	}

	function get_modul_menu_top(){
		$select = [
			'TV_MN_MODUL.id AS MODUL_ID',
			'TV_MN_MENU.id AS MENU_ID',
			'TV_MN_MENU.name AS MENU_NAME',
			'TV_MN_MENU_TOP.id AS MENU_TOP_ID',
			'TV_MN_MENU_TOP.name AS MENU_TOP_NAME',
		];
		$query = $this->db->select($select)
				->from('TV_MN_MENU_TOP')
				->join('TV_MN_MODUL','TV_MN_MENU_TOP.modul_id = TV_MN_MODUL.id')
				->join('TV_MN_MENU','TV_MN_MENU_TOP.menu_id = TV_MN_MENU.id')
				->get()->result();

		foreach ($query as $key) {
			$data[$key->MENU_NAME][] = [
				'MODUL_ID' => $key->MODUL_ID,
				'MENU_ID' => $key->MENU_ID,
				'MENU_TOP_ID' => $key->MENU_TOP_ID,
				'MENU_TOP_NAME' => $key->MENU_TOP_NAME
			];
		}
		return $data;
	}

	function create_group($name){
		$data = [
			'name' => $name,
			'is_active' => 1,
			'create_at' => date('Y-m-d H:i:s')
		];
		$this->db->insert("TV_MN_GROUP",$data);
    	return $this->db->insert_id();
	}

	function create_mn_menu($data){
		return $this->db->insert_batch('TV_MN_MANAGE_MENU', $data);
	}

	function delete_mn_menu($id){
		return $this->db->delete('TV_MN_MANAGE_MENU', array('group_id' => $id));
	}

	function create_mn_menu_top($data){
		$this->db->delete('TV_MN_MANAGE_MENU_TOP', array('group_id' => $data[0]['group_id']));
		return $this->db->insert_batch('TV_MN_MANAGE_MENU_TOP', $data);
	}

	function delete_mn_menu_top($id){
		return $this->db->delete('TV_MN_MANAGE_MENU_TOP', array('group_id' => $id));
	}

	function get_group($id_group){
		return $this->db->get_where('TV_MN_GROUP', array('id' => $id_group))->result();
	}

	function get_group_menu($id_group){
		$select = [
			'TV_MN_MANAGE_MENU.modul_id',
			'TV_MN_MANAGE_MENU.menu_id',
		];
		$query = $this->db->select($select)
						->from('TV_MN_MANAGE_MENU')
						->where('TV_MN_MANAGE_MENU.group_id',$id_group)
						->get()->result();
		$data = array();
		foreach ($query as $key) {
			$data[] = $key->modul_id."_".$key->menu_id;
		}
		return $data;
	}

	function get_group_menu_top($id_group){
		$select = [
			'TV_MN_MANAGE_MENU_TOP.modul_id',
			'TV_MN_MANAGE_MENU_TOP.menu_id',
			'TV_MN_MANAGE_MENU_TOP.menu_top_id',
			'TV_MN_MENU.name'
		];
		$query = $this->db->select($select)
						->from('TV_MN_MANAGE_MENU_TOP')
						->join('TV_MN_MENU','TV_MN_MANAGE_MENU_TOP.menu_id = TV_MN_MENU.id')
						->where('TV_MN_MANAGE_MENU_TOP.group_id',$id_group)
						->get()->result();
		$data = array();
		foreach ($query as $key) {
			$data[$key->name] = TRUE;
			$data['value'][] = $key->modul_id."_".$key->menu_id."_".$key->menu_top_id;
		}
		return $data;
	}

	function update_group($id,$name_group,$status_group){
		$this->db->set('name', $name_group);
		$this->db->set('is_active', $status_group);
		$this->db->set('update_at', date('Y-m-d H:i:s'));
        $this->db->where('id', $id);
        return $this->db->update('TV_MN_GROUP');
	}


	/*=====================================================================================================================*/

    private function _get_datatables($type){
        $column_order = $this->column_order[$type];
        $column_search = $this->column_search[$type];
        $order = $this->order[$type];
    
        $i = 0;$a = 0;
        if($_REQUEST['search']['value']){
            foreach ($column_search as $item){
                if($_REQUEST['search']['value']) {  
                    if($i===0) {
                        //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->like($item, $_REQUEST['search']['value']);
                    } else {
                        $this->db->or_like($item, $_REQUEST['search']['value']);
                    }
                    //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
                }
                $i++;
            }
        }else{
            $like_arr = [];
            foreach ($_REQUEST['columns'] as $key) {
                if($key['search']['value']){
                    $index = $column_search[$a-2];
                    $like_arr[$index] = $key['search']['value'];
                    /*if($a===0) {
                        $this->db->like($column_search[$a-2], $key['search']['value']);
                    } else {
                        $this->db->or_like($column_search[$a-2], $key['search']['value']);
                    }*/
                }
                $a++;
            }
            $this->db->like($like_arr);
        }
      
        if(isset($_REQUEST['order'])) {
            if($_REQUEST['order']['0']['column'] == 0) {
                $this->db->order_by(key($order), $order[key($order)]);
            }else{
                $this->db->order_by($column_order[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
            }
        } else if(isset($order)) {
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $result =  $this->db->get_compiled_select();

        $query = explode("WHERE", $result);
        if(count($query) == 1){
            $pecah = explode("ORDER BY", $result);
            $where = "";
        }else{
            $pecah = explode("ORDER BY", $query[1]);
            $where = "WHERE ".$pecah[0];
        }
        
        $order = $pecah[1];
        $start = $_REQUEST['start'];
        $end   = $_REQUEST['length'] + $_REQUEST['start'];

        return array(
            'start'       => $start,
            'end'         => $end,
            'order'       => $order,
            'where'       => $where
        );
    }

}
?>