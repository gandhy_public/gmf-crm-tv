<?php
date_default_timezone_set('Asia/Jakarta');
class M_login extends CI_Model {

    protected $tb_user = 'TV_USER';
    protected $tb_role = 'TV_USER_GROUP';

    private function check_ldap($username, $password){
        $dn = "DC=gmf-aeroasia,DC=co,DC=id";
        $ip_ldap = [
            '0' => "192.168.240.57",
            '1' => "172.16.100.46"
        ];
        for($a=0;$a<count($ip_ldap);$a++){
            $ldapconn = ldap_connect($ip_ldap[$a]);
            if($ldapconn){
                break;
            }else{
                continue;
            }
        }
        
        //$ldapconn = ldap_connect("172.16.100.46") or die ("Could not connect to LDAP server.");
        if ($ldapconn) {
            ldap_set_option(@$ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option(@$ldap, LDAP_OPT_REFERRALS, 0);
            $ldapbind = ldap_bind($ldapconn, "ldap", "aeroasia");
            @$sr = ldap_search($ldapconn, $dn, "samaccountname=$username");
            @$srmail = ldap_search($ldapconn, $dn, "mail=$username@gmf-aeroasia.co.id");
            @$info = ldap_get_entries($ldapconn, @$sr);
            @$infomail = ldap_get_entries($ldapconn, @$srmail);
            @$usermail = substr(@$infomail[0]["mail"][0], 0, strpos(@$infomail[0]["mail"][0], '@'));
            @$bind = @ldap_bind($ldapconn, $info[0][dn], $password);
            // print("<pre>".print_r(@$info['count'],true)."</pre>");
            // print("<pre>".print_r(@$info,true)."</pre>");
            // print("<pre>".print_r(@$infomail,true)."</pre>");
            // print("<pre>".print_r(@$usermail,true)."</pre>");
            // print("<pre>".print_r(@$bind,true)."</pre>");
            //die();


            if(@$info['count']){
                if ((@$info[0]["samaccountname"][0] == $username AND $bind)) {
                    return true;
                } else {
                    return false;
                }
            }else{
                return false;
            }
        } else {
            echo "LDAP Connection trouble, please try again 2/3 time";
        }
    }

    function login($username,$password){
        try {
            $cek_user = $this->db->select("PASSWORD")
                                ->from('TV_MN_USER')
                                ->where("USERNAME",$username)
                                ->get()->result_array();
            if(count($cek_user) == 0) throw new Exception("User not found", 1);
            if($cek_user[0]['PASSWORD'] == 'using ldap'){
                $cek_ldap = $this->check_ldap($username,$password);
                if(!$cek_ldap) throw new Exception("User LDAP not found!", 1);
                $password = $cek_user[0]['PASSWORD'];
            }

            $select = [
                "TV_MN_USER.ID_USER AS user_id",
                "TV_MN_USER.USERNAME AS user_username",
                "TV_MN_USER.NAME AS user_name",
                "TV_MN_USER.STATUS AS user_status",
                "TV_MN_USER.ROLE_ID AS user_role_id",
                "TV_MN_ROLE.LEVEL AS user_role_level",
                "TV_MN_ROLE.USER_ROLE AS user_role",
                "TV_MN_USER.GROUP_ID AS user_group_id",
                "TV_MN_GROUP.name AS user_group",
                "TV_MN_GROUP.is_active AS user_group_status",
                "TV_MN_USER.CUSTOMER_ID AS user_customer_id"
            ];
            $cek_user = $this->db->select($select)
                                ->from('TV_MN_USER')
                                ->join('TV_MN_ROLE',"TV_MN_USER.ROLE_ID=TV_MN_ROLE.ID")
                                ->join('TV_MN_GROUP',"TV_MN_USER.GROUP_ID=TV_MN_GROUP.id")
                                ->where(array(
                                    'TV_MN_USER.USERNAME'=>$username,
                                    'TV_MN_USER.PASSWORD'=>$password,
                                    "TV_MN_USER.STATUS"=>1,
                                    "TV_MN_GROUP.is_active"=>1
                                ))
                                ->get()->result_array();
            if(!$cek_user)throw new Exception("User not found!", 1);
            return [
                'status' => TRUE,
                'data' => $cek_user[0]
            ];
        } catch (Exception $e) {
            return [
                'status' => FALSE,
                'message' => $e->getMessage()
            ];
        }
    }

    function update_last_log($id){
        $data = array(
            'LAST_LOGIN' => date("Y-m-d h:i:s"),
        );
        $this->db->where('ID_USER', $id);
        $user = $this->db->update('TV_MN_USER', $data);
    }

    function get_menu($group_id,$user_id){
        $sql = "
        SELECT
            TV_MN_MODUL.name AS modul_name,
            TV_MN_MODUL.logo AS modul_logo,
            TV_MN_MENU.name AS menu_name,
            TV_MN_MENU.logo AS menu_logo,
            TV_MN_MENU.url AS menu_url,
            TV_MN_EDITABLE_MENU.id AS menu_editable
        FROM 
            TV_MN_MANAGE_MENU
        JOIN TV_MN_MENU ON TV_MN_MANAGE_MENU.menu_id = TV_MN_MENU.id
        JOIN TV_MN_MODUL ON TV_MN_MANAGE_MENU.modul_id = TV_MN_MODUL.id
        LEFT JOIN TV_MN_EDITABLE_MENU ON TV_MN_MANAGE_MENU.menu_id = TV_MN_EDITABLE_MENU.menu_id AND TV_MN_EDITABLE_MENU.user_id = $user_id
        WHERE 
            TV_MN_MANAGE_MENU.group_id = $group_id
        ";
        $query = $this->db->query($sql)->result();
        foreach ($query as $key) {
            $data[$key->modul_name.'='.$key->modul_logo][] = [
                'name' => $key->menu_name,
                'url' => base_url().'index.php/'.$key->menu_url,
                'icon' => $key->menu_logo,
                'editable' => $key->menu_editable,
            ];
        }
        return $data;
    }

    function get_menu_top($group_id,$user_id){
        $sql = "
        SELECT
            TV_MN_MENU.url AS menu_url,
            TV_MN_MENU_TOP.name AS top_name,
            TV_MN_MENU_TOP.url AS top_url,
            TV_MN_MENU_TOP.logo AS top_logo,
            TV_MN_MENU_TOP.style AS top_style,
            TV_MN_EDITABLE_MENU_TOP.id AS top_editable
        FROM
            TV_MN_MANAGE_MENU_TOP
        JOIN TV_MN_MENU_TOP ON TV_MN_MANAGE_MENU_TOP.menu_top_id = TV_MN_MENU_TOP.id
        JOIN TV_MN_MENU ON TV_MN_MANAGE_MENU_TOP.menu_id = TV_MN_MENU.id
        LEFT JOIN TV_MN_EDITABLE_MENU_TOP ON TV_MN_MANAGE_MENU_TOP.menu_top_id = TV_MN_EDITABLE_MENU_TOP.menu_top_id AND TV_MN_EDITABLE_MENU_TOP.user_id = $user_id
        WHERE
            TV_MN_MANAGE_MENU_TOP.group_id = $group_id
        ";
        $query = $this->db->query($sql)->result();
        foreach ($query as $key) {
            $data[$key->menu_url][] = [
                'name' => $key->top_name,
                'url' => base_url().'index.php/'.$key->top_url,
                'icon' => $key->top_logo,
                'style' => $key->top_style,
                'editable' => $key->top_editable,
            ];
        }
        return $data;
    }

}
?>
