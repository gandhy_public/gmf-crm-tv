<?php
date_default_timezone_set("Asia/Jakarta");
class M_dashboard extends CI_Model {

    private $column_order = [
        'project_open' => ["SERNR","REVNR","TEAM_EO","COMPANY_NAME","REVTY","INDUCT_DATE","CONTRACTUAL_TAT","CURRENT_TAT"],
        'project_close' => ["SERNR","REVNR","TEAM_EO","COMPANY_NAME","REVTY","ACT_FINISH","CONTRACTUAL_TAT","CURRENT_TAT"],
        'jobcard' => ["ILART", "KTEXT", "TXT_STAT"],
        'survey' => [null,"NAME", "COMPANY", "ESN"],
        'manhours' => ["AUFNR", "AUART", "ILART", "KTEXT", "STATUS", "ARBEI", "ISMNW"],
        'deviation' => [null,"DEV_NUMBER","DELAY_TAT","DELAY_CUSTOMER","CG_DELAY_PROCESS","CURRENT_GATE","NEXT_GATE_EFFCTD","DEVIATION_REASON","PROCESS_DELAYED","DEPARTEMENT","ROOT_CAUSE","CORECTIVE_ACTION","CREATED_BY","CREATED_DATE"],
        'highlight' => [null,"HIGHLIGHT_NUMBER","APPLCBLE_MODULE","HIGHLIGHT","APPROVAL_STATUS","CREATED_BY","CREATED_DATE"],
        'report' => ["WEEK","DETAIL_PROJECT","DATE_INPUT"]
    ];
    private $column_search = [
        'project_open' => ["SERNR","REVNR","TEAM_EO","COMPANY_NAME","REVTY","INDUCT_DATE","CONTRACTUAL_TAT","CURRENT_TAT"],
        'project_close' => ["SERNR","REVNR","TEAM_EO","COMPANY_NAME","REVTY","ACT_FINISH","CONTRACTUAL_TAT","CURRENT_TAT"],
        'jobcard' => ["ILART", "KTEXT", "TXT_STAT"],
        'survey' => ["NAME", "COMPANY", "ESN"],
        'manhours' => ["AUFNR", "AUART", "ILART", "KTEXT", "STATUS", "ARBEI", "ISMNW"],
        'deviation' => ["DEV_NUMBER","DELAY_TAT","DELAY_CUSTOMER","CG_DELAY_PROCESS","CURRENT_GATE","NEXT_GATE_EFFCTD","DEVIATION_REASON","PROCESS_DELAYED","DEPARTEMENT","ROOT_CAUSE","CORECTIVE_ACTION","CREATED_BY","CREATED_DATE"],
        'highlight' => ["HIGHLIGHT_NUMBER","APPLCBLE_MODULE","HIGHLIGHT","APPROVAL_STATUS","CREATED_BY","CREATED_DATE"],
        'report' => ["WEEK","DETAIL_PROJECT","DATE_INPUT"]
    ];
    private $order = [
        'project_open' => ['ERDAT' => 'DESC'],
        'project_close' => ['ERDAT' => 'DESC'],
        'jobcard' => ['ILART' => 'DESC'],
        'survey' => ['ESN'=>'DESC'],
        'manhours' => ['AUFNR' => 'DESC'],
        'deviation' => ['CREATED_DATE' => 'DESC'],
        'highlight' => ['CREATED_DATE' => 'DESC'],
        'report' => ['CREATE_AT' => 'DESC']
    ];

    function get_detail($docno, $revnr){
        $sql = "
        SELECT
            A.SERNR,
            A.TEAM_EO,
            A.COMPANY_NAME,
            A.REVTY,
            A.ATWRT,
            A.ENGINE_APU,
            A.INDUCT_DATE,
            A.ATSDATE_SMR,
            A.AGREED_TAT as CONTRACTUAL_TAT,
            A.AEDAT
        FROM
            (
                SELECT
                    R.AEDAT,
                    R.SERNR,
                    D.TEAM_EO,
                    C.COMPANY_NAME,
                    R.REVTY,
                    R.ATWRT,
                    (
                        CASE 
                        WHEN LEFT ( R.ATWRT, 3 ) = 'CFM' 
                            THEN 'ENGINE' 
                        WHEN LEFT ( R.ATWRT, 4 ) = 'GTCP' 
                            THEN 'APU' 
                        ELSE '' 
                        END 
                    ) AS ENGINE_APU,
                    D.INDUCT_DATE,
                    R.ATSDATE_SMR,
                    D.AGREED_TAT
                FROM
                    M_REVISION R
                LEFT JOIN M_REVISIONDETAIL D ON R.REVNR = D.REVNR
                LEFT JOIN CUSTOMER C ON R.PARNR = C.ID_CUSTOMER
                WHERE
                    R.IWERK = 'GAEM' AND 
                    R.REVNR = $revnr AND
                    R.EQUNR = $docno
            ) A";
        error_log($sql);
        return $this->db->query($sql)->result_array();
    }

    function get_list_project($type = 'open', $customer_id = NULL, $total = FALSE, $filter = FALSE){
        $param = $this->_get_datatables('project_'.$type);
         
        $customer = '';
        if ($customer_id) {
            $customer = "AND R.PARNR LIKE '$customer_id'";
        }

        if($type == 'close'){
            $type = 'CLSD';
            $contractual = "
            (
                CASE
                WHEN ISDATE( DETAIL.REVBD ) = 1 AND ISDATE( DETAIL.REVED) = 1
                    THEN DATEDIFF( DAY, DETAIL.REVBD, DETAIL.REVED )
                ELSE '-'
                END
            ) AS CONTRACTUAL_TAT,
            ";
            $actual = "
            (
                CASE
                WHEN ISDATE( DETAIL.UDATE_SMR ) = 1 AND ISDATE( DETAIL.ATSDATE_SMR ) = 1
                    THEN DATEDIFF( DAY, DETAIL.UDATE_SMR, DETAIL.ATSDATE_SMR )
                ELSE '-'
                END 
            ) AS CURRENT_TAT,
            ";
        }else{
            $type = 'REL';
            $contractual = "
            (
                CASE 
                WHEN DETAIL.AGREED_TAT IS NULL
                    THEN 0
                ELSE DETAIL.AGREED_TAT
                END
            ) AS CONTRACTUAL_TAT,
            ";
            $actual = "
            (
                CASE
                -- WHEN ISDATE( DETAIL.REVBD ) = 1 AND ISDATE( DETAIL.REVED ) = 1
                    -- THEN DATEDIFF( DAY, DETAIL.REVBD, DETAIL.REVED )
                -- WHEN ISDATE( DETAIL.REVBD ) = 1
                    -- THEN DATEDIFF( DAY, DETAIL.REVBD, GETDATE() )
                WHEN ISDATE( DETAIL.INDUCT_DATE ) = 1
                    THEN DATEDIFF( DAY, DETAIL.INDUCT_DATE, GETDATE() )
                ELSE '-'
                END 
            ) AS CURRENT_TAT,
            ";
        }

        $sql = "
        WITH DETAIL AS (
            SELECT
                (
                    CASE 
                    WHEN ISDATE(D.INDUCT_DATE) = 1 
                        THEN CAST( D.INDUCT_DATE AS DATE )
                    END 
                ) AS MONTH, 
                R.SERNR, 
                R.PARNR, 
                R.REVNR, 
                R.EQUNR, 
                R.REVBD, 
                R.REVED, 
                D.TEAM_EO, 
                D.WORKSCOPE, 
                D.AGREED_TAT, 
                D.INDUCT_DATE, 
                (
                    CASE 
                    WHEN LOWER ( R.PARNR ) LIKE '%citilink%' 
                        THEN 'GA'
                    WHEN LOWER ( R.PARNR ) LIKE '%ga%' 
                        THEN 'GA'
                    WHEN LOWER ( R.PARNR ) LIKE '%garuda%' 
                        THEN 'GA'
                    ELSE ''
                    END 
                ) AS CUST_TYPE, 
                R.ATWRT, 
                (
                    CASE
                    WHEN LEFT ( R.ATWRT, 3 ) = 'CFM' 
                        THEN 'ENGINE'
                    WHEN LEFT ( R.ATWRT, 4 ) = 'GTCP' 
                        THEN 'APU'
                    ELSE ''
                    END 
                ) AS ENGINE_APU, 
                R.REVTY, 
                (
                    CASE
                    WHEN ISDATE( R.ATSDATE_SMR ) = 1 
                        THEN CAST ( R.ATSDATE_SMR AS DATE )
                    END 
                ) AS ACT_FINISH, 
                (
                    CASE
                    WHEN ISDATE( D.INDUCT_DATE ) = 1 
                        THEN CAST ( D.INDUCT_DATE AS DATE )
                    END 
                ) AS ACT_START,
                R.ERDAT,
                R.UDATE_SMR,
                R.ATSDATE_SMR
            FROM
                M_REVISION R
            LEFT JOIN M_REVISIONDETAIL D 
            ON
                R.REVNR = D.REVNR
            WHERE
                R.IWERK = 'GAEM' AND 
                R.TXT04 = '$type' AND 
                R.REVTY IN ('AH', 'AL', 'AM', 'EM', 'EO', 'EP', 'SR') AND
                R.ERDAT > '20151231'
                $customer
        ),
        TEMP AS (
            SELECT
                DETAIL.MONTH,
                DETAIL.SERNR,
                DETAIL.PARNR,
                CUSTOMER.COMPANY_NAME,
                DETAIL.REVNR,
                DETAIL.EQUNR,
                DETAIL.REVBD,
                DETAIL.REVED,
                DETAIL.CUST_TYPE,
                DETAIL.ATWRT,
                DETAIL.ENGINE_APU,
                DETAIL.REVTY,
                DETAIL.TEAM_EO,
                DETAIL.WORKSCOPE,
                $contractual
                $actual
                DETAIL.INDUCT_DATE,
                ACT_FINISH,
                DATEDIFF( DAY, DETAIL.ACT_START, DETAIL.ACT_FINISH ) AS ACT_TAT,
                ( DETAIL.AGREED_TAT / ( DATEDIFF( DAY, DETAIL.ACT_START, DETAIL.ACT_FINISH )) ) AS PERFORMANCE,
                DETAIL.ERDAT
            FROM
                DETAIL
            LEFT JOIN CUSTOMER 
            ON
                CUSTOMER.ID_CUSTOMER = DETAIL.PARNR
        ),
        X AS ( 
            SELECT 
                ROW_NUMBER ( ) OVER ( ORDER BY  ".$param['order']." ) AS ROWID,
                *
            FROM TEMP 
            ".$param['where']."
        ) 
        SELECT * FROM X
        WHERE ROWID > ".$param['start']." AND ROWID <= ".$param['end']."
        ";
        //print('<pre>'.print_r($sql,TRUE).'</pre>');die();
        if($total){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return $this->db->query($sql)->num_rows();
        }elseif($filter){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return $this->db->query($sql)->num_rows();
        }else{
            return $this->db->query($sql)->result();
        }
    }

    public function get_all_project($type = 'open',$customer_id = NULL){
        $customer = '';
        if ($customer_id) {
            $customer = "AND R.PARNR LIKE '$customer_id'";
        }

        if($type == 'close'){
            $type = 'CLSD';
        }else{
            $type = 'REL';
        }

        $sql = "
        WITH DETAIL AS (
            SELECT
                (
                    CASE 
                    WHEN ISDATE(D.INDUCT_DATE) = 1 
                        THEN CAST( D.INDUCT_DATE AS DATE )
                    END 
                ) AS MONTH, 
                R.SERNR, 
                R.PARNR, 
                R.REVNR, 
                R.EQUNR, 
                R.REVBD, 
                R.REVED, 
                D.TEAM_EO, 
                D.WORKSCOPE, 
                D.AGREED_TAT, 
                D.INDUCT_DATE, 
                (
                    CASE 
                    WHEN LOWER ( R.PARNR ) LIKE '%citilink%' 
                        THEN 'GA'
                    WHEN LOWER ( R.PARNR ) LIKE '%ga%' 
                        THEN 'GA'
                    WHEN LOWER ( R.PARNR ) LIKE '%garuda%' 
                        THEN 'GA'
                    ELSE ''
                    END 
                ) AS CUST_TYPE, 
                R.ATWRT, 
                (
                    CASE
                    WHEN LEFT ( R.ATWRT, 3 ) = 'CFM' 
                        THEN 'ENGINE'
                    WHEN LEFT ( R.ATWRT, 4 ) = 'GTCP' 
                        THEN 'APU'
                    ELSE ''
                    END 
                ) AS ENGINE_APU, 
                R.REVTY, 
                (
                    CASE
                    WHEN ISDATE( R.ATSDATE_SMR ) = 1 
                        THEN CAST ( R.ATSDATE_SMR AS DATE )
                    END 
                ) AS ACT_FINISH, 
                (
                    CASE
                    WHEN ISDATE( D.INDUCT_DATE ) = 1 
                        THEN CAST ( D.INDUCT_DATE AS DATE )
                    END 
                ) AS ACT_START,
                R.ERDAT
            FROM
                M_REVISION R
            LEFT JOIN M_REVISIONDETAIL D 
            ON
                R.REVNR = D.REVNR
            WHERE
                R.IWERK = 'GAEM' AND 
                R.TXT04 = '$type' AND 
                R.REVTY IN ('AH', 'AL', 'AM', 'EM', 'EO', 'EP', 'SR') AND
                R.ERDAT > '20151231'
                $customer
        ),
        TEMP AS (
            SELECT
                DETAIL.MONTH,
                DETAIL.SERNR,
                DETAIL.PARNR,
                CUSTOMER.COMPANY_NAME,
                DETAIL.REVNR,
                DETAIL.EQUNR,
                DETAIL.REVBD,
                DETAIL.REVED,
                DETAIL.CUST_TYPE,
                DETAIL.ATWRT,
                DETAIL.ENGINE_APU,
                DETAIL.REVTY,
                DETAIL.TEAM_EO,
                DETAIL.WORKSCOPE,
                (
                    CASE 
                    WHEN DETAIL.AGREED_TAT IS NULL
                        THEN 0
                    ELSE DETAIL.AGREED_TAT
                    END
                ) AS CONTRACTUAL_TAT,
                (
                    CASE
                    WHEN ISDATE( DETAIL.REVBD ) = 1 AND ISDATE( DETAIL.REVED ) = 1
                        THEN DATEDIFF( DAY, DETAIL.REVBD, DETAIL.REVED )
                    WHEN ISDATE( DETAIL.REVBD ) = 1
                        THEN DATEDIFF( DAY, DETAIL.REVBD, GETDATE() )
                    ELSE '-'
                    END 
                ) AS CURRENT_TAT,
                DETAIL.INDUCT_DATE,
                DATEDIFF( DAY, DETAIL.ACT_START, DETAIL.ACT_FINISH ) AS ACT_TAT,
                ( DETAIL.AGREED_TAT / ( DATEDIFF( DAY, DETAIL.ACT_START, DETAIL.ACT_FINISH )) ) AS PERFORMANCE,
                DETAIL.ERDAT
            FROM
                DETAIL
            LEFT JOIN CUSTOMER 
            ON
                CUSTOMER.ID_CUSTOMER = DETAIL.PARNR
        ),
        X AS ( 
            SELECT 
                ROW_NUMBER ( ) OVER ( ORDER BY  ERDAT DESC ) AS ROWID,
                *
            FROM TEMP 
        ) 
        SELECT * FROM X
        ";
        return $this->db->query($sql)->result();
    }

    function get_list_jobcard($docno, $revnr, $total = FALSE, $filter = FALSE){
        $param = $this->_get_datatables('jobcard');

        $EQUNR = ltrim($docno, '0');
        $sql = "
        WITH BB AS (
            SELECT DISTINCT
                AUFNR,
                AUART,
                KTEXT,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open'
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress'
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close'
                END AS TXT_STAT
            FROM
                M_PMORDERH
            WHERE
                REVNR LIKE '%$revnr' 
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01'
        ),
        AA AS (
            SELECT * 
            FROM BB 
            WHERE 
                -- ILART IN ('REM','PRI','DSY','1','RT1','RT2','RT3','REP','NWT','ASY','INS','TST','QEC')
                ILART IN ('REM','PRI','DSY','1','RT0','RT1','RT2','RT3','RTC','REP','NRT','MDI','ASY','INS','TES','QEC','TOP','SPC','OUT','BSI','MDR','AD','SB')

        ),
        X AS (
            SELECT 
                ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order']." ) AS ROWID,
                * 
            FROM AA ".$param['where']."
        )
        SELECT * FROM X 
        WHERE ROWID > ".$param['start']." AND ROWID <= ".$param['end']."
        ";
        if($total){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return $this->db->query($sql)->num_rows();
        }elseif($filter){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return $this->db->query($sql)->num_rows();
        }else{
            return $this->db->query($sql)->result();
        }
    }

    function get_overview($docno, $revnr){
        $sql = "
        SELECT
            A.MONTH,
            A.SERNR,
            A.PARNR,
            A.REVNR,
            A.EQUNR,
            A.REVBD,
            A.REVED,
            A.CUST_TYPE,
            A.ATWRT,
            A.ENGINE_APU,
            A.REVTY,
            A.TEAM_EO,
            A.WORKSCOPE,
            A.AGREED_TAT as CONTRACTUAL_TAT,
            A.INDUCT_DATE, 
            DATEDIFF( DAY, A.ACT_START, A.ACT_FINISH ) AS ACT_TAT,
            ( A.AGREED_TAT / ( DATEDIFF( DAY, A.ACT_START, A.ACT_FINISH )) ) AS PERFORMANCE 
        FROM
            (
                SELECT
                    ( CASE WHEN ISDATE(D.INDUCT_DATE) = 1 THEN CAST(D.INDUCT_DATE AS DATE) END ) AS MONTH,
                    R.SERNR,
                    R.PARNR,
                    R.REVNR,
                    R.EQUNR,
                    R.REVBD,
                    R.REVED,
                    D.TEAM_EO,
                    D.WORKSCOPE,
                    D.AGREED_TAT,
                    D.INDUCT_DATE, 
                    (
                        CASE
                        WHEN LOWER ( R.PARNR ) LIKE '%citilink%' 
                            THEN 'GA' 
                        WHEN LOWER ( R.PARNR ) LIKE '%ga%' 
                            THEN 'GA' 
                        WHEN LOWER ( R.PARNR ) LIKE '%garuda%' 
                            THEN 'GA' 
                        ELSE '' 
                        END 
                    ) AS CUST_TYPE,
                    R.ATWRT,
                    (
                        CASE
                        WHEN LEFT ( R.ATWRT, 3 ) = 'CFM' 
                            THEN 'ENGINE' 
                        WHEN LEFT ( R.ATWRT, 4 ) = 'GTCP' 
                            THEN 'APU' 
                        ELSE '' 
                        END 
                    ) AS ENGINE_APU,
                    R.REVTY,
                    ( CASE WHEN ISDATE( R.ATSDATE_SMR ) = 1 THEN CAST ( R.ATSDATE_SMR AS DATE ) END ) AS ACT_FINISH,
                    ( CASE WHEN ISDATE( D.INDUCT_DATE ) = 1 THEN CAST ( D.INDUCT_DATE AS DATE ) END ) AS ACT_START 
                FROM
                    M_REVISION R
                LEFT JOIN M_REVISIONDETAIL D ON R.REVNR = D.REVNR 
                WHERE
                    R.IWERK = 'GAEM' AND 
                    R.REVNR = $revnr
            ) A";
        error_log($sql);
        return $this->db->query($sql)->result_array();
    }

    function get_part_overview($docno, $revnr, $type, $status){
        if($type == "disassembly"){
            if($status == "closed"){
                $select = "SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('REM','DSY') AND TXT_STAT LIKE 'close'";
            }else{
                $select = "SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('REM','DSY')";
            }
        }elseif($type == "inspection"){
            if($status == "closed"){
                $select = "SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('1','RT0','RT1','RT2','RT3','RTC','MDI','PRI','OUT','BSI') AND TXT_STAT LIKE 'close'";
            }else{
                $select = "SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('1','RT0','RT1','RT2','RT3','RTC','MDI','PRI','OUT','BSI')";
            }
        }elseif($type == "repair"){
            if($status == "closed"){
                $select = "SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('REP','NRT','MDR') AND TXT_STAT LIKE 'close'";
            }else{
                $select = "SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('REP','NRT','MDR')";
            }
        }elseif($type == "assembly"){
            if($status == "closed"){
                $select = "SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('INS','ASY') AND TXT_STAT LIKE 'close'";
            }else{
                $select = "SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('INS','ASY')";
            }
        }elseif($type == "test"){
            if($status == "closed"){
                $select = "SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'TES' AND TXT_STAT LIKE 'close'";
            }else{
                $select = "SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'TES'";
            }
        }elseif($type == "qec"){
            if($status == "closed"){
                $select = "SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'QEC' AND TXT_STAT LIKE 'close'";
            }else{
                $select = "SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'QEC'";
            }
        }elseif($type == "adsb"){
            if($status == "closed"){
                $select = "SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('AD','SB') AND TXT_STAT LIKE 'close'";
            }else{
                $select = "SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('AD','SB')";
            }
        }elseif($type == "spec"){
            if($status == "closed"){
                $select = "SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('SPC','TOP') AND TXT_STAT LIKE 'close'";
            }else{
                $select = "SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('SPC','TOP')";
            }
        }elseif($type == "jc"){
            if($status == "open"){
                $select = "
                SELECT COUNT(*) as 'JUMLAH'
                FROM BB 
                WHERE 
                    -- ILART IN ('REM','PRI','DSY','1','RT1','RT2','RT3','REP','NWT','ASY','INS','TST','QEC') AND TXT_STAT LIKE 'open'
                    ILART IN ('REM','PRI','DSY','1','RT0','RT1','RT2','RT3','RTC','REP','NRT','MDI','ASY','INS','TES','QEC','TOP','SPC','OUT','BSI','MDR','AD','SB') AND TXT_STAT LIKE 'open'
                ";
            }elseif($status == "closed"){
                $select = "
                SELECT COUNT(*) as 'JUMLAH' 
                FROM BB 
                WHERE 
                    -- ILART IN ('REM','PRI','DSY','1','RT1','RT2','RT3','REP','NWT','ASY','INS','TST','QEC') AND TXT_STAT LIKE 'close'
                    ILART IN ('REM','PRI','DSY','1','RT0','RT1','RT2','RT3','RTC','REP','NRT','MDI','ASY','INS','TES','QEC','TOP','SPC','OUT','BSI','MDR','AD','SB') AND TXT_STAT LIKE 'close'
                ";
            }elseif($status == "progress"){
                $select = "
                SELECT COUNT(*) as 'JUMLAH' 
                FROM BB 
                WHERE 
                    -- ILART IN ('REM','PRI','DSY','1','RT1','RT2','RT3','REP','NWT','ASY','INS','TST','QEC') AND TXT_STAT LIKE 'progress'
                    ILART IN ('REM','PRI','DSY','1','RT0','RT1','RT2','RT3','RTC','REP','NRT','MDI','ASY','INS','TES','QEC','TOP','SPC','OUT','BSI','MDR','AD','SB') AND TXT_STAT LIKE 'progress'
                ";
            }else{
                $select = "
                SELECT COUNT(*) as 'JUMLAH' 
                FROM BB 
                WHERE 
                    -- ILART IN ('REM','PRI','DSY','1','RT1','RT2','RT3','REP','NWT','ASY','INS','TST','QEC')
                    ILART IN ('REM','PRI','DSY','1','RT0','RT1','RT2','RT3','RTC','REP','NRT','MDI','ASY','INS','TES','QEC','TOP','SPC','OUT','BSI','MDR','AD','SB')
                ";
            }
        }
        $EQUNR = ltrim($docno, '0');
        $sql = "
        WITH BB AS (
            SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                WHEN PHAS0 LIKE 'x' 
                    THEN 'open' 
                WHEN PHAS1 LIKE 'x' 
                    THEN 'progress' 
                WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' 
                    THEN 'close' 
                END AS TXT_STAT 
            FROM
                M_PMORDERH
            WHERE
                REVNR LIKE '%$revnr' 
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01'
        ) 
        $select
        ";
        $result = $this->db->query($sql)->row_array();
        return $result['JUMLAH'];
    }

    function get_GSTRP($docno, $revnr){
        $EQUNR = ltrim($docno, '0');
        $sql = "
        SELECT 
            DISTINCT CONVERT( DATE, SUBSTRING(GSTRP, 0, 3) +'-'+ SUBSTRING(GSTRP, 3, 2) +'-'+ SUBSTRING(GSTRP, 5, 4), 104) AS GSTRP 
        FROM 
            M_PMORDER
        WHERE 
            REVNR = '$revnr' AND
            EQUNR = '$EQUNR' AND
            GSTRP != '00000000'
        GROUP BY GSTRP
        ORDER BY GSTRP ASC
        ";
        return $this->db->query($sql)->result();
    }

    function get_PLAN($docno, $revnr){
        $EQUNR = ltrim($docno, '0');
        $sql = "
        SELECT 
            COUNT(AUART) as AUART,
            CASE 
            WHEN CONVERT(DATE, SUBSTRING(GSTRP, 0, 3) +'-'+ SUBSTRING(GSTRP, 3, 2) +'-'+ SUBSTRING(GSTRP, 5, 4), 104) IS NOT NULL
                THEN CONVERT(DATE, SUBSTRING(GSTRP, 0, 3) +'-'+ SUBSTRING(GSTRP, 3, 2) +'-'+ SUBSTRING(GSTRP, 5, 4), 104)
            ELSE 'NULL'
            END AS TGL
        FROM 
            (
                SELECT DISTINCT
                    AUFNR,
                    AUART,
                    REVNR,
                    EQUNR,
                    ILART,
                    GSTRP
                FROM 
                    M_PMORDER
                WHERE 
                    REVNR = '$revnr' AND
                    EQUNR = '$EQUNR' AND
                    GSTRP != '00000000'
            ) TV
        GROUP BY GSTRP
        ORDER BY TGL
        ";
        //print('<pre>'.print_r($sql,TRUE).'</pre>');die();
        return $this->db->query($sql)->result();
    }

    function get_ACTUAL($docno, $revnr){
        $EQUNR = ltrim($docno, '0');
        $sql = "
        SELECT 
            COUNT(AUART) as AUART,
            CASE 
            WHEN CONVERT(DATE, SUBSTRING(GETRI, 0, 3) +'-'+ SUBSTRING(GETRI, 3, 2) +'-'+ SUBSTRING(GETRI, 5, 4), 104) IS NOT NULL
                THEN CONVERT(DATE, SUBSTRING(GETRI, 0, 3) +'-'+ SUBSTRING(GETRI, 3, 2) +'-'+ SUBSTRING(GETRI, 5, 4), 104)
            ELSE 'NULL'
            END AS TGL
            FROM 
                (
                    SELECT DISTINCT
                        AUFNR,
                        AUART,
                        REVNR,
                        EQUNR,
                        ILART,
                        GETRI
                    FROM
                        M_PMORDER
                    WHERE
                        REVNR = '$revnr' AND
                        EQUNR = '$EQUNR' AND
                        GETRI != '00000000' 
                ) TV
            GROUP BY GETRI
            ORDER BY TGL
        ";
        return $this->db->query($sql)->result_array();
    }

    function get_list_manhours($revnr, $total = FALSE, $filter = FALSE){
        $param = $this->_get_datatables('manhours');

        $sql = "
        WITH TEMP AS (
            SELECT
                DISTINCT
                T.AUFNR,
                T.AUART,
                T.KTEXT,
                CONVERT( decimal(6,4),T.ARBEI ) AS ARBEI,
                CONVERT( decimal(6,4),T.ISMNW ) AS ISMNW,
                CASE
                    WHEN M.PHAS0 = 'x' THEN 'OPEN'
                    WHEN M.PHAS1 = 'x' THEN 'PROGRESS'
                    WHEN M.PHAS2 = 'x'
                    OR M.PHAS3 = 'x' THEN 'CLOSE'
                END AS STATUS,
                M.ILART
            FROM
                (
                SELECT
                    DISTINCT AUFNR,
                    AUART,
                    KTEXT,
                    VORNR,
                    ARBEI,
                    ISMNW
                FROM
                    M_PMORDER
                WHERE
                    REVNR = '$revnr' AND 
                    AUART IN ( 'GA01','GA02' ) 
                ) T
            LEFT JOIN M_PMORDER M ON T.AUFNR = M.AUFNR
            WHERE 
                -- ILART IN ('REM','PRI','DSY','1','RT1','RT2','RT3','REP','NWT','ASY','INS','TST','QEC')
                ILART IN ('REM','PRI','DSY','1','RT0','RT1','RT2','RT3','RTC','REP','NRT','MDI','ASY','INS','TES','QEC','TOP','SPC','OUT','BSI','MDR','AD','SB')
        ),
        TEMP2 AS (
            SELECT
                TEMP.AUFNR,
                TEMP.AUART,
                A.KTEXT,
                TEMP.ARBEI,
                TEMP.ISMNW,
                CASE
                    WHEN A.PHAS0 = 'x' THEN 'OPEN'
                    WHEN A.PHAS1 = 'x' THEN 'PROGRESS'
                    WHEN A.PHAS2 = 'x'
                    OR A.PHAS3 = 'x' THEN 'CLOSE'
                END AS STATUS_MANHOURS,
                TEMP.ILART
            FROM
                TEMP
            JOIN M_PMORDERH A ON TEMP.AUFNR = A.AUFNR AND TEMP.ILART = A.ILART
        ),
        TOTAL AS (
            SELECT
                AUFNR,
                AUART,
                KTEXT,
                CONVERT( decimal(6,4),SUM(ARBEI)) AS ARBEI,
                CONVERT( decimal(6,4),SUM(ISMNW)) AS ISMNW,
                STATUS_MANHOURS AS STATUS,
                ILART
            FROM
                TEMP2
            GROUP BY
                AUFNR,
                AUART,
                KTEXT,
                STATUS_MANHOURS,
                ILART
        ),
        X AS ( 
          SELECT 
          ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order']." ) AS ROWID,
          * 
          FROM TOTAL ".$param['where']."
        ) 
        SELECT * FROM X 
        WHERE ROWID > ".$param['start']." AND ROWID <= ".$param['end']."
        ";
        //print('<pre>'.print_r($sql,TRUE).'</pre>');die();
        if($total){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return $this->db->query($sql)->num_rows();
        }elseif($filter){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return $this->db->query($sql)->num_rows();
        }else{
            return $this->db->query($sql)->result();
        }
    }

    function get_all_manhours($revnr){
        $sql = "
        WITH TEMP AS (
            SELECT
                DISTINCT
                T.AUFNR,
                T.AUART,
                T.KTEXT,
                CONVERT( decimal(6,4),T.ARBEI ) AS ARBEI,
                CONVERT( decimal(6,4),T.ISMNW ) AS ISMNW,
                CASE
                    WHEN M.PHAS0 = 'x' THEN 'OPEN'
                    WHEN M.PHAS1 = 'x' THEN 'PROGRESS'
                    WHEN M.PHAS2 = 'x'
                    OR M.PHAS3 = 'x' THEN 'CLOSE'
                END AS STATUS,
                M.ILART
            FROM
                (
                SELECT
                    DISTINCT AUFNR,
                    AUART,
                    KTEXT,
                    VORNR,
                    ARBEI,
                    ISMNW
                FROM
                    M_PMORDER
                WHERE
                    REVNR = '$revnr' AND 
                    AUART IN ( 'GA01','GA02' ) 
                ) T
            LEFT JOIN M_PMORDER M ON T.AUFNR = M.AUFNR
            WHERE 
                -- ILART IN ('REM','PRI','DSY','1','RT1','RT2','RT3','REP','NWT','ASY','INS','TST','QEC')
                ILART IN ('REM','PRI','DSY','1','RT0','RT1','RT2','RT3','RTC','REP','NRT','MDI','ASY','INS','TES','QEC','TOP','SPC','OUT','BSI','MDR','AD','SB')
        ),
        TEMP2 AS (
            SELECT
                TEMP.AUFNR,
                TEMP.AUART,
                A.KTEXT,
                TEMP.ARBEI,
                TEMP.ISMNW,
                CASE
                    WHEN A.PHAS0 = 'x' THEN 'OPEN'
                    WHEN A.PHAS1 = 'x' THEN 'PROGRESS'
                    WHEN A.PHAS2 = 'x'
                    OR A.PHAS3 = 'x' THEN 'CLOSE'
                END AS STATUS_MANHOURS,
                TEMP.ILART
            FROM
                TEMP
            JOIN M_PMORDERH A ON TEMP.AUFNR = A.AUFNR AND TEMP.ILART = A.ILART
        ),
        TOTAL AS (
            SELECT
                AUFNR,
                AUART,
                KTEXT,
                CONVERT( decimal(6,4),SUM(ARBEI)) AS ARBEI,
                CONVERT( decimal(6,4),SUM(ISMNW)) AS ISMNW,
                STATUS_MANHOURS AS STATUS,
                ILART
            FROM
                TEMP2
            GROUP BY
                AUFNR,
                AUART,
                KTEXT,
                STATUS_MANHOURS,
                ILART
        ),
        X AS ( 
          SELECT 
          ROW_NUMBER ( ) OVER ( ORDER BY AUFNR DESC ) AS ROWID,
          * 
          FROM TOTAL
        ) 
        SELECT * FROM X 
        ";
        return $this->db->query($sql)->result();
    }

    public function get_chart_manhours($revnr){
        $sql = "
        WITH TEMP AS (
            SELECT
                DISTINCT
                T.AUFNR,
                T.AUART,
                T.KTEXT,
                CONVERT( decimal(6,4),T.ARBEI ) AS ARBEI,
                CONVERT( decimal(6,4),T.ISMNW ) AS ISMNW,
                CASE
                    WHEN M.PHAS0 = 'x' THEN 'OPEN'
                    WHEN M.PHAS1 = 'x' THEN 'PROGRESS'
                    WHEN M.PHAS2 = 'x'
                    OR M.PHAS3 = 'x' THEN 'CLOSE'
                END AS STATUS,
                M.ILART
            FROM
                (
                SELECT
                    DISTINCT AUFNR,
                    AUART,
                    KTEXT,
                    VORNR,
                    ARBEI,
                    ISMNW
                FROM
                    M_PMORDER
                WHERE
                    REVNR = '$revnr' AND 
                    AUART IN ( 'GA01','GA02' ) 
                ) T
            LEFT JOIN M_PMORDER M ON T.AUFNR = M.AUFNR
            WHERE 
                -- ILART IN ('REM','PRI','DSY','1','RT1','RT2','RT3','REP','NWT','ASY','INS','TST','QEC')
                ILART IN ('REM','PRI','DSY','1','RT0','RT1','RT2','RT3','RTC','REP','NRT','MDI','ASY','INS','TES','QEC','TOP','SPC','OUT','BSI','MDR','AD','SB')
        ),
        TEMP2 AS (
            SELECT
                TEMP.AUFNR,
                TEMP.AUART,
                A.KTEXT,
                TEMP.ARBEI,
                TEMP.ISMNW,
                CASE
                    WHEN A.PHAS0 = 'x' THEN 'OPEN'
                    WHEN A.PHAS1 = 'x' THEN 'PROGRESS'
                    WHEN A.PHAS2 = 'x'
                    OR A.PHAS3 = 'x' THEN 'CLOSE'
                END AS STATUS_MANHOURS,
                TEMP.ILART
            FROM
                TEMP
            JOIN M_PMORDERH A ON TEMP.AUFNR = A.AUFNR AND TEMP.ILART = A.ILART
        ),
        TOTAL AS (
            SELECT
                AUFNR,
                AUART,
                KTEXT,
                CONVERT( decimal(6,4),SUM(ARBEI)) AS ARBEI,
                CONVERT( decimal(6,4),SUM(ISMNW)) AS ISMNW,
                STATUS_MANHOURS AS STATUS,
                ILART
            FROM
                TEMP2
            GROUP BY
                AUFNR,
                AUART,
                KTEXT,
                STATUS_MANHOURS,
                ILART
        ),
        X AS ( 
          SELECT 
          ROW_NUMBER ( ) OVER ( ORDER BY AUFNR DESC ) AS ROWID,
          * 
          FROM TOTAL
        ) 
        SELECT * FROM X
        ";
        return $this->db->query($sql)->result();
    }

    function get_list_deviation($docno, $total = FALSE, $filter = FALSE){
        $param = $this->_get_datatables('deviation');

        $sql = "
        WITH TEMP AS (
            SELECT
                RD.DEV_NUMBER,
                RD.DELAY_TAT,
                RD.DELAY_CUSTOMER,
                RD.CG_DELAY_PROCESS,
                RD.CURRENT_GATE,
                RD.NEXT_GATE_EFFCTD,
                RD.DEVIATION_REASON,
                RD.DEPARTEMENT,
                RD.PROCESS_DELAYED,
                RD.ROOT_CAUSE,
                RD.CORECTIVE_ACTION,
                RD.CREATED_BY,
                RD.CREATED_DATE
            FROM
                M_REVISIONDEVIATION RD
            JOIN M_REVISION R ON RD.REVNR = R.REVNR
            WHERE R.EQUNR LIKE '%{$docno}'
        ),
        X AS (
            SELECT 
                ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order']." ) AS ROWID,
                * 
            FROM TEMP ".$param['where']."
        )
        SELECT * FROM X 
        WHERE ROWID > ".$param['start']." AND ROWID <= ".$param['end']."
        ";
        if($total){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return $this->db->query($sql)->num_rows();
        }elseif($filter){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return $this->db->query($sql)->num_rows();
        }else{
            return $this->db->query($sql)->result();
        }
    }

    function get_chart_deviation($docno){
        $sql = "
        SELECT
            RD.DEV_NUMBER,
            RD.DELAY_TAT,
            RD.DELAY_CUSTOMER,
            RD.CG_DELAY_PROCESS,
            RD.CURRENT_GATE,
            RD.NEXT_GATE_EFFCTD,
            RD.DEVIATION_REASON,
            RD.DEPARTEMENT,
            RD.PROCESS_DELAYED,
            RD.ROOT_CAUSE,
            RD.CORECTIVE_ACTION,
            RD.CREATED_BY,
            RD.CREATED_DATE
        FROM
            M_REVISIONDEVIATION RD
        JOIN M_REVISION R ON RD.REVNR = R.REVNR
        WHERE R.EQUNR LIKE '%{$docno}'
        ";
        return $this->db->query($sql)->result();
    }

    function get_list_highlight($docno, $total = FALSE, $filter = FALSE){
        $param = $this->_get_datatables('highlight');

        $sql = "
        WITH TEMP AS (
            SELECT
                HIGHLIGHT_NUMBER,
                APPLCBLE_MODULE,
                HIGHLIGHT,
                APPROVAL_STATUS,
                CREATED_BY,
                CREATED_DATE
            FROM
                M_REVISION
            WHERE 
                EQUNR LIKE '%{$docno}' AND 
                IWERK = 'GAEM' AND 
                HIGHLIGHT_NUMBER != ''
        ),
        X AS (
            SELECT 
                ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order']." ) AS ROWID,
                * 
            FROM TEMP ".$param['where']."
        )
        SELECT * FROM X 
        WHERE ROWID > ".$param['start']." AND ROWID <= ".$param['end']."
        ";
        if($total){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return $this->db->query($sql)->num_rows();
        }elseif($filter){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return $this->db->query($sql)->num_rows();
        }else{
            return $this->db->query($sql)->result();
        }
    }

    function get_chart_highlight($docno){
        $sql = "
        SELECT
            HIGHLIGHT_NUMBER,
            APPLCBLE_MODULE,
            HIGHLIGHT,
            APPROVAL_STATUS,
            CREATED_BY,
            CREATED_DATE
        FROM
            M_REVISION
        WHERE 
            EQUNR LIKE '%{$docno}' AND 
            IWERK = 'GAEM' AND 
            HIGHLIGHT_NUMBER != ''
        ";
        return $this->db->query($sql)->result();
    }

    function get_list_report($docno,$total = FALSE, $filter = FALSE){
        $param = $this->_get_datatables('report');

        $sql = "
        WITH TEMP AS (
            SELECT
                *
            FROM
                TV_EO_REPORT
            WHERE 
                IS_DELETED = 0 AND
                EQUNR = $docno
        ),
        X AS (
            SELECT 
                ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order']." ) AS ROWID,
                * 
            FROM TEMP ".$param['where']."
        )
        SELECT * FROM X 
        WHERE ROWID > ".$param['start']." AND ROWID <= ".$param['end']."
        ";
        if($total){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return $this->db->query($sql)->num_rows();
        }elseif($filter){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return $this->db->query($sql)->num_rows();
        }else{
            return $this->db->query($sql)->result();
        }
    }

    function create_report($week,$detail,$date,$equnr){
        $data = array(
            'EQUNR' => $equnr,
            'WEEK' => $week,
            'DETAIL_PROJECT' => $detail,
            'DATE_INPUT' => $date,
            'CREATE_AT' => date('Y-m-d H:i:s'),
            'CREATED_BY' => get_session('id_user'),
            'IS_DELETED' => 0
        );
        return $this->db->insert('TV_EO_REPORT', $data);
    }

    function delete_report($id){
        $this->db->set('IS_DELETED', '1');
        $this->db->where('ID', $id);
        return $this->db->update('TV_EO_REPORT');
    }

    function detail_report($id){
        $query = $this->db->get_where('TV_EO_REPORT', array('ID' => $id));
        return $query->result_array();
    }

    function update_report($week,$detail,$date,$id){
        $this->db->set('WEEK', $week);
        $this->db->set('DETAIL_PROJECT', $detail);
        $this->db->set('DATE_INPUT', $date);
        $this->db->where('ID', $id);
        return $this->db->update('TV_EO_REPORT');
    }

/*=====================================================================================================================*/

    private function _get_datatables($type){
        $column_order = $this->column_order[$type];
        $column_search = $this->column_search[$type];
        $order = $this->order[$type];
    
        $i = 0;$a = 0;
        if($_REQUEST['search']['value']){
            foreach ($column_search as $item){
                if($_REQUEST['search']['value']) {  
                    if($i===0) {
                        //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->like($item, $_REQUEST['search']['value']);
                    } else {
                        $this->db->or_like($item, $_REQUEST['search']['value']);
                    }
                    //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
                }
                $i++;
            }
        }else{
            $like_arr = [];
            foreach ($_REQUEST['columns'] as $key) {
                if($key['search']['value']){
                    $index = $column_search[$a-2];
                    $like_arr[$index] = $key['search']['value'];
                    /*if($a===0) {
                        $this->db->like($column_search[$a-2], $key['search']['value']);
                    } else {
                        $this->db->or_like($column_search[$a-2], $key['search']['value']);
                    }*/
                }
                $a++;
            }
            $this->db->like($like_arr);
        }
      
        if(isset($_REQUEST['order'])) {
            if($_REQUEST['order']['0']['column'] == 0) {
                $this->db->order_by(key($order), $order[key($order)]);
            }else{
                $this->db->order_by($column_order[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
            }
        } else if(isset($order)) {
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $result =  $this->db->get_compiled_select();

        $query = explode("WHERE", $result);
        if(count($query) == 1){
            $pecah = explode("ORDER BY", $result);
            $where = "";
        }else{
            $pecah = explode("ORDER BY", $query[1]);
            $where = "WHERE ".$pecah[0];
        }
        
        $order = $pecah[1];
        $start = $_REQUEST['start'];
        $end   = $_REQUEST['length'] + $_REQUEST['start'];

        return array(
            'start'       => $start,
            'end'         => $end,
            'order'       => $order,
            'where'       => $where
        );
    }

}
?>