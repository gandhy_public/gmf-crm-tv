<?php
 
function is_has_session($session) {
  if(!$session) {
    redirect('logout', 'refresh');
  } else {
    return true;
  }
}

function get_session($param) {
  $session = $_SESSION['logged_in'];
  return $session[$param];
}

