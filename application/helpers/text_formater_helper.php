<?php
function raw_datetime_format($date){
  $raw_start_date = strtotime($date);
  return date('Y-M-d H:s', $raw_start_date);
}
?>