<?php
date_default_timezone_set("Asia/Jakarta");
require_once 'application/libraries/spout/src/Spout/Autoloader/autoload.php';
include APPPATH.'libraries/PHPExcel/PHPExcel.php';

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;
use Box\Spout\Writer\Style\Border;
use Box\Spout\Writer\Style\BorderBuilder;

class Survey extends CI_Controller {

    function __construct(){
        parent::__construct();
        //LOAD MODEL
        $this->load->model('M_survey', '', TRUE);
        //LOAD OTHER
        $this->load->library('tcpdf/tcpdf');
        $this->data["session"] = $this->session->userdata('logged_in');
        //SCRIPTING HERE
        if(count($_SESSION['logged_in']) == 0){
        	redirect(base_url());
        }
	}

	private function pdf(){
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('PT SISI');
        $pdf->SetTitle('Document PDF');
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(true);

        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        $pdf->AddPage('L', 'A4');
        $pdf->SetFont('helvetica', '', 10);
        return $pdf;
	}

	private function excel(){
		$writer = WriterFactory::create(Type::XLSX);
        $default_style = (new StyleBuilder())
                ->setFontName('Arial')
                ->setFontSize(10)
                ->setShouldWrapText(false)
                ->build();          
        $writer->setDefaultRowStyle($default_style);
        return $writer;
	}

	function all($type){
	    $excel = new PHPExcel();

	    $style_col = array(
	      'font' => array('bold' => true), // Set font nya jadi bold
	      'alignment' => array(
	        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
	        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
	      ),
	      'borders' => array(
	        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
	        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
	        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
	        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
	      )
	    );
	    
	    $style_row = array(
	      'alignment' => array(
	        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
	      ),
	      'borders' => array(
	        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
	        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
	        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
	        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
	      )
	    );
	    
	    $excel->setActiveSheetIndex(0)->setCellValue('A1', "NO");
	    $excel->getActiveSheet()->mergeCells('A1:A2');
	    $excel->setActiveSheetIndex(0)->setCellValue('B1', "ESN");
	    $excel->getActiveSheet()->mergeCells('B1:B2');
	    $excel->setActiveSheetIndex(0)->setCellValue('C1', "EO");
	    $excel->getActiveSheet()->mergeCells('C1:C2');
	    $excel->setActiveSheetIndex(0)->setCellValue('D1', "WORKSCOPE");
	    $excel->getActiveSheet()->mergeCells('D1:D2');
	    $excel->setActiveSheetIndex(0)->setCellValue('E1', "CUSTOMER");
	    $excel->getActiveSheet()->mergeCells('E1:E2');
	    $excel->setActiveSheetIndex(0)->setCellValue('F1', "RESPONDER");
	    $excel->getActiveSheet()->mergeCells('F1:F2');
	    $excel->setActiveSheetIndex(0)->setCellValue('G1', "SERVICEABLE DATE");
	    $excel->getActiveSheet()->mergeCells('G1:G2');
	    $excel->setActiveSheetIndex(0)->setCellValue('H1', "COMMUNICATION");
	    $excel->getActiveSheet()->mergeCells('H1:I1');
	    $excel->setActiveSheetIndex(0)->setCellValue('H2', "RATE");
	    $excel->setActiveSheetIndex(0)->setCellValue('I2', "COMMENT");
	    $excel->setActiveSheetIndex(0)->setCellValue('J1', "PRODUCT QUALITY");
	    $excel->getActiveSheet()->mergeCells('J1:K1');
	    $excel->setActiveSheetIndex(0)->setCellValue('J2', "RATE");
	    $excel->setActiveSheetIndex(0)->setCellValue('K2', "COMMENT");
	    $excel->setActiveSheetIndex(0)->setCellValue('L1', "QUALITY ASSURANCE & SAFETY");
	    $excel->getActiveSheet()->mergeCells('L1:M1');
	    $excel->setActiveSheetIndex(0)->setCellValue('L2', "RATE");
	    $excel->setActiveSheetIndex(0)->setCellValue('M2', "COMMENT");
	    $excel->setActiveSheetIndex(0)->setCellValue('N1', "ON TIME DELIVERY");
	    $excel->getActiveSheet()->mergeCells('N1:O1');
	    $excel->setActiveSheetIndex(0)->setCellValue('N2', "RATE");
	    $excel->setActiveSheetIndex(0)->setCellValue('O2', "COMMENT");
	    $excel->setActiveSheetIndex(0)->setCellValue('P1', "PERSONNEL COMPETENCY & AVAILABILITY");
	    $excel->getActiveSheet()->mergeCells('P1:Q1');
	    $excel->setActiveSheetIndex(0)->setCellValue('P2', "RATE");
	    $excel->setActiveSheetIndex(0)->setCellValue('Q2', "COMMENT");
	    $excel->setActiveSheetIndex(0)->setCellValue('R1', "OTHER COMMENT");
	    $excel->getActiveSheet()->mergeCells('R1:R2');
	    $excel->setActiveSheetIndex(0)->setCellValue('S1', "AVERAGE RATE");
	    $excel->getActiveSheet()->mergeCells('S1:S2');

	    
	    $excel->getActiveSheet()->getStyle('A1')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('A2')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('B1')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('B2')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('C1')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('C2')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('D1')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('D2')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('E1')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('E2')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('F1')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('F2')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('G1')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('G2')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('H1')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('H2')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('I1')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('I2')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('J1')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('J2')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('K1')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('K2')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('L1')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('L2')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('M1')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('M2')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('N1')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('N2')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('O1')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('O2')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('P1')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('P2')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('Q1')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('Q2')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('R1')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('R2')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('S1')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('S2')->applyFromArray($style_col);

	    $survey = $this->M_survey->get_all_survey();
	    $no = 1;
	    $numrow = 3;
	    foreach($survey as $data){
	      $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
	      $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->SERNR);
	      $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->EO);
	      $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->WORKSCOPE);
	      $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->COMPANY_NAME);
	      $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->NAME);
	      $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data->SERVICEABLE_DATE);
	      $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data->RATE_KATEGORI_1);
	      $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $data->COMMENT_KATEGORI_1);
	      $excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $data->RATE_KATEGORI_2);
	      $excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $data->COMMENT_KATEGORI_2);
	      $excel->setActiveSheetIndex(0)->setCellValue('L'.$numrow, $data->RATE_KATEGORI_3);
	      $excel->setActiveSheetIndex(0)->setCellValue('M'.$numrow, $data->COMMENT_KATEGORI_3);
	      $excel->setActiveSheetIndex(0)->setCellValue('N'.$numrow, $data->RATE_KATEGORI_4);
	      $excel->setActiveSheetIndex(0)->setCellValue('O'.$numrow, $data->COMMENT_KATEGORI_4);
	      $excel->setActiveSheetIndex(0)->setCellValue('P'.$numrow, $data->RATE_KATEGORI_5);
	      $excel->setActiveSheetIndex(0)->setCellValue('Q'.$numrow, $data->COMMENT_KATEGORI_5);
	      $excel->setActiveSheetIndex(0)->setCellValue('R'.$numrow, $data->OTHER_COMMENT);
	      $excel->setActiveSheetIndex(0)->setCellValue('S'.$numrow, $data->AVERAGE_RATE);
	      
	      $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('A'.$numrow)->getAlignment()->setWrapText(true);
	      $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('B'.$numrow)->getAlignment()->setWrapText(true);
	      $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('C'.$numrow)->getAlignment()->setWrapText(true);
	      $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('D'.$numrow)->getAlignment()->setWrapText(true);
	      $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('E'.$numrow)->getAlignment()->setWrapText(true);
	      $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('F'.$numrow)->getAlignment()->setWrapText(true);
	      $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('G'.$numrow)->getAlignment()->setWrapText(true);
	      $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('H'.$numrow)->getAlignment()->setWrapText(true);
	      $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('I'.$numrow)->getAlignment()->setWrapText(true);
	      $excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('J'.$numrow)->getAlignment()->setWrapText(true);
	      $excel->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('K'.$numrow)->getAlignment()->setWrapText(true);
	      $excel->getActiveSheet()->getStyle('L'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('L'.$numrow)->getAlignment()->setWrapText(true);
	      $excel->getActiveSheet()->getStyle('M'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('M'.$numrow)->getAlignment()->setWrapText(true);
	      $excel->getActiveSheet()->getStyle('N'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('N'.$numrow)->getAlignment()->setWrapText(true);
	      $excel->getActiveSheet()->getStyle('O'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('O'.$numrow)->getAlignment()->setWrapText(true);
	      $excel->getActiveSheet()->getStyle('P'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('P'.$numrow)->getAlignment()->setWrapText(true);
	      $excel->getActiveSheet()->getStyle('Q'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('Q'.$numrow)->getAlignment()->setWrapText(true);
	      $excel->getActiveSheet()->getStyle('R'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('R'.$numrow)->getAlignment()->setWrapText(true);
	      $excel->getActiveSheet()->getStyle('S'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('S'.$numrow)->getAlignment()->setWrapText(true);
	      
	      $no++;
	      $numrow++;
	    }
	    // Set width kolom
	    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
	    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
	    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
	    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
	    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
	    $excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
	    $excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
	    $excel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
	    $excel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
	    $excel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
	    $excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
	    $excel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
	    $excel->getActiveSheet()->getColumnDimension('M')->setWidth(30);
	    $excel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
	    $excel->getActiveSheet()->getColumnDimension('O')->setWidth(30);
	    $excel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
	    $excel->getActiveSheet()->getColumnDimension('Q')->setWidth(30);
	    $excel->getActiveSheet()->getColumnDimension('R')->setWidth(40);
	    $excel->getActiveSheet()->getColumnDimension('S')->setWidth(25);
	    
	    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
	    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
	    // Set orientasi kertas jadi LANDSCAPE
	    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	    // Proses file excel
	    $date = date('Y-m-d');
	    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	    header("Content-Disposition: attachment; filename='Feedback List_$date.xlsx'"); // Set nama file excel nya
	    header('Cache-Control: max-age=0');
	    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
	    $write->save('php://output');
	}

}
