<?php
date_default_timezone_set("Asia/Jakarta");
require_once 'application/libraries/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;
use Box\Spout\Writer\Style\Border;
use Box\Spout\Writer\Style\BorderBuilder;

class Dashboard extends CI_Controller {

    function __construct(){
        parent::__construct();
        //LOAD MODEL
        $this->load->model('M_dashboard', '', TRUE);
        //LOAD OTHER
        $this->load->library('tcpdf/tcpdf');
        $this->data["session"] = $this->session->userdata('logged_in');
        //SCRIPTING HERE
        if(count($_SESSION['logged_in']) == 0){
        	redirect(base_url());
        }
	}

	private function pdf(){
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('PT SISI');
        $pdf->SetTitle('Document PDF');
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(true);

        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        $pdf->AddPage('L', 'A4');
        $pdf->SetFont('helvetica', '', 10);
        return $pdf;
	}

	private function excel(){
		$writer = WriterFactory::create(Type::XLSX);
        $default_style = (new StyleBuilder())
                ->setFontName('Arial')
                ->setFontSize(10)
                ->setShouldWrapText(false)
                ->build();          
        $writer->setDefaultRowStyle($default_style);
        return $writer;
	}

	public function project($file,$type){
		if(get_session('user_role_level') == '3') {
            $customer_id = $this->data["session"]["customer_id"];
        } else {
            $customer_id = NULL;
        }
        $listProject = $this->M_dashboard->get_all_project($type,$customer_id);

		if($file == 'excel'){
			$excel = $this->excel();
			if($type == "open"){
				$file_name = "WIP Project_".date('Y-m-d').".xlsx";
			}else{
				$file_name = "Serviceable Project_".date('Y-m-d').".xlsx";
			}
	        $excel->openToFile($file_name); 

	        $data_header = array();
	        $data_header = [
	            "NO",
	            "ESN",
	            "Project No",
	            "EO",
	            "CUSTOMER",
	            "WORKSCOPE",
	            "INDUCTION DATE",
	            "CONTRACTUAL TAT",
	            "CURRENT TAT",
	        ];
	        $excel->addRow($data_header);

	        $data_isi = array(); $no = 1;
	        foreach ($listProject as $key) {
	            if($key->REVTY == "AH"){
	                $workscope = "APU Heavy Repair";
	            }elseif($key->REVTY == "AL"){
	                $workscope = "APU Light Repair";
	            }elseif($key->REVTY == "AM"){
	                $workscope = "APU Medium Repair";
	            }elseif($key->REVTY == "EM"){
	                $workscope = "Engine Minimum";
	            }elseif($key->REVTY == "EO"){
	                $workscope = "Engine Overhaul";
	            }elseif($key->REVTY == "EP"){
	                $workscope = "Engine Performance";
	            }elseif($key->REVTY == "SR"){
	                $workscope = "SPECIAL REQUEST (CM)";
	            }

	            if($key->INDUCT_DATE != null && $key->INDUCT_DATE != "00000000"){
	                $date = new DateTime($key->INDUCT_DATE);
	                $induction_date = $date->format('d F Y');
	            }else{
	                $induction_date = '';
	            }

	            $data_isi[] = [
	                $no++,
	                $key->SERNR,
	                $key->REVNR,
	                $key->TEAM_EO,
	                $key->COMPANY_NAME,
	                $workscope,
	                $induction_date,
	                $key->CONTRACTUAL_TAT,
	                $key->CURRENT_TAT
	            ];
	        }
	        $excel->addRows($data_isi);
	        $excel->close();

	        $this->load->helper('download');
	        force_download($file_name, null);
		}else{
			$pdf = $this->pdf();
	        if($type == "open"){
				$file_name = "WIP Project_".date('Y-m-d').".pdf";
			}else{
				$file_name = "Serviceable Project_".date('Y-m-d').".pdf";
			}

	        $data_isi = ""; $no = 1;
	        foreach ($listProject as $key) {
	            $date = new DateTime($key->INDUCT_DATE);

	            if($key->REVTY == "AH"){
	                $workscope = "APU Heavy Repair";
	            }elseif($key->REVTY == "AL"){
	                $workscope = "APU Light Repair";
	            }elseif($key->REVTY == "AM"){
	                $workscope = "APU Medium Repair";
	            }elseif($key->REVTY == "EM"){
	                $workscope = "Engine Minimum";
	            }elseif($key->REVTY == "EO"){
	                $workscope = "Engine Overhaul";
	            }elseif($key->REVTY == "EP"){
	                $workscope = "Engine Performance";
	            }elseif($key->REVTY == "SR"){
	                $workscope = "SPECIAL REQUEST (CM)";
	            }

	            if($key->INDUCT_DATE != null && $key->INDUCT_DATE != "00000000"){
	                $date = new DateTime($key->INDUCT_DATE);
	                $induction_date = $date->format('d F Y');
	            }else{
	                $induction_date = '';
	            }
	            $data_isi .= '
	            <tr>
	                <td align="center">'.$no++.'</td>
	                <td>'.$key->SERNR.'</td>
	                <td>'.$key->REVNR.'</td>
	                <td>'.$key->TEAM_EO.'</td>
	                <td>'.$key->COMPANY_NAME.'</td>
	                <td>'.$workscope.'</td>
	                <td>'.$induction_date.'</td>
	                <td>'.$key->CONTRACTUAL_TAT.' Days</td>
	                <td>'.$key->CURRENT_TAT.' Days</td>
	            </tr>
	            ';
	        }

	        $data_header = '
	        <table cellspacing="0" cellpadding="1" border="1">
	            <tr>
	                <td width="5%" align="center"><b>NO</b></td>
	                <td width="10%" align="center"><b>ESN</b></td>
	                <td width="10%" align="center"><b>Project No</b></td>
	                <td width="10%" align="center"><b>EO</b></td>
	                <td width="15%" align="center"><b>CUSTOMER</b></td>
	                <td width="15%" align="center"><b>WORKSCOPE</b></td>
	                <td width="11%" align="center"><b>INDUCTION DATE</b></td>
	                <td width="12%" align="center"><b>CONTRACTUAL TAT</b></td>
	                <td width="12%" align="center"><b>CURRENT TAT</b></td>
	            </tr>
	            '.$data_isi.'
	        </table>
	        ';
	        $pdf->writeHTML($data_header, true, false, false, false, 'center');
	        // write some JavaScript code
			//$pdf->IncludeJS('print(true);');
	        $pdf->Output($file_name, 'D'); //I or D
		}
	}

	public function manhours($file,$revnr){
        $listManhours = $this->M_dashboard->get_all_manhours($revnr);

		if($file == 'excel'){
			$excel = $this->excel();
			$file_name = "Manhours_".date('Y-m-d').".xlsx";
	        $excel->openToFile($file_name); 

	        $data_header = array();
	        $data_header = [
	            "NO",
	            "Order Number",
	            "Order Type",
	            "MAT",
	            "Description",
	            "Status",
	            "Mhrs Plan (Hrs)",
	            "Mhrs Actual (Hrs)",
	        ];
	        $excel->addRow($data_header);

	        $data_isi = array(); $no = 1;
	        foreach ($listManhours as $key) {
	            $data_isi[] = [
	                $no++,
	                $key->AUFNR,
	                $key->AUART,
	                $key->ILART,
	                $key->KTEXT,
	                $key->STATUS,
	                $key->ARBEI,
	                $key->ISMNW
	            ];
	        }
	        $excel->addRows($data_isi);
	        $excel->close();

	        $this->load->helper('download');
	        force_download($file_name, null);
		}else{
			$pdf = $this->pdf();
	        $file_name = "Manhours_".date('Y-m-d').".pdf";

	        $data_isi = ""; $no = 1;
	        foreach ($listManhours as $key) {
	            $data_isi .= '
	            <tr>
	                <td align="center">'.$no++.'</td>
	                <td>'.$key->AUFNR.'</td>
	                <td>'.$key->AUART.'</td>
	                <td>'.$key->ILART.'</td>
	                <td>'.$key->KTEXT.'</td>
	                <td>'.$key->STATUS.'</td>
	                <td>'.$key->ARBEI.'</td>
	                <td>'.$key->ISMNW.'</td>
	            </tr>
	            ';
	        }

	        $data_header = '
	        <table cellspacing="0" cellpadding="1" border="1">
	            <tr>
	                <td width="5%" align="center"><b>NO</b></td>
	                <td width="10%" align="center"><b>Order Number</b></td>
	                <td width="10%" align="center"><b>Order Type</b></td>
	                <td width="10%" align="center"><b>MAT</b></td>
	                <td width="27%" align="center"><b>Description</b></td>
	                <td width="15%" align="center"><b>Status</b></td>
	                <td width="11%" align="center"><b>Mhrs Plan (Hrs)</b></td>
	                <td width="12%" align="center"><b>Mhrs Actual (Hrs)</b></td>
	            </tr>
	            '.$data_isi.'
	        </table>
	        ';
	        $pdf->writeHTML($data_header, true, false, false, false, 'center');
	        $pdf->Output($file_name, 'D');
		}
	}

}
