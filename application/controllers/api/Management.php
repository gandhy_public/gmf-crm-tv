<?php
date_default_timezone_set("Asia/Jakarta");
class Management extends CI_Controller
{

	function __construct(){
        parent::__construct();
        //LOAD MODEL
        $this->load->model('M_management', '', TRUE);
        //LOAD OTHER
        $this->data["session"] = $this->session->userdata('logged_in');
        //SCRIPTING HERE
        if(count($_SESSION['logged_in']) == 0){
        	redirect(base_url());
        }
	}

	function ListUser(){
		$editable = $this->input->post('editable');

		$listGroup = $this->M_management->get_list_user(FALSE,FALSE);
		$total = $this->M_management->get_list_user(TRUE,FALSE);
    	$filter = $this->M_management->get_list_user(FALSE,TRUE);

    	$data = array();
		$no = $_REQUEST['start'];
	    foreach ($listGroup as $item) {
			$no++;
			$list = array();
            $list['name']        = "<center>".$item->NAME."</center>";
            $list['username']    = "<center>".$item->USERNAME."</center>";
            $list['role']        = "<center>".$item->USER_ROLE."</center>";
            $list['group']        = "<center>".$item->USER_GROUP."</center>";

			if($item->STATUS){
				$status = "<center><span class='label label-success'>Active</span></center>";
			}else{
				$status = "<center><span class='label label-danger'>Non Active</span></center>";
			}
			$list['status'] = $status;

			if($editable){
				if($item->PASSWORD == "using ldap"){
					$act = '
					<center>
		            <button type="button" data-x="'.$item->ID_USER.'" class="btn btn-success btn-xs btn_edit" title="Edit User"><i class="fa fa-edit"></i></button>
		            </center>
					';
				}else{
					$act = '
					<center>
		            <button type="button" data-x="'.$item->ID_USER.'" class="btn btn-success btn-xs btn_edit" title="Edit User"><i class="fa fa-edit"></i></button>
		            &nbsp;&nbsp;&nbsp;
		            <button type="button" data-x="'.$item->ID_USER.'" class="btn btn-warning btn-xs btn_password" title="Change Password"><i class="fa fa-unlock-alt"></i></button>
		            </center>
					';
				}
			}else{
				$act = '
			<center>
            No action
            </center>
			';
			}
			$list['act'] = $act;


			$data[] = $list;
	    }

	    $results = array(
			"draw" => $_REQUEST["draw"],
			"recordsTotal" => $total,
			"recordsFiltered" => $filter,
			"data" => $data
	    );

	    echo json_encode($results);
	}

	function CreateUser(){
		$ldap = $this->input->post('ldap');
		$nama = $this->input->post('nama');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$role = $this->input->post('role');
		$customer = $this->input->post('customer');
		$group = $this->input->post('group');

		$param = [
			'NAME' => $nama,
			'USERNAME' => $username,
			'PASSWORD' => $password,
			'CUSTOMER_ID' => $customer,
			'GROUP_ID' => $group,
			'ROLE_ID' => $role,
			'STATUS' => '1',
		];

		if($ldap == "on"){
			$param['PASSWORD'] = "using ldap";	
		}

		$cek_user = $this->M_management->cek_user($param['USERNAME'],$param['PASSWORD']);
		if(count($cek_user) > 0){
			$this->session->set_flashdata('fail',"Data user already exist");
			redirect("Management/user");die();
		}else{
			$create_user = $this->M_management->create_user($param);
		}

		if(isset($_POST['config_menu'])){
            $this->M_management->delete_mn_editable_menu($create_user);
            for($a=0;$a<count($_POST['config_menu']);$a++){
                $data = $_POST['config_menu'];
                $param_menu = [
                    'user_id' => $create_user,
                    'menu_id' =>  $data[$a],
                ];
                $this->M_management->create_mn_editable_menu($param_menu);
            }
        }

        if(isset($_POST['config_menu_top'])){
            $this->M_management->delete_mn_editable_menu_top($create_user);
            for($a=0;$a<count($_POST['config_menu_top']);$a++){
                $data = $_POST['config_menu_top'];
                $param_menu_top = [
                    'user_id' => $create_user,
                    'menu_top_id' => $data[$a],
                ];
                $this->M_management->create_mn_editable_menu_top($param_menu_top);
            }
        }

		$this->session->set_flashdata('success',"Success create new user");
		redirect("Management/user");
	}

	function GetUser(){
		try {
			$id_user = $this->input->post('id_user');

			$get_user = $this->M_management->get_user($id_user);
			if(count($get_user) == 0) throw new Exception("Failed get data user", 1);
			$edit_menu = $this->M_management->get_editable_menu($id_user);
			$edit_menu_top = $this->M_management->get_editable_menu_top($id_user);

			$resp = [
				'status' => TRUE,
				'data' => $get_user,
				'menu' => $edit_menu,
				'menu_top' => $edit_menu_top,
			];
			echo json_encode($resp);
		} catch (Exception $e) {
			$resp = [
				'status' => FALSE,
				'data' => [],
				'menu' => [],
				'menu_top' => [],
			];
			echo json_encode($resp);
		}
	}

	function UpdateUser(){
		try {
			$id_user = $this->input->post('id');
			$nama = $this->input->post('nama');
			$username = $this->input->post('username');
			$role = $this->input->post('role');
			$customer = (isset($_POST['customer']) ? $this->input->post('customer') : null);
			$group = $this->input->post('group');
			$status = $this->input->post('status');

			$param = [
				'NAME' => $nama,
				'USERNAME' => $username,
				'CUSTOMER_ID' => $customer,
				'GROUP_ID' => $group,
				'ROLE_ID' => $role,
				'STATUS' => $status,
			];
			$update = $this->M_management->update_user($param,$id_user);
			if(!$update) throw new Exception("Fail Update Data User", 1);

			$this->M_management->delete_mn_editable_menu($id_user);
			if(isset($_POST['config_menu'])){
	            for($a=0;$a<count($_POST['config_menu']);$a++){
	                $data = $_POST['config_menu'];
	                $param_menu = [
	                    'user_id' => $id_user,
	                    'menu_id' =>  $data[$a],
	                ];
	                $this->M_management->create_mn_editable_menu($param_menu);
	            }
	        }

	        $this->M_management->delete_mn_editable_menu_top($id_user);
	        if(isset($_POST['config_menu_top'])){
	            for($a=0;$a<count($_POST['config_menu_top']);$a++){
	                $data = $_POST['config_menu_top'];
	                $param_menu_top = [
	                    'user_id' => $id_user,
	                    'menu_top_id' => $data[$a],
	                ];
	                $this->M_management->create_mn_editable_menu_top($param_menu_top);
	            }
	        }

			$this->session->set_flashdata('success','Success Update Data User');
			redirect('Management/user');
		} catch (Exception $e) {
			$this->session->set_flashdata('fail',$e->getMessage());
			redirect('Management/user');
		}
	}

	function GetEditable(){
		try {
			$id_group = $this->input->post('id_group');
			$data_editable = $this->M_management->get_editable($id_group);
			echo json_encode($data_editable);
		} catch (Exception $e) {
			echo FALSE;
		}
	}

	function UpdatePassword(){
		try {
			$id_user = $this->input->post('id');
			$pass = $this->input->post('password');
			$repass = $this->input->post('repassword');

			$param = [
				'PASSWORD' => $pass
			];
			$update = $this->M_management->update_user($param,$id_user);
			if(!$update) throw new Exception("Fail Update Password User", 1);

			$this->session->set_flashdata('success','Success Update Password User');
			redirect('Management/user');
		} catch (Exception $e) {
			$this->session->set_flashdata('fail',$e->getMessage());
			redirect('Management/user');
		}
	}

	/*==================================================================================================================*/

	function ListGroup(){
		$editable = $this->input->post('editable');

		$listGroup = $this->M_management->get_list_group(FALSE,FALSE);
		$total = $this->M_management->get_list_group(TRUE,FALSE);
    	$filter = $this->M_management->get_list_group(FALSE,TRUE);

    	$data = array();
		$no = $_REQUEST['start'];
	    foreach ($listGroup as $item) {
			$no++;
			$list = array();
			$list['no'] = "<center>".$no."</center>";
			$list['group'] = "<center>".$item->name."</center>";

			if($item->is_active){
				$status = "<center><span class='label label-success'>Active</span></center>";
			}else{
				$status = "<center><span class='label label-danger'>Non Active</span></center>";
			}
			$list['status'] = $status;

			if($editable){
				$act = '
			<center>
            <a href="'.base_url('index.php/Management/EditGroup/').$item->id.'" class="btn btn-success btn-xs btn_edit" title="Edit Group"><i class="fa fa-edit"></i></a>
            &nbsp;&nbsp;&nbsp;
            <a href="'.base_url('index.php/Management/ViewGroup/').$item->id.'" class="btn btn-warning btn-xs btn_detail" title="Detail Group"><i class="fa fa-eye"></i></a>
            </center>
			';
			}else{
				$act = '
			<center>
            <a href="'.base_url('index.php/Management/ViewGroup/').$item->id.'" class="btn btn-warning btn-xs btn_detail" title="Detail Group"><i class="fa fa-eye"></i></a>
            </center>
			';
			}
			$list['act'] = $act;
			
			$data[] = $list;
	    }

	    $results = array(
			"draw" => $_REQUEST["draw"],
			"recordsTotal" => $total,
			"recordsFiltered" => $filter,
			"data" => $data
	    );

	    echo json_encode($results);
	}

	function CreateGroup(){
		try {
			if(!isset($_POST['menu'])) throw new Exception("Failed create menu group", 1);
			
			//Create Group Name
			$group = $this->input->post('group');
			$id_group = $this->M_management->create_group($group);
			if(!$id_group) throw new Exception("Failed create menu group", 1);
			
			//Create Menu Sidebare
			unset($_POST['group']);
			$mn_menu = array();
			foreach ($_POST['menu'] as $key) {
				$data = explode("_", $key);
				$mn_menu[] = [
					'group_id' => $id_group,
					'modul_id' => $data[0],
					'menu_id' => $data[1]
				];
			}
			$create_mn_menu = $this->M_management->create_mn_menu($mn_menu);
			if(!$create_mn_menu) throw new Exception("Failed create menu group", 1);

			//Create Menu TOP
			unset($_POST['menu']);
			if(isset($_POST['menu_top'])){
				$mn_menu_top = array();
				foreach ($_POST['menu_top'] as $key) {
					$data = explode("_", $key);
					$mn_menu_top[] = [
						'group_id' => $id_group,
						'modul_id' => $data[0],
						'menu_id' => $data[1],
						'menu_top_id' => $data[2]
					];
				}
				$create_mn_menu_top = $this->M_management->create_mn_menu_top($mn_menu_top);
				if(!$create_mn_menu_top) throw new Exception("Failed create menu group", 1);
			}


			$this->session->set_flashdata("success","Success create menu group");
			redirect("Management/menu");
		} catch (Exception $e) {
			$this->session->set_flashdata("fail",$e->getMessage());
			redirect("Management/menu");
		}
	}

	function GetGroup(){
		try {
			$id_group = $this->input->post('id_group');

			$get_group = $this->M_management->get_group($id_group);
			if(!$get_group) throw new Exception("Failed get data detail group", 1);
			$get_menu = $this->M_management->get_group_menu($id_group);
			if(!$get_menu) throw new Exception("Failed get data menu", 1);
			$get_menu_top = $this->M_management->get_group_menu_top($id_group);
			if(!$get_menu_top) $get_menu_top = FALSE;

			$results = [
				'status' => TRUE,
				'group' => $get_group,
				'menu' => $get_menu,
				'menu_top' => $get_menu_top
			];
			echo json_encode($results,true);
		} catch (Exception $e) {
			$results = [
				'status' => FALSE,
				'message' => $e->getMessage(),
			];
			echo json_encode($results,true);
		}
	}

	function UpdateGroup(){
		try {
			if(!isset($_POST['id_group'])) throw new Exception("Failed update menu group", 1);
			if(!isset($_POST['group'])) throw new Exception("Failed update menu group", 1);
			if(!isset($_POST['menu'])) throw new Exception("Failed update menu group", 1);

			$id_group = $this->input->post('id_group');
			$name_group = $this->input->post('group');
			$status_group = $this->input->post('status');
			$update_group = $this->M_management->update_group($id_group,$name_group,$status_group);
			if(!$update_group) throw new Exception("Failed update menu group", 1);

			$this->M_management->delete_mn_menu($id_group);
			$this->M_management->delete_mn_menu_top($id_group);
			
			//Create Menu Sidebare
			$mn_menu = array();
			foreach ($_POST['menu'] as $key) {
				$data = explode("_", $key);
				$mn_menu[] = [
					'group_id' => $id_group,
					'modul_id' => $data[0],
					'menu_id' => $data[1]
				];
			}
			$create_mn_menu = $this->M_management->create_mn_menu($mn_menu);
			if(!$create_mn_menu) throw new Exception("Failed update menu group", 1);

			//Create Menu TOP
			if(isset($_POST['menu_top'])){
				$mn_menu_top = array();
				foreach ($_POST['menu_top'] as $key) {
					$data = explode("_", $key);
					$mn_menu_top[] = [
						'group_id' => $id_group,
						'modul_id' => $data[0],
						'menu_id' => $data[1],
						'menu_top_id' => $data[2]
					];
				}
				$create_mn_menu_top = $this->M_management->create_mn_menu_top($mn_menu_top);
				if(!$create_mn_menu_top) throw new Exception("Failed update menu group", 1);
			}

			$this->session->set_flashdata("success","Success update menu group");
			redirect("Management/EditGroup/".$id_group);
		} catch (Exception $e) {
			$this->session->set_flashdata("fail",$e->getMessage());
			redirect("Management/menu");	
		}
	}

}
?>