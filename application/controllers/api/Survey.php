<?php
date_default_timezone_set("Asia/Jakarta");
class Survey extends CI_Controller
{

	function __construct(){
        parent::__construct();
        //LOAD MODEL
        $this->load->model('M_survey', '', TRUE);
        //LOAD OTHER
        $this->data["session"] = $this->session->userdata('logged_in');
        //SCRIPTING HERE
        if(count($_SESSION['logged_in']) == 0){
        	redirect(base_url());
        }
	}

	function GetListSurvey(){

		$listSurvey = $this->M_survey->get_list_survey(FALSE,FALSE);
		$total = $this->M_survey->get_list_survey(TRUE,FALSE);
    	$filter = $this->M_survey->get_list_survey(FALSE,TRUE);
		
		$data = array();
		$no = $_REQUEST['start'];
	    foreach ($listSurvey as $item) {
			$no++;
			$list = array();
			$list['esn'] = "<center>".$item->SERNR."</center>";
			$list['eo'] = "<center>".$item->EO."</center>";
			$list['workscope'] = "<center>".$item->WORKSCOPE."</center>";
			$list['customer'] = "<center>".$item->COMPANY_NAME."</center>";
			$list['responder'] = "<center>".$item->NAME."</center>";
			$serviceable = new DateTime($item->SERVICEABLE_DATE);
			$list['serviceable'] = "<center>".date_format($serviceable, 'd F Y')."</center>";
			$list['score'] = "<center>".$item->AVERAGE_RATE."</center>";
			$list['action'] = "<center><a href='".base_url('index.php/Survey/view/').$item->EQUNR."/".$item->REVNR."'><button class='btn btn-success' >VIEW SURVEY</button></a></center>";


			$data[] = $list;
	    }

	    $results = array(
			"draw" => $_REQUEST["draw"],
			"recordsTotal" => $total,
			"recordsFiltered" => $filter,
			"data" => $data
	    );

	    echo json_encode($results);
	}

}
?>