<?php
date_default_timezone_set("Asia/Jakarta");
class Dashboard extends CI_Controller
{

	function __construct(){
        parent::__construct();
        //LOAD MODEL
        $this->load->model('M_dashboard', '', TRUE);
        $this->load->model('M_survey', '', TRUE);
        //LOAD OTHER
        $this->data["session"] = $this->session->userdata('logged_in');
        //SCRIPTING HERE
        if(count($_SESSION['logged_in']) == 0){
        	redirect(base_url());
        }
	}

	private function _array_clear($data, $val = NULL){
        $tmp = [];
        foreach ($data as $key => $value) {
            $tmp[] = $value->{$val};
        }
        return $tmp;
    }

	function GetListProject($type){
		if(get_session('user_role_level') == '3') {
            $customer_id = $this->data["session"]["customer_id"];
        } else {
            $customer_id = "";
        }

		$listProject = $this->M_dashboard->get_list_project($type,$customer_id,FALSE,FALSE);
		$total = $this->M_dashboard->get_list_project($type,$customer_id,TRUE,FALSE);
    	$filter = $this->M_dashboard->get_list_project($type,$customer_id,FALSE,TRUE);
		
		$data = array();
		$no = $_REQUEST['start'];
	    foreach ($listProject as $item) {
			$no++;
			$list = array();
			$list['esn'] = "<a href='".base_url()."index.php/dashboard/overview/".$item->EQUNR."/".$item->REVNR."/".$type."'>".$item->SERNR."</a>";
			$list['project_no'] = $item->REVNR;
			$list['eo'] = $item->TEAM_EO;
			$list['customer'] = $item->COMPANY_NAME;

			if($item->REVTY == "AH"){
				$workscope = "APU Heavy Repair";
			}elseif($item->REVTY == "AL"){
				$workscope = "APU Light Repair";
			}elseif($item->REVTY == "AM"){
				$workscope = "APU Medium Repair";
			}elseif($item->REVTY == "EM"){
				$workscope = "Engine Minimum";
			}elseif($item->REVTY == "EO"){
				$workscope = "Engine Overhaul";
			}elseif($item->REVTY == "EP"){
				$workscope = "Engine Performance";
			}elseif($item->REVTY == "SR"){
				$workscope = "SPECIAL REQUEST (CM)";
			}
			$list['workscope'] = $workscope;

			if($type == "open"){
				if($item->INDUCT_DATE != null && $item->INDUCT_DATE != "00000000"){
					$date = new DateTime($item->INDUCT_DATE);
					$list['induction_date'] ="<center>".$date->format('d F Y')."</center>";
				}else{
					$list['induction_date'] = '';
				}
			}else{
				if($item->ACT_FINISH != null && $item->ACT_FINISH != "00000000"){
					$date = new DateTime($item->ACT_FINISH);
					$list['induction_date'] ="<center>".$date->format('d F Y')."</center>";
				}else{
					$list['induction_date'] = '';
				}
			}

			//if((int)$item->CONTRACTUAL_TAT == 0) $item->CONTRACTUAL_TAT = "-";
			$list['contractual_tat'] = "<center>".(((int)$item->CONTRACTUAL_TAT)+1)."</center>";
			

			//if((int)$item->CURRENT_TAT == 0) $item->CURRENT_TAT = "-";
			$list['current_tat'] = "<center>".(((int)$item->CURRENT_TAT)+1)."</center>";

			$cek = ((int)$item->CURRENT_TAT) - ((int)$item->CONTRACTUAL_TAT);
			if($cek > 0) $list['cek'] = "1";
			else $list['cek'] = "0";

			$list['action'] = "<center><button type='button' data-x='".$item->REVNR."' class='btn btn-warning btn_sync' data-toggle='tooltip' title='Sync with SAP''><i class='fa fa-refresh'></i></button></center>";


			$data[] = $list;
	    }

	    $results = array(
			"draw" => $_REQUEST["draw"],
			"recordsTotal" => $total,
			"recordsFiltered" => $filter,
			"data" => $data
	    );

	    echo json_encode($results);
	}

	function GetListJobcard(){
		$docno = $this->input->post('docno');
		$revnr = $this->input->post('revnr');
		$listJobcard = $this->M_dashboard->get_list_jobcard($docno,$revnr,FALSE,FALSE);
		$total = $this->M_dashboard->get_list_jobcard($docno,$revnr,TRUE,FALSE);
    	$filter = $this->M_dashboard->get_list_jobcard($docno,$revnr,FALSE,TRUE);

    	$data = array();
		$no = $_REQUEST['start'];
	    foreach ($listJobcard as $item) {
			$no++;
			$list = array();
			$list['mat'] = $item->ILART;
			$list['description'] = $item->KTEXT;
			$list['status'] = $item->TXT_STAT;


			$data[] = $list;
	    }

	    $results = array(
			"draw" => $_REQUEST["draw"],
			"recordsTotal" => $total,
			"recordsFiltered" => $filter,
			"data" => $data
	    );

	    echo json_encode($results);
	}

	function GetDetailOverview(){
		$docno = $this->input->post('docno');
		$revnr = $this->input->post('revnr');

		$result = array();
		if(!empty($docno) && !empty($revnr)) {
			$listProject = $this->M_dashboard->get_overview($docno, $revnr);
        	$tmp = [];
        	$n_result = [];

        	foreach ($listProject as $key => $value) {
				$tmp = $value;
				if (isset($tmp['REVBD']) && isset($tmp['REVED'])) {
					$date1 = date('Ymd', strtotime($tmp['REVBD']));
					$date2 = date('Ymd', strtotime($tmp['REVED']));
					$start = date_create($date1);
					$end = date_create($date2);
					$tmp['CURRENT_TAT'] = (date_diff($start, $end)->format("%a"));
					$data['PlanSVDate'] = $tmp['CURRENT_TAT'];
				} else if (isset($tmp['REVBD'])) {
					$date1 = date('Ymd', strtotime($tmp['REVBD']));
					$date2 = date('Ymd');
					$start = date_create($date1);
					$end = date_create($date2);
					$tmp['CURRENT_TAT'] = (date_diff($start, $end)->format("%a"));
					$data['PlanSVDate'] = '-';
				} else {
					$tmp['CURRENT_TAT'] = '-';
					$data['PlanSVDate'] = '-';
				}
				$data['ACT_TAT'] = $tmp['ACT_TAT'] == null ? 0 : $tmp['ACT_TAT'];
				$data['ESN'] = $tmp['SERNR'];
				$data['TEAM_EO'] = $tmp['TEAM_EO'];
				$data['WORKSCOPE'] = $tmp['WORKSCOPE'];
				$data['REVTY'] = $tmp['REVTY'];
				$data['INDUCT_DATE'] = $tmp['INDUCT_DATE'];
				$data['CONTRACTUAL_TAT'] = $tmp['CONTRACTUAL_TAT'];
				$data['ATWRT'] = $tmp['ATWRT'];
				$data['ENGINE_APU'] = $tmp['ENGINE_APU'];
				$n_result[] = $tmp;
			}

			$data['listProject'] = $n_result;

			$total_disassembly = $this->M_dashboard->get_part_overview($docno, $revnr, "disassembly", "total");
			$closed_disassembly = $this->M_dashboard->get_part_overview($docno, $revnr, "disassembly" ,"closed");
			$data['percentage_disassembly'] = @round(($closed_disassembly / $total_disassembly) * 100);

			$total_inspection = $this->M_dashboard->get_part_overview($docno, $revnr, "inspection", "total");
			$closed_inspection = $this->M_dashboard->get_part_overview($docno, $revnr, "inspection", "closed");
			$data['percentage_inspection'] = @round(($closed_inspection / $total_inspection) * 100);

			$total_repair = $this->M_dashboard->get_part_overview($docno, $revnr, "repair", "total");
			$closed_repair = $this->M_dashboard->get_part_overview($docno, $revnr, "repair", "closed");
			$data['percentage_repair'] = @round(($closed_repair / $total_repair) * 100);

			$total_assembly = $this->M_dashboard->get_part_overview($docno, $revnr, "assembly", "total");
			$closed_assembly = $this->M_dashboard->get_part_overview($docno, $revnr, "assembly", "closed");
			$data['percentage_assembly'] = @round(($closed_assembly / $total_assembly) * 100);

			$total_test = $this->M_dashboard->get_part_overview($docno, $revnr, "test", "total");
			$closed_test = $this->M_dashboard->get_part_overview($docno, $revnr, "test", "closed");
			$data['percentage_test'] = @round(($closed_test / $total_test) * 100);

			$total_qec = $this->M_dashboard->get_part_overview($docno, $revnr, "qec", "total");
			$closed_qec = $this->M_dashboard->get_part_overview($docno, $revnr, "qec", "closed");
			$data['percentage_qec'] = @round(($closed_qec / $total_qec) * 100);

			$total_adsb = $this->M_dashboard->get_part_overview($docno, $revnr, "adsb", "total");
			$closed_adsb = $this->M_dashboard->get_part_overview($docno, $revnr, "adsb", "closed");
			$data['percentage_adsb'] = @round(($closed_adsb / $total_adsb) * 100);

			$total_spec = $this->M_dashboard->get_part_overview($docno, $revnr, "spec", "total");
			$closed_spec = $this->M_dashboard->get_part_overview($docno, $revnr, "spec", "closed");
			$data['percentage_spec'] = @round(($closed_spec / $total_spec) * 100);

			$totalJobCard = $this->M_dashboard->get_part_overview($docno, $revnr, "jc", "total");
			$totalJcOpen = $this->M_dashboard->get_part_overview($docno, $revnr, "jc", "open");
			$totalJcProgress = $this->M_dashboard->get_part_overview($docno, $revnr, "jc", "progress");
			$totalJcClosed = $this->M_dashboard->get_part_overview($docno, $revnr, "jc", "closed");

			$data['percentage_jc_open'] = @round(($totalJcOpen / $totalJobCard) * 100);
			$data['percentage_jc_progress'] = @round(($totalJcProgress / $totalJobCard) * 100);
			$data['percentage_jc_closed'] = @round(($totalJcClosed / $totalJobCard) * 100);

			$data['totalJcClosed'] = $this->M_dashboard->get_part_overview($docno, $revnr, "jc", "closed");
			$data['totalJobCard'] = $this->M_dashboard->get_part_overview($docno, $revnr, "jc", "total");
			
			$result['status'] = 'Success';
			$result['data'] = $data;
		} else {
			$result['status'] = 'Failed';
		}

		echo json_encode($result);
	}

	function GetScurve($docno,$revnr){
		$gstrp = $this->M_dashboard->get_GSTRP($docno, $revnr);
        $data_gstrp = $this->_array_clear($gstrp, 'GSTRP');
        array_push($data_gstrp, date('d-m-Y'));
		$plan = $this->M_dashboard->get_PLAN($docno, $revnr);
        $data_plan = [];
        if ($plan) {
            $tPlan = $this->_array_clear($plan, 'AUART');

            $data_plan = $tPlan;
            $juml = $data_plan[0];
            for ($i = 1; $i < count($tPlan); $i++) {
                $juml += $tPlan[$i];
                $data_plan[$i] = $juml;
            }
            array_push($data_plan, $juml);
        }
        $tmp = [];
        $n_result_actual = [];
        $actual = $this->M_dashboard->get_ACTUAL($docno, $revnr);
        if (isset($actual)) {
            foreach ($actual as $key => $value) {
                $tmp = $value;
                $tmp['cumulative'] = 0;
                foreach ($actual as $key2 => $value2) {
                    if ($value2['TGL'] <= $tmp['TGL']) {
                        $tmp['cumulative'] += $value2['AUART'];
                    }
                }
                $i = $i + 1;
                $n_result_actual[] = $tmp;
            }
        }
        $data_actual = [];
        foreach ($n_result_actual as $rows) {
            $data_actual[] = $rows['cumulative'];
        }
        
        $dataSCurve=[
			'gstrp' => $data_gstrp,
			'plan' => $data_plan,
			'actual' => $data_actual,
			'lineIndex' => count($data_gstrp)-1
		];
      
        echo json_encode($dataSCurve,TRUE);
	}

	function GetListManhours(){
		$revnr = $this->input->post('revnr');
		$listManhours = $this->M_dashboard->get_list_manhours($revnr,FALSE,FALSE);
		$total = $this->M_dashboard->get_list_manhours($revnr,TRUE,FALSE);
    	$filter = $this->M_dashboard->get_list_manhours($revnr,FALSE,TRUE);

    	$tmp = [];
        $manhour_plan = 0;
        $manhour_actual = 0;

        $manhour_jobcard = 0;
        $manhour_mdr = 0;

        $act_jobcard = 0;
        $act_mdr = 0;
        foreach ($listManhours as $key => $value) {

            $manhour_plan += (float) $value->ARBEI;
            $manhour_actual += (float) $value->ISMNW;

            $tmp = $value;
            if ($value->AUART == "GA02") {
                $tmp->MAT = 'MDR';
                $manhour_mdr += (float) $value->ARBEI;
                $act_mdr += (float) $value->ISMNW;
            } else {
                $tmp->MAT = 'JOB CARD';
                $manhour_jobcard += (float) $value->ARBEI;
                $act_jobcard += (float) $value->ISMNW;
            }

            $data['listManhours'][] = $tmp;
        }

    	$data = array();
		$no = $_REQUEST['start'];
	    foreach ($listManhours as $item) {
			$no++;
			$list = array();
			$list['order_number'] = $item->AUFNR;
			$list['order_type'] = $item->AUART;
			$list['mat'] = $item->ILART;
			$list['description'] = $item->KTEXT;
			$list['status'] = $item->STATUS;
			$list['plan'] = floatval($item->ARBEI) . PHP_EOL;
			$list['actual'] = floatval($item->ISMNW) . PHP_EOL;


			$data[] = $list;
	    }

	    $results = array(
			"draw" => $_REQUEST["draw"],
			"recordsTotal" => $total,
			"recordsFiltered" => $filter,
			"data" => $data
	    );

	    echo json_encode($results);
	}

	function GetChartManhours(){
		$revnr = $this->input->post('revnr');
		$chartManhour = $this->M_dashboard->get_chart_manhours($revnr);

        $tmp = [];
        $manhour_plan = 0;
        $manhour_actual = 0;

        $manhour_jobcard = 0;
        $manhour_mdr = 0;

        $act_jobcard = 0;
        $act_mdr = 0;

        foreach ($chartManhour as $key => $value) {

            $manhour_plan += $value->ARBEI;
            $manhour_actual += $value->ISMNW;

            $tmp = $value;
            if ($value->AUART == "GA02") {
                $tmp->MAT = 'MDR';
                $manhour_mdr += (float) $value->ARBEI;
                $act_mdr += (float) $value->ISMNW;
            } else {
                $tmp->MAT = 'JOB CARD';
                $manhour_jobcard += (float) $value->ARBEI;
                $act_jobcard += (float) $value->ISMNW;
            }

            $data['listManhour'][] = $tmp;
        }

        $data['chart']['total'] = [$manhour_plan,$manhour_actual];
        $data['chart']['jobcard'] = [$manhour_jobcard,$act_jobcard];
        $data['chart']['mdr'] = [$manhour_mdr,$act_mdr];

        echo json_encode($data);
	}

	function GetListDeviation(){
		$revnr = $this->input->post('revnr');
		$docno = $this->input->post('docno');

		$listDeviation = $this->M_dashboard->get_list_deviation($docno,FALSE,FALSE);
		$total = $this->M_dashboard->get_list_deviation($docno,TRUE,FALSE);
    	$filter = $this->M_dashboard->get_list_deviation($docno,FALSE,TRUE);

    	$data = array();
		$no = $_REQUEST['start'];
	    foreach ($listDeviation as $item) {
			$no++;
			$list = array();
			$list['no'] = $no;
            $list['dev_no'] = $item->DEV_NUMBER;
            $list['eng_tat'] = $item->DELAY_TAT;
            $list['cus_tat'] = $item->DELAY_CUSTOMER;
            $list['prc_tat_increase'] = $item->CG_DELAY_PROCESS;
            $list['current_gate'] = $item->CURRENT_GATE;
            $list['next_gate_eff'] = $item->NEXT_GATE_EFFCTD;
            $list['dev_reason'] = $item->DEVIATION_REASON;
            $list['prc_delayed'] = $item->PROCESS_DELAYED;
            $list['department'] = $item->DEPARTEMENT;
            $list['root_cause'] = $item->ROOT_CAUSE;
            $list['corrective_action'] = $item->CORECTIVE_ACTION;
            $list['created_by'] = $item->CREATED_BY;
            $list['created_date'] = $item->CREATED_DATE;

			$data[] = $list;
	    }

	    $results = array(
			"draw" => $_REQUEST["draw"],
			"recordsTotal" => $total,
			"recordsFiltered" => $filter,
			"data" => $data
	    );

	    echo json_encode($results);
	}

	function GetChartDeviation(){
		$revnr = $this->input->post('revnr');
		$docno = $this->input->post('docno');

		$listDeviation = $this->M_dashboard->get_chart_deviation($docno);

		$count_gate = [0, 0, 0, 0, 0, 0, 0, 0, 0];

        foreach ($listDeviation as $key => $value) {

            $count_gate[($value->CURRENT_GATE - 1)] = $count_gate[($value->CURRENT_GATE - 1)] + 1;
        }
        $data['count_gate'] = $count_gate;

	    echo json_encode($data);
	}

	function GetListHighlight(){
		$revnr = $this->input->post('revnr');
		$docno = $this->input->post('docno');

		$listHighlight = $this->M_dashboard->get_list_highlight($docno,FALSE,FALSE);
		$total = $this->M_dashboard->get_list_highlight($docno,TRUE,FALSE);
    	$filter = $this->M_dashboard->get_list_highlight($docno,FALSE,TRUE);

    	$data = array();
		$no = $_REQUEST['start'];
	    foreach ($listHighlight as $item) {
			$no++;
			$list = array();
			$list['no'] = $no;
            $list['hl_no'] = $item->HIGHLIGHT_NUMBER;
            $list['applicable_module'] = $item->APPLCBLE_MODULE;
            $list['highlight'] = $item->HIGHLIGHT;
            $list['status'] = ($item->APPROVAL_STATUS=='CL')?'Close': 'Open';
            $list['created_by'] = $item->CREATED_BY;
            $list['created_date'] = date('d-m-Y', strtotime($item->CREATED_DATE));

			$data[] = $list;
	    }

	    $results = array(
			"draw" => $_REQUEST["draw"],
			"recordsTotal" => $total,
			"recordsFiltered" => $filter,
			"data" => $data
	    );

	    echo json_encode($results);
	}

	function GetChartHighlight(){
		$revnr = $this->input->post('revnr');
		$docno = $this->input->post('docno');

		$listHighlight = $this->M_dashboard->get_chart_highlight($docno);

		$status = [0, 0];
        foreach ($listHighlight as $key => $value) {
            if ($value->APPROVAL_STATUS == 'CL') {
                $status[1] = $status[1] + 1;
            } else if ($value->APPROVAL_STATUS == 'CR') {
                $status[0] = $status[0] + 1;
            }
        }

        $data['status'] = $status;
        echo json_encode($data);
	}

	function GetListReport(){
		$docno = $this->input->post('docno');
		$editable = $this->input->post('editable');
		$type = $this->input->post('type');
		$listReport = $this->M_dashboard->get_list_report($docno,FALSE,FALSE);
		$total = $this->M_dashboard->get_list_report($docno,TRUE,FALSE);
    	$filter = $this->M_dashboard->get_list_report($docno,FALSE,TRUE);

    	$data = array();
		$no = $_REQUEST['start'];
	    foreach ($listReport as $item) {
			$no++;
			$list = array();
            $list['week'] = $item->WEEK;
            $list['detail'] = str_replace("\n","<br>",$item->DETAIL_PROJECT);
            $list['date'] = $item->DATE_INPUT;
            if($editable && $type == "open"){
            	$act = "
            	<center>
            	<button data-x='{$item->ID}' class='btn btn-warning btn-sm btn-edit' style='padding:6px 10px;'>
            		<i class='fa fa-pencil'></i>
            	</button>
                <button data-x='{$item->ID}' class='btn btn-danger btn-sm btn-delete' style='padding:6px 10px'>
                	<i class='fa fa-trash'></i>
                </button>
                </center>";
            }else{
            	$act = "<center>No Action</center>";
            }
            $list['action'] = $act;

			$data[] = $list;
	    }

	    $results = array(
			"draw" => $_REQUEST["draw"],
			"recordsTotal" => $total,
			"recordsFiltered" => $filter,
			"data" => $data
	    );

	    echo json_encode($results);
	}

	function CreateReport($docno,$revnr){
		$week = $this->input->post('WEEK');
		$detail = $this->input->post('DETAIL_PROJECT');
		$date = $this->input->post('DATE');
		$equnr = $this->input->post('EQUNR');

		$create = $this->M_dashboard->create_report($week,$detail,$date,$equnr);
		if($create){
			$this->session->set_flashdata("success","Success create report");
		}else{
			$this->session->set_flashdata("fail","Failed create report");
		}
		redirect("dashboard/eo_report/$docno/$revnr/open");
	}

	function DeleteReport(){
		$id = $this->input->post('id');
		$delete = $this->M_dashboard->delete_report($id);
		echo $delete;
	}

	function DetailReport(){
		$id = $this->input->post('id');
		$detail = $this->M_dashboard->detail_report($id);
		echo json_encode($detail);
	}

	function UpdateReport(){
		$week = $this->input->post('WEEK');
		$detail = $this->input->post('DETAIL_PROJECT');
		$date = $this->input->post('DATE');
		$id = $this->input->post('id');

		$docno = $this->input->post('docno');
		$revnr = $this->input->post('revnr');

		$update = $this->M_dashboard->update_report($week,$detail,$date,$id);
		if($update){
			$this->session->set_flashdata("success","Success update report");
		}else{
			$this->session->set_flashdata("fail","Failed update report");
		}
		redirect("dashboard/eo_report/$docno/$revnr/open");
	}

	function CreateSurvey(){
		try {
			$SERNR = $this->input->post('SERNR');
			$REVNR = $this->input->post('REVNR');
			$EQUNR = $this->input->post('EQUNR');
			$COMPANY_NAME = $this->input->post('COMPANY_NAME');
			$EO = $this->input->post('EO');
			$WORKSCOPE = $this->input->post('WORKSCOPE');
			$AEDAT = date_create($this->input->post('AEDAT'));
			$AEDAT = date_format($AEDAT, 'Y-m-d');
			$rating = $this->input->post('rating');
			$comment = $this->input->post('comment');
			$other_comment = $this->input->post('other_comment');
			$average = $this->input->post('average');

			$create_survey = $this->M_survey->create_survey($SERNR,$REVNR,$EQUNR,$COMPANY_NAME,$EO,$WORKSCOPE,$AEDAT,$rating,$comment,$other_comment,$average);
			if(!$create_survey) throw new Exception("Failed Create Survey", 1);
			
			$this->session->set_flashdata('success','Success survey');
			redirect("dashboard/survey/$EQUNR/$REVNR/close");
		} catch (Exception $e) {
			$this->session->set_flashdata('fail',$e->getMessage());
			redirect("dashboard/survey/$EQUNR/$REVNR/close");
		}
	}

	function GetSurvey(){
		try {
			$revnr = $this->input->post('revnr');

			$get_survey = $this->M_survey->get_survey($revnr);
			if(!$get_survey) throw new Exception("Error Processing Request", 1);
			
			echo TRUE;
		} catch (Exception $e) {
			echo FALSE;
		}
	}

}
?>