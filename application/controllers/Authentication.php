<?php
date_default_timezone_set("Asia/Jakarta");
class Authentication extends CI_Controller {

	function __construct(){
        parent::__construct();
        //LOAD MODEL
        $this->load->model('M_login', '', TRUE);
    }

    function index(){
        if($this->session->userdata('logged_in')){
            redirect('dashboard/');
        }else{
            $this->load->helper(array('form'));
            $this->load->view('login');
        }
    }

    function login(){
        try {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $cek_user = $this->M_login->login($username,$password);
            if(!$cek_user['status']) throw new Exception($cek_user['message'], 1);

            $sess_array = array(
                'id_user' => $cek_user['data']['user_id'],
                'username' => $cek_user['data']['user_username'],
                'name' => $cek_user['data']['user_name'],
                'user_status' => $cek_user['data']['user_status'],
                'user_role_id' => $cek_user['data']['user_role_id'],
                'user_role_level' => $cek_user['data']['user_role_level'],
                'user_role' => $cek_user['data']['user_role'],
                'group_id' => $cek_user['data']['user_group_id'],
                'group_name' => $cek_user['data']['user_group'],
                'group_status' => $cek_user['data']['user_group_status'],
                'customer_id' => $cek_user['data']['user_customer_id'],
            );
            $this->session->set_userdata('logged_in', $sess_array);

            $update = $this->M_login->update_last_log($sess_array['id_user']);

            $this->GetMenu($sess_array['group_id'],$sess_array['id_user']);
            $this->GetMenuTop($sess_array['group_id'],$sess_array['id_user']);
            
            $this->session->set_flashdata('success', 'success login');
            redirect('dashboard');
        } catch (Exception $e) {
            $this->session->set_flashdata('fail', $e->getMessage());
            redirect('login');
        }
    }

    function GetMenu($group_id,$user_id){
        $get_menu = $this->M_login->get_menu($group_id,$user_id);
        $menu['menu'] = $get_menu;

        $cookie_name = "all_menu";
        $cookie_value = json_encode($menu);
        setcookie($cookie_name, $cookie_value, 2147483647, "/");
    }

    function GetMenuTop($group_id,$user_id){
        $get_menu_top = $this->M_login->get_menu_top($group_id,$user_id);
        $menu['menu_top'] = $get_menu_top;

        $cookie_name = "all_menu_top";
        $cookie_value = json_encode($menu);
        setcookie($cookie_name, $cookie_value, 2147483647, "/");
    }

    function logout(){
        $cookie_name = "all_menu_top";
        $cookie_value = null;
        setcookie($cookie_name, $cookie_value, 2147483647, "/");

        $cookie_name = "all_menu";
        $cookie_value = null;
        setcookie($cookie_name, $cookie_value, 2147483647, "/");

        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();

        redirect(base_url());
    }

}
?>