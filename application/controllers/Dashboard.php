<?php
date_default_timezone_set("Asia/Jakarta");
class Dashboard extends CI_Controller {

    function __construct(){
        parent::__construct();
        //LOAD LIBRARY
        $this->load->library('tcpdf/tcpdf');
        //LOAD MODEL
        $this->load->model('M_dashboard', '', TRUE);
        $this->load->model('M_survey', '', TRUE);
        //LOAD OTHER
        $this->data["session"] = $this->session->userdata('logged_in');
        $this->url = base_url()."index.php/".$this->uri->segment(1)."/".$this->uri->segment(2);
        $this->menu_top = json_decode($_COOKIE['all_menu_top'],true);
        error_reporting(0);
        $this->menu_top = $this->menu_top['menu_top']['dashboard/'.$this->uri->segment(5)];
        //SCRIPTING HERE
        if(count($_SESSION['logged_in']) == 0){
            redirect(base_url());
        }
    }

    function cek(){
        print('<pre>'.print_r($_SESSION,TRUE).'</pre>');
        print('<pre>'.print_r(json_decode($_COOKIE['all_menu'],TRUE),TRUE).'</pre>');
        print('<pre>'.print_r(json_decode($_COOKIE['all_menu_top'],TRUE),TRUE).'</pre>');die();
    }

    function index(){
        $menu = json_decode($_COOKIE['all_menu'],TRUE);
        $menu = $menu['menu'];
        $menu_utama = array_keys($menu);
        $menu_utama = $menu_utama[0];
        $menu_url = $menu[$menu_utama][0]['url'];

        redirect($menu_url);
    }

    function open(){
        $data["session"] = $this->data["session"];
        $data['content'] = 'dashboard/index';
        $data['script'] = 'dashboard/script_index';
        $data['type'] = 'open';
        $data['title'] = 'WIP Project';
        
        $this->load->view('template', $data);
    }

    function close(){
        $data["session"] = $this->data["session"];
        $data['content'] = 'dashboard/index';
        $data['script'] = 'dashboard/script_index';
        $data['type'] = 'close';
        $data['title'] = 'Serviceable Project';
        
        $this->load->view('template', $data);
    }

    function overview($docno, $revnr, $type){
        $data["session"] = $this->data["session"];
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['content'] = 'dashboard/overview';
        $data['script'] = 'dashboard/script_overview';
        $data['title'] = "OVERVIEW";
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data['type'] = $type;

        $detail_title = $this->M_dashboard->get_detail($docno,$revnr);
        foreach ($detail_title[0] as $key => $value) {
            $data[$key] = $value;
        }
        if($data['REVTY'] == "AH"){
            $data['REVTY'] = "APU Heavy Repair";
        }elseif($data['REVTY'] == "AL"){
            $data['REVTY'] = "APU Light Repair";
        }elseif($data['REVTY'] == "AM"){
            $data['REVTY'] = "APU Medium Repair";
        }elseif($data['REVTY'] == "EM"){
            $data['REVTY'] = "Engine Minimum";
        }elseif($data['REVTY'] == "EO"){
            $data['REVTY'] = "Engine Overhaul";
        }elseif($data['REVTY'] == "EP"){
            $data['REVTY'] = "Engine Performance";
        }elseif($data['REVTY'] == "SR"){
            $data['REVTY'] = "SPECIAL REQUEST (CM)";
        }else{
            $data['REVTY'] = "";
        }
        
        $this->load->view('template', $data);
    }

    function scurve($docno, $revnr, $type){
        $data["session"] = $this->data["session"];
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['content'] = 'dashboard/scurve';
        $data['script'] = 'dashboard/script_scurve';
        $data['title'] = "S-CURVE";
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data['type'] = $type;

        $detail_title = $this->M_dashboard->get_detail($docno,$revnr);
        foreach ($detail_title[0] as $key => $value) {
            $data[$key] = $value;
        }
        if($data['REVTY'] == "AH"){
            $data['REVTY'] = "APU Heavy Repair";
        }elseif($data['REVTY'] == "AL"){
            $data['REVTY'] = "APU Light Repair";
        }elseif($data['REVTY'] == "AM"){
            $data['REVTY'] = "APU Medium Repair";
        }elseif($data['REVTY'] == "EM"){
            $data['REVTY'] = "Engine Minimum";
        }elseif($data['REVTY'] == "EO"){
            $data['REVTY'] = "Engine Overhaul";
        }elseif($data['REVTY'] == "EP"){
            $data['REVTY'] = "Engine Performance";
        }elseif($data['REVTY'] == "SR"){
            $data['REVTY'] = "SPECIAL REQUEST (CM)";
        }else{
            $data['REVTY'] = "";
        }

        $this->load->view('template', $data);
    }

    function manhours($docno, $revnr, $type){
        $data["session"] = $this->data["session"];
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['content'] = 'dashboard/manhours';
        $data['script'] = 'dashboard/script_manhours';
        $data['title'] = "MANHOURS";
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data['type'] = $type;

        $detail_title = $this->M_dashboard->get_detail($docno,$revnr);
        foreach ($detail_title[0] as $key => $value) {
            $data[$key] = $value;
        }
        if($data['REVTY'] == "AH"){
            $data['REVTY'] = "APU Heavy Repair";
        }elseif($data['REVTY'] == "AL"){
            $data['REVTY'] = "APU Light Repair";
        }elseif($data['REVTY'] == "AM"){
            $data['REVTY'] = "APU Medium Repair";
        }elseif($data['REVTY'] == "EM"){
            $data['REVTY'] = "Engine Minimum";
        }elseif($data['REVTY'] == "EO"){
            $data['REVTY'] = "Engine Overhaul";
        }elseif($data['REVTY'] == "EP"){
            $data['REVTY'] = "Engine Performance";
        }elseif($data['REVTY'] == "SR"){
            $data['REVTY'] = "SPECIAL REQUEST (CM)";
        }else{
            $data['REVTY'] = "";
        }

        $this->load->view('template', $data);
    }

    function deviation($docno, $revnr, $type){
        $data["session"] = $this->data["session"];
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['content'] = 'dashboard/deviation';
        $data['script'] = 'dashboard/script_deviation';
        $data['title'] = "DEVIATION";
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data['type'] = $type;

        $detail_title = $this->M_dashboard->get_detail($docno,$revnr);
        foreach ($detail_title[0] as $key => $value) {
            $data[$key] = $value;
        }
        if($data['REVTY'] == "AH"){
            $data['REVTY'] = "APU Heavy Repair";
        }elseif($data['REVTY'] == "AL"){
            $data['REVTY'] = "APU Light Repair";
        }elseif($data['REVTY'] == "AM"){
            $data['REVTY'] = "APU Medium Repair";
        }elseif($data['REVTY'] == "EM"){
            $data['REVTY'] = "Engine Minimum";
        }elseif($data['REVTY'] == "EO"){
            $data['REVTY'] = "Engine Overhaul";
        }elseif($data['REVTY'] == "EP"){
            $data['REVTY'] = "Engine Performance";
        }elseif($data['REVTY'] == "SR"){
            $data['REVTY'] = "SPECIAL REQUEST (CM)";
        }else{
            $data['REVTY'] = "";
        }

        $this->load->view('template', $data);
    }

    function highlight($docno, $revnr, $type){
        $data["session"] = $this->data["session"];
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['content'] = 'dashboard/highlight';
        $data['script'] = 'dashboard/script_highlight';
        $data['title'] = "HIGHLIGHT";
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data['type'] = $type;

        $detail_title = $this->M_dashboard->get_detail($docno,$revnr);
        foreach ($detail_title[0] as $key => $value) {
            $data[$key] = $value;
        }
        if($data['REVTY'] == "AH"){
            $data['REVTY'] = "APU Heavy Repair";
        }elseif($data['REVTY'] == "AL"){
            $data['REVTY'] = "APU Light Repair";
        }elseif($data['REVTY'] == "AM"){
            $data['REVTY'] = "APU Medium Repair";
        }elseif($data['REVTY'] == "EM"){
            $data['REVTY'] = "Engine Minimum";
        }elseif($data['REVTY'] == "EO"){
            $data['REVTY'] = "Engine Overhaul";
        }elseif($data['REVTY'] == "EP"){
            $data['REVTY'] = "Engine Performance";
        }elseif($data['REVTY'] == "SR"){
            $data['REVTY'] = "SPECIAL REQUEST (CM)";
        }else{
            $data['REVTY'] = "";
        }

        $this->load->view('template', $data);
    }

    function eo_report($docno, $revnr, $type){
        $editable = FALSE;
        foreach ($this->menu_top as $key => $value) {
            if(strtolower($value['url']) == strtolower($this->url)){
                if($value['editable'] != ""){
                    $editable = TRUE;
                }else{
                    $editable = FALSE;
                }
            }
        }
        $data['editable'] = $editable;
        $data["session"] = $this->data["session"];
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['content'] = 'dashboard/eo_report';
        $data['script'] = 'dashboard/script_eo_report';
        $data['title'] = "EO-REPORT";
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data['type'] = $type;

        $detail_title = $this->M_dashboard->get_detail($docno,$revnr);
        foreach ($detail_title[0] as $key => $value) {
            $data[$key] = $value;
        }
        if($data['REVTY'] == "AH"){
            $data['REVTY'] = "APU Heavy Repair";
        }elseif($data['REVTY'] == "AL"){
            $data['REVTY'] = "APU Light Repair";
        }elseif($data['REVTY'] == "AM"){
            $data['REVTY'] = "APU Medium Repair";
        }elseif($data['REVTY'] == "EM"){
            $data['REVTY'] = "Engine Minimum";
        }elseif($data['REVTY'] == "EO"){
            $data['REVTY'] = "Engine Overhaul";
        }elseif($data['REVTY'] == "EP"){
            $data['REVTY'] = "Engine Performance";
        }elseif($data['REVTY'] == "SR"){
            $data['REVTY'] = "SPECIAL REQUEST (CM)";
        }else{
            $data['REVTY'] = "";
        }

        $this->load->view('template', $data);
    }

    function survey($docno, $revnr, $type){
        $editable = FALSE;
        foreach ($this->menu_top as $key => $value) {
            if(strtolower($value['url']) == strtolower($this->url)){
                if($value['editable'] != ""){
                    $editable = TRUE;
                }else{
                    $editable = FALSE;
                }
            }
        }
        $data['editable'] = $editable;
        $data["session"] = $this->data["session"];
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['content'] = 'dashboard/survey';
        $data['script'] = 'dashboard/script_survey';
        $data['title'] = "POST PROJECT FEEDBACK";
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data['type'] = $type;

        $detail_title = $this->M_dashboard->get_detail($docno,$revnr);
        foreach ($detail_title[0] as $key => $value) {
            $data[$key] = $value;
        }
        if($data['REVTY'] == "AH"){
            $data['REVTY'] = "APU Heavy Repair";
        }elseif($data['REVTY'] == "AL"){
            $data['REVTY'] = "APU Light Repair";
        }elseif($data['REVTY'] == "AM"){
            $data['REVTY'] = "APU Medium Repair";
        }elseif($data['REVTY'] == "EM"){
            $data['REVTY'] = "Engine Minimum";
        }elseif($data['REVTY'] == "EO"){
            $data['REVTY'] = "Engine Overhaul";
        }elseif($data['REVTY'] == "EP"){
            $data['REVTY'] = "Engine Performance";
        }elseif($data['REVTY'] == "SR"){
            $data['REVTY'] = "SPECIAL REQUEST (CM)";
        }else{
            $data['REVTY'] = "";
        }

        $detail_survey = $this->M_survey->get_survey($revnr);
        if($detail_survey){
            $data['survey'] = $detail_survey;
        }else{
            $data['survey'] = "";
        }

        //print('<pre>'.print_r($data,TRUE).'</pre>');die();
        $this->load->view('template', $data);
    }

}
?>