<?php
date_default_timezone_set("Asia/Jakarta");
class Management extends CI_Controller {

    function __construct(){
        parent::__construct();
        //LOAD MODEL
        $this->load->model('M_management', '', TRUE);
        //LOAD OTHER
        $this->data["session"] = $this->session->userdata('logged_in');
        $this->url = current_url();
        $this->menu = json_decode($_COOKIE['all_menu'],true);
        $this->menu = $this->menu['menu'];
        //SCRIPTING HERE
        if(count($_SESSION['logged_in']) == 0){
            redirect(base_url());
        }
    }

    function user(){
        $editable = FALSE;
        foreach ($this->menu as $key => $value) {
            foreach ($this->menu[$key] as $val) {
                if(strtolower($val['url']) == strtolower($this->url)){
                    if($val['editable']){
                        $editable = TRUE;
                    }else{
                        $editable = FALSE;
                    }
                }
            }
        }
        $data["session"] = $this->data["session"];
        $data['content'] = 'management/user';
        $data['script'] = 'management/script_user';
        $data['modal'] = 'management/modal_user';
        $data['title'] = 'User Management';
        $data['editable'] = $editable;

        $data['role']     = $this->M_management->get_all_role();
        $data['customer'] = $this->M_management->get_all_customer();
        $data['group']    = $this->M_management->get_all_group();
        $this->load->view('template', $data);
    }

    function menu(){
        $editable = FALSE;
        foreach ($this->menu as $key => $value) {
            foreach ($this->menu[$key] as $val) {
                if(strtolower($val['url']) == strtolower($this->url)){
                    if($val['editable']){
                        $editable = TRUE;
                    }else{
                        $editable = FALSE;
                    }
                }
            }
        }
        $data["session"] = $this->data["session"];
        $data['content'] = 'management/menu';
        $data['script'] = 'management/script_menu';
        $data['title'] = 'Menu Management';
        $data['editable'] = $editable;

        $data_modul_menu = $this->M_management->get_modul_menu();
        $data_modul_menu_top = $this->M_management->get_modul_menu_top();
        $data['modul'] = array_keys($data_modul_menu);
        $data['menu'] = $data_modul_menu;
        $data['menu_top'] = $data_modul_menu_top;
        
        $this->load->view('template', $data);
    }

    function EditGroup($id_group){
        $data["session"] = $this->data["session"];
        $data['content'] = 'management/menu_edit';
        $data['script'] = 'management/script_menu_edit';
        $data['title'] = 'Edit Group Menu';

        $data['title_tab'] = 'Edit Group';
        $data['id_group'] = $id_group;
        $data['editable'] = TRUE;
        $data_modul_menu = $this->M_management->get_modul_menu();
        $data_modul_menu_top = $this->M_management->get_modul_menu_top();
        $data['modul'] = array_keys($data_modul_menu);
        $data['menu'] = $data_modul_menu;
        $data['menu_top'] = $data_modul_menu_top;
        
        $this->load->view('template', $data);
    }

    function ViewGroup($id_group){
        $data["session"] = $this->data["session"];
        $data['content'] = 'management/menu_edit';
        $data['script'] = 'management/script_menu_edit';
        $data['title'] = 'View Group Menu';

        $data['title_tab'] = 'View Group';
        $data['id_group'] = $id_group;
        $data['editable'] = FALSE;
        $data_modul_menu = $this->M_management->get_modul_menu();
        $data_modul_menu_top = $this->M_management->get_modul_menu_top();
        $data['modul'] = array_keys($data_modul_menu);
        $data['menu'] = $data_modul_menu;
        $data['menu_top'] = $data_modul_menu_top;

        $this->load->view('template', $data);
    }

}
?>