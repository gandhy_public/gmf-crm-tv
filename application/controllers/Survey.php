<?php
date_default_timezone_set("Asia/Jakarta");
class Survey extends CI_Controller {

    function __construct(){
        parent::__construct();
        //LOAD LIBRARY
        $this->load->library('tcpdf/tcpdf');
        //LOAD MODEL
        $this->load->model('M_survey', '', TRUE);
        $this->load->model('M_dashboard', '', TRUE);
        //LOAD OTHER
        $this->data["session"] = $this->session->userdata('logged_in');
        //SCRIPTING HERE
        if(count($_SESSION['logged_in']) == 0){
            redirect(base_url());
        }
    }

    function index(){
        $data["session"] = $this->data["session"];
        $data['content'] = 'survey/index';
        $data['script'] = 'survey/script_index';
        $data['title'] = 'Feedback List';
        
        $this->load->view('template', $data);
    }

    function view($docno, $revnr){
        $data["session"] = $this->data["session"];
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['content'] = 'survey/view';
        $data['script'] = 'survey/script_view';
        $data['title'] = "POST PROJECT FEEDBACK";
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["type"] = 'close';

        $detail_title = $this->M_dashboard->get_detail($docno,$revnr);
        foreach ($detail_title[0] as $key => $value) {
            $data[$key] = $value;
        }
        if($data['REVTY'] == "AH"){
            $data['REVTY'] = "APU Heavy Repair";
        }elseif($data['REVTY'] == "AL"){
            $data['REVTY'] = "APU Light Repair";
        }elseif($data['REVTY'] == "AM"){
            $data['REVTY'] = "APU Medium Repair";
        }elseif($data['REVTY'] == "EM"){
            $data['REVTY'] = "Engine Minimum";
        }elseif($data['REVTY'] == "EO"){
            $data['REVTY'] = "Engine Overhaul";
        }elseif($data['REVTY'] == "EP"){
            $data['REVTY'] = "Engine Performance";
        }elseif($data['REVTY'] == "SR"){
            $data['REVTY'] = "SPECIAL REQUEST (CM)";
        }else{
            $data['REVTY'] = "";
        }

        $detail_survey = $this->M_survey->get_survey($revnr);
        if($detail_survey){
            $data['survey'] = $detail_survey;
        }else{
            $data['survey'] = "";
        }

        $this->load->view('template', $data);
    }

}
?>