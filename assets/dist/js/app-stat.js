/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var lblPhase = ["PRELIM", "OPN/REM", "INSP", "SERV/LUB", "INST/REST", "OPC/FUC"];
var lblMaterialStatsus = ["NIL STOCK", "SOA", "CUST. SPLY", "ORDERED PURC", "CUSTOM", "SHIPMENT"];
var lblPerArea = ["AREA", "COCKPIT", "FUSELAGE", "LH-WING", "RH-WING", "L/G", "ENG#1", "ENG#2", "ENG#3", "ENG#4", "TAIL", "CABIN", "FWD CARGO", "AFT CARGO", "ELECT"];
var lblSkill = ["A/P", "CBN", "E/A", "NDT", "PAINTING", "STR", "TBR-SEAL", "TBR-SHOP"];
var lblShop = ["TVP", "TBRS", "CBN", "CBN SHOP", "SEAT", "TBP"];
var lblProgress = ["PERFORM BY SHOP", "WAITING RO", "MATERIAL WAITING", "PERFORM BY PROD"];

// Chart Default Conf
var defConfy = {
    beginAtZero: true,
    fontColor: 'rgba(236, 240, 241,1.0)'
};
var defConfx = {
    beginAtZero: true,
    fontColor: 'rgba(236, 240, 241,1.0)',
    autoSkip: false,
    fontSize: 8
};

var defConfy2 = {
    beginAtZero: true
};
var defConfx2 = {
    autoSkip: false,
    fontSize: 8

    // JOBCARD PERCENTAGE PER PHASE
};var ctxPerPhase = document.getElementById("PerPhase");
if (ctxPerPhase != null) {
    ctxPerPhase.height = 140;
    var chartPerPhase = new Chart(ctxPerPhase, {
        type: 'bar',
        data: {
            labels: lblPhase,
            datasets: [{
                data: [getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100)],
                backgroundColor: 'rgba(236, 240, 241,1.0)'
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: defConfy
                }],
                xAxes: [{
                    ticks: defConfx
                }]
            },
            legend: {
                display: false
            }
        }
    });
}

// MATERIAL STATUS
var ctxMaterialStatusQty = document.getElementById("MaterialStatusQty");
if (ctxMaterialStatusQty != null) {
    ctxMaterialStatusQty.height = 140;
    var chartMaterialStatusQty = new Chart(ctxMaterialStatusQty, {
        type: 'bar',
        data: {
            labels: lblMaterialStatsus,
            datasets: [{
                data: [getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100)],
                backgroundColor: 'rgba(236, 240, 241,1.0)'
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: defConfy
                }],
                xAxes: [{
                    ticks: defConfx
                }]
            },
            legend: {
                display: false
            }
        }
    });
}

// JOBCARD PERCENTAGE PER AREA
var ctxPerArea = document.getElementById("PerArea");
if (ctxPerArea != null) {
    ctxPerArea.height = 140;
    var chartPerArea = new Chart(ctxPerArea, {
        type: 'bar',
        data: {
            labels: lblPerArea,
            datasets: [{
                data: [getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100)],
                backgroundColor: 'rgba(236, 240, 241,1.0)'
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: defConfy
                }],
                xAxes: [{
                    ticks: defConfx
                }]
            },
            legend: {
                display: false
            }
        }
    });
}

// MDR OPEN & CLOSE COMPARASSION 
var ctxOpenCloseComparassion = document.getElementById("OpenCloseComparassion");
if (ctxOpenCloseComparassion != null) {
    ctxOpenCloseComparassion.height = 140;
    var chartOpenCloseComparassion = new Chart(ctxOpenCloseComparassion, {
        type: 'bar',
        data: {
            labels: lblPerArea,
            datasets: [{
                label: 'Open',
                data: [getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100)],
                backgroundColor: 'rgba(52, 73, 94,1.0)'
            }, {
                label: 'Close',
                data: [40, 50, 0, 80, 60, 30, 44, 10, 10, 10, 25, 10, 10, 25, 30, 44, 10],
                backgroundColor: 'rgba(236, 240, 241,1.0)'
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    stacked: true,
                    ticks: {
                        autoSkip: false,
                        fontSize: 6
                    }
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        autoSkip: false,
                        fontSize: 8
                    }
                }]
            }
        }
    });
}

// JOBCARD BY SKILL
var ctxJobCardBySkill = document.getElementById("JobCardBySkill");
if (ctxJobCardBySkill != null) {
    JobCardBySkill.height = 140;
    var chartJobCardBySkill = new Chart(ctxJobCardBySkill, {
        type: 'bar',
        data: {
            labels: lblSkill,
            datasets: [{
                label: 'Open',
                data: [60, 50, 100, 20, 40, 70, 56, 90],
                backgroundColor: 'rgba(52, 73, 94,1.0)'
            }, {
                label: 'Close',
                data: [40, 50, 0, 80, 60, 30, 44, 10],
                backgroundColor: 'rgba(236, 240, 241,1.0)'
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    stacked: true,
                    ticks: {
                        autoSkip: false,
                        fontSize: 8
                    }
                }],
                yAxes: [{
                    stacked: true,
                    autoSkip: false,
                    fontSize: 8
                }]
            }
        }
    });
}

/* SECOND THEME
**********************************************************************/

// JOBCARD PERCENTAGE PER PHASE
var ctxPerPhase2 = document.getElementById("PerPhase2");
if (ctxPerPhase2 != null) {
    ctxPerPhase2.height = 140;
    var myChart = new Chart(ctxPerPhase2, {
        type: 'bar',
        data: {
            labels: lblPhase,
            datasets: [{
                data: [getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100)],
                backgroundColor: ['rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)', 'rgba(153, 102, 255, 1)', 'rgba(255, 159, 64, 1)']
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        autoSkip: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false,
                        fontSize: 8
                    }
                }]
            },
            legend: {
                display: false
            }
        }
    });
}

// JOB PERCENTAGE BY AREA
var ctxPerArea2 = document.getElementById("PerArea2");
if (ctxPerArea2 != null) {
    ctxPerArea2.height = 140;
    var chartPerArea2 = new Chart(ctxPerArea2, {
        type: 'bar',
        data: {
            labels: lblPerArea,
            datasets: [{
                data: [getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100)],
                backgroundColor: ['rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)', 'rgba(153, 102, 255, 1)', 'rgba(255, 159, 64, 1)', 'rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)', 'rgba(153, 102, 255, 1)', 'rgba(255, 159, 64, 1)', 'rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)', 'rgba(153, 102, 255, 1)', 'rgba(255, 159, 64, 1)']
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: defConfy2
                }],
                xAxes: [{
                    ticks: defConfx2
                }]
            },
            legend: {
                display: false
            }
        }
    });
}

// MATERIAL STATUS
var ctxMaterialStatusQty2 = document.getElementById("MaterialStatusQty2");
if (ctxMaterialStatusQty2 != null) {
    ctxMaterialStatusQty2.height = 140;
    var chartMaterialStatusQty2 = new Chart(ctxMaterialStatusQty2, {
        type: 'bar',
        data: {
            labels: lblMaterialStatsus,
            datasets: [{
                data: [getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100)],
                backgroundColor: ['rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)', 'rgba(153, 102, 255, 1)', 'rgba(255, 159, 64, 1)']
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: defConfy2
                }],
                xAxes: [{
                    ticks: defConfx2
                }]
            },
            legend: {
                display: false
            }
        }
    });
}

// MDR OPEN & CLOSE COMPARASSION 
var ctxOpenCloseComparassion2 = document.getElementById("OpenCloseComparassion2");
if (ctxOpenCloseComparassion2 != null) {
    ctxOpenCloseComparassion2.height = 140;
    var chartOpenCloseComparassion2 = new Chart(ctxOpenCloseComparassion2, {
        type: 'bar',
        data: {
            labels: lblPerArea,
            datasets: [{
                label: 'Open',
                data: [getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100)],
                backgroundColor: 'rgba(255, 99, 132, 1)'
            }, {
                label: 'Close',
                data: [getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100)],
                backgroundColor: 'rgba(54, 162, 235, 1)'
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    stacked: true,
                    ticks: {
                        autoSkip: false,
                        fontSize: 6
                    }
                }],
                yAxes: [{
                    stacked: true,
                    autoSkip: false,
                    fontSize: 6
                }]
            }
        }
    });
}

// JOBCARD BY SKILL
var ctxJobCardBySkill2 = document.getElementById("JobCardBySkill2");
if (ctxJobCardBySkill2 != null) {
    JobCardBySkill2.height = 140;
    var chartJobCardBySkill2 = new Chart(ctxJobCardBySkill2, {
        type: 'bar',
        data: {
            labels: lblSkill,
            datasets: [{
                label: 'Open',
                data: [getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100)],
                backgroundColor: 'rgba(255, 99, 132, 1)'
            }, {
                label: 'Close',
                data: [getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100)],
                backgroundColor: 'rgba(54, 162, 235, 1)'
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    stacked: true,
                    ticks: {
                        autoSkip: false,
                        fontSize: 8
                    }
                }],
                yAxes: [{
                    stacked: true,
                    autoSkip: false,
                    fontSize: 8
                }]
            }
        }
    });
}

// MDR SENT TO SHOP
var ctxMdrSentToShop2 = document.getElementById("MdrSentToShop2");
if (ctxMdrSentToShop2 != null) {
    MdrSentToShop2.height = 140;
    var chartMdrSentToShop2 = new Chart(ctxMdrSentToShop2, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100)],
                backgroundColor: ['rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)', 'rgba(153, 102, 255, 1)', 'rgba(255, 159, 64, 1)', 'rgba(255, 99, 132, 1)'],
                borderWidth: 0,
                label: 'Dataset 1'
            }],
            labels: lblShop
        },
        options: {
            responsive: true,
            legend: {
                fontSize: 6,
                position: 'right'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    });
}

// MATERIAL PROGRESS STATUS
var ctxMdrProgressStatus2 = document.getElementById("MdrProgressStatus2");
if (ctxMdrProgressStatus2 != null) {
    MdrProgressStatus2.height = 140;
    var chartMdrProgressStatus2 = new Chart(ctxMdrProgressStatus2, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100), getRandomInt(1, 100)],
                backgroundColor: ['rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)', 'rgba(153, 102, 255, 1)', 'rgba(255, 159, 64, 1)'],
                borderColor: ['rgba(255,99,132,1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)', 'rgba(153, 102, 255, 1)', 'rgba(255, 159, 64, 1)', 'rgba(255,99,132,1)'],
                borderWidth: 1,
                label: 'Dataset 1'
            }],
            labels: lblProgress
        },
        options: {
            responsive: true,
            legend: {
                fontSize: 6,
                position: 'right'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    });
}

/***/ })
/******/ ]);